﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipmentHandler : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public GameObject UnarmedSword;
    [SerializeField]
    public GameObject UnarmedShield;
    [SerializeField]
    public GameObject UnarmedBow;
    [SerializeField]
    public GameObject ArmedSword;
    [SerializeField]
    public GameObject ArmedShield;
    [SerializeField]
    public GameObject ArmedBow;
    [SerializeField]
    public GameObject ArmedArrow;

    // Super-script
    private PlayerManager _PlayerManager;

    // Components
    private LineRenderer m_ArrowLine;

    // Members
    private PlayerManager.ArmState m_FromState;
    private PlayerManager.ArmState m_ToState;

    private void Awake()
    {
        if (UnarmedSword == null || 
            UnarmedShield == null || 
            UnarmedBow == null ||
            ArmedSword == null || 
            ArmedShield == null || 
            ArmedBow == null ||
            ArmedArrow == null)
            Debug.LogError("A mesh field is null! PlayerStateMachine will not work correctly.");

        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        m_ArrowLine = ArmedArrow.GetComponent<LineRenderer>();
        m_ArrowLine.SetPosition(0, ArmedArrow.transform.position);
        m_ArrowLine.enabled = false;

        ArmedSword.SetActive(false);
        ArmedShield.SetActive(false);
        ArmedBow.SetActive(false);
        ArmedArrow.SetActive(false);

    }

    public void EquipSwordShield(bool isEquipped)
    {
        UnarmedSword.SetActive(!isEquipped);
        UnarmedShield.SetActive(!isEquipped);

        ArmedSword.SetActive(isEquipped);
        ArmedShield.SetActive(isEquipped);
    }

    public void EquipBowArrow(bool isEquipped)
    {
        UnarmedBow.SetActive(!isEquipped);

        ArmedBow.SetActive(isEquipped);

        // Only show arrow if we have arrows
        ArmedArrow.SetActive(isEquipped && _PlayerManager._PlayerAttributes.GetCurrentArrows() > 0);
    }

    public void ShowArrowLine(bool show)
    {
        m_ArrowLine.enabled = show;
    }

    public void ShowArrow(int show)
    {
        if (show == 0)
            ArmedArrow.SetActive(false);
        else if (show == 1)
            ArmedArrow.SetActive(true);
        else
            Debug.LogError("Invalid flag int in equipment handler!");
    }

    public void AimArrowLine(Vector3 target)
    {
        if (!m_ArrowLine.enabled) return;

        m_ArrowLine.SetPosition(0, ArmedArrow.transform.position);
        m_ArrowLine.SetPosition(1, target);
    }

    public Vector3 GetArrowStartPosition()
    {
        return ArmedArrow.transform.position;
    }

    public void SetTransitionStates(PlayerManager.ArmState fromState, PlayerManager.ArmState toState)
    {
        m_FromState = fromState;
        m_ToState = toState;
    }

    public bool HasBowEquipped() { return ArmedBow.activeSelf; }

    public PlayerManager.ArmState GetFromState() { return m_FromState; }
    public PlayerManager.ArmState GetToState() { return m_ToState; }

}
