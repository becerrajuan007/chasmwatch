﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationHandler : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Components
    private Animator m_PlayerAnimator;

    // Member constants
    private readonly int h_MovementMagnitude = Animator.StringToHash("MovementMagnitude");
    private readonly int h_MovementX = Animator.StringToHash("MovementX");
    private readonly int h_MovementZ = Animator.StringToHash("MovementZ");
    private readonly int h_MovementY = Animator.StringToHash("MovementY");

    private readonly int h_IsGrounded = Animator.StringToHash("IsGrounded");
    private readonly int h_IsSprinting = Animator.StringToHash("IsSprinting");
    private readonly int h_IsFocussed = Animator.StringToHash("IsFocussed");
    private readonly int h_IsDodging = Animator.StringToHash("IsDodging");
    private readonly int h_IsBlocking = Animator.StringToHash("IsBlocking");
    private readonly int h_IsSwordShield = Animator.StringToHash("IsSwordShield");
    private readonly int h_IsBow = Animator.StringToHash("IsBow");
    private readonly int h_IsAttacking = Animator.StringToHash("IsAttacking");
    private readonly int h_IsDrawingBow = Animator.StringToHash("IsDrawingBow");

    private readonly int h_ArmTrigger = Animator.StringToHash("ArmTrigger");
    private readonly int h_LightAttackTrigger = Animator.StringToHash("LightAttackTrigger");
    private readonly int h_HeavyAttackTrigger = Animator.StringToHash("HeavyAttackTrigger");
    private readonly int h_StaggerTrigger = Animator.StringToHash("StaggerTrigger");
    private readonly int h_CancelAttackTrigger = Animator.StringToHash("CancelAttackTrigger");
    private readonly int h_DeathTrigger = Animator.StringToHash("DeathTrigger");

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        m_PlayerAnimator = GetComponent<Animator>();
    }

    public void UpdateAnimatorData()
    {
        m_PlayerAnimator.SetFloat(h_MovementMagnitude, _PlayerManager._PlayerMover.GetVelocityXZMagnitude());

        Vector3 movementVector = _PlayerManager._PlayerMover.GetVelocityMovementVector();
        m_PlayerAnimator.SetFloat(h_MovementX, movementVector.z);
        m_PlayerAnimator.SetFloat(h_MovementZ, movementVector.x);
        m_PlayerAnimator.SetFloat(h_MovementY, _PlayerManager._PlayerMover.GetVelocityYMagnitude());

        m_PlayerAnimator.SetBool(h_IsGrounded, _PlayerManager._MovementState != PlayerManager.MovementState.FALLING);
        m_PlayerAnimator.SetBool(h_IsSprinting, _PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING);
        m_PlayerAnimator.SetBool(h_IsFocussed, _PlayerManager._FocusState != PlayerManager.FocusState.UNFOCUSSED);
        m_PlayerAnimator.SetBool(h_IsDodging, _PlayerManager._DodgeState != PlayerManager.DodgeState.NONE);
        m_PlayerAnimator.SetBool(h_IsBlocking, IsBlocking());
        m_PlayerAnimator.SetBool(h_IsSwordShield, _PlayerManager._ArmState == PlayerManager.ArmState.SWORD_SHIELD);
        m_PlayerAnimator.SetBool(h_IsBow, _PlayerManager._ArmState == PlayerManager.ArmState.BOW);
        m_PlayerAnimator.SetBool(h_IsAttacking, IsAttacking());
        m_PlayerAnimator.SetBool(h_IsDrawingBow, IsDrawingBow());
    }

    // Util
    private bool IsAttacking() 
    {
        return (_PlayerManager._AttackState == PlayerManager.AttackState.L_A_1 ||
                _PlayerManager._AttackState == PlayerManager.AttackState.L_A_2 ||
                _PlayerManager._AttackState == PlayerManager.AttackState.L_A_3 ||
                _PlayerManager._AttackState == PlayerManager.AttackState.H_A_1 ||
                _PlayerManager._AttackState == PlayerManager.AttackState.H_A_2);
    }

    private bool IsDrawingBow()
    {
        return (_PlayerManager._AttackState == PlayerManager.AttackState.BOW_DRAW ||
                _PlayerManager._AttackState == PlayerManager.AttackState.BOW_HOLD);
    }

    private bool IsBlocking()
    {
        return _PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_IDLE ||
               _PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_HIT;
    }

    // Interface 
    public void TriggerArmTransition() { m_PlayerAnimator.SetTrigger(h_ArmTrigger); }
    public void TriggerLightAttack() { m_PlayerAnimator.SetTrigger(h_LightAttackTrigger); }
    public void TriggerHeavyAttack() { m_PlayerAnimator.SetTrigger(h_HeavyAttackTrigger); }
    public void TriggerStagger() { m_PlayerAnimator.SetTrigger(h_StaggerTrigger); }
    public void TriggerCancelAttack() { m_PlayerAnimator.SetTrigger(h_CancelAttackTrigger); }
    public  void TriggerDeath() { m_PlayerAnimator.SetTrigger(h_DeathTrigger); }

}
