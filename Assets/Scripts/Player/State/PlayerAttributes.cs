﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttributes : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public DungeonShrine StartingShrine = null;

    // Fields
    [SerializeField]
    public int StaminaRecoveryTickRate = 25;
    [SerializeField]
    public int DodgeStaminaCost = 25;
    [SerializeField]
    public int LightAttackStaminaCost = 15;
    [SerializeField]
    public int HeavyAttackStaminaCost = 20;
    [SerializeField]
    public float SprintStaminaCost = 12f;
    [SerializeField]
    public float StaminaRecoveryTime = 0.24f;

    [SerializeField]
    public int Level = 1;
    [SerializeField]
    public float Experience = 0f;
    [SerializeField]
    public int Gold = 1000;

    // Super-script
    public PlayerManager _PlayerManager;

    // Members
    private float m_CurrentHealth, m_CurrentStamina;
    private float m_MaxHealth, m_MaxStamina;
    private float m_CurrentPoise, m_MaxPoise;
    private int m_CurrentArrows, m_MaxArrows;
    private int m_CurrentKeys;
    private bool m_GodmodeEnabled = false;

    private List<DungeonShrine> m_VisitedShrines;
    private int m_LastVisitedShrineIndex;

    private float m_LastDodgeTime;
    private float m_LastAttackTime;
    private float m_LastSprintTime;
    private float m_LastBlockTime;

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        m_CurrentHealth = 100f;
        m_CurrentStamina = 100f;
        m_CurrentPoise = 10f;
        m_CurrentArrows = 10;
        m_CurrentKeys = 0;

        m_MaxHealth = 100f;
        m_MaxStamina = 100f;
        m_MaxPoise = 10f;
        m_MaxArrows = 10;

        if (StartingShrine == null) Debug.LogError("Starting shrine is null!");

        m_VisitedShrines = new List<DungeonShrine>();
        m_VisitedShrines.Add(StartingShrine);
        m_LastVisitedShrineIndex = 0;

        m_LastDodgeTime = 0f;
        m_LastAttackTime = 0f;
        m_LastSprintTime = 0f;
        m_LastBlockTime = 0f;
    }

    private void Start()
    {
        // Warp to starting shrine
        m_VisitedShrines[m_LastVisitedShrineIndex].ActivateShrine();
        WarpToDungeonShrine(m_VisitedShrines[m_LastVisitedShrineIndex]);
    }

    // Main attributes
    public void UpdateAttributes()
    {
        // Update state timers
        if (_PlayerManager._DodgeState != PlayerManager.DodgeState.NONE) m_LastDodgeTime = Time.time;
        if (_PlayerManager._AttackState == PlayerManager.AttackState.L_A_1 ||
            _PlayerManager._AttackState == PlayerManager.AttackState.L_A_2 ||
            _PlayerManager._AttackState == PlayerManager.AttackState.L_A_3 ||
            _PlayerManager._AttackState == PlayerManager.AttackState.H_A_1 ||
            _PlayerManager._AttackState == PlayerManager.AttackState.H_A_2) m_LastAttackTime = Time.time;
        if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING) m_LastSprintTime = Time.time;

        // Can we recover stamina?
        if (Time.time - m_LastDodgeTime >= StaminaRecoveryTime && 
            Time.time - m_LastAttackTime >= StaminaRecoveryTime &&
            Time.time - m_LastSprintTime >= StaminaRecoveryTime &&
            Time.time - m_LastBlockTime >= StaminaRecoveryTime)
        {
            // Increment stamina, but make it half the speed if we're blocking
            ChangeStamina(StaminaRecoveryTickRate * Time.deltaTime * 
                (_PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_IDLE ? 0.5f : 1.0f));
        }
    }

    // Interface
    public int GetCurrentHealth() { return (int)m_CurrentHealth; }
    public int GetCurrentStamina() { return (int)m_CurrentStamina; }
    public int GetCurrentArrows() { return m_CurrentArrows; }
    public int GetCurrentKeys() { return m_CurrentKeys; }

    public int GetMaxHealth() { return (int)m_MaxHealth; }
    public int GetMaxStamina() { return (int)m_MaxStamina; }
    public int GetMaxArrows() { return m_MaxArrows; }

    public void ChangeHealth(float delta)
    {
        if (m_GodmodeEnabled) return;

        m_CurrentHealth = Mathf.Clamp(m_CurrentHealth + delta, 0, m_MaxHealth);

        // Player is dead
        if (m_CurrentHealth == 0f)
        {
            _PlayerManager._PlayerCollisionHandler.EnableBodyHitbox(false);
            _PlayerManager._InteractionState = PlayerManager.InteractionState.DEAD;
            _PlayerManager._PlayerAnimationHandler.TriggerDeath();
            _PlayerManager._PlayerUIHandler.PushMessage("You died! Returning to last shrine...", true);

            _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;
            _PlayerManager._FocusState = PlayerManager.FocusState.UNFOCUSSED;
            _PlayerManager._DodgeState = PlayerManager.DodgeState.NONE;
            _PlayerManager._ArmState = PlayerManager.ArmState.UNARMED;
            _PlayerManager._AttackState = PlayerManager.AttackState.IDLE;
        }
    }

    public void ChangePoise(float delta)
    {
        if (m_GodmodeEnabled) return;

        m_CurrentPoise = Mathf.Clamp(m_CurrentPoise + delta, 0f, m_MaxPoise);

        // Player is staggered
        if (m_CurrentPoise == 0f && m_CurrentHealth != 0f)
        {
            _PlayerManager._PlayerAnimationHandler.TriggerStagger();

            m_CurrentPoise = m_MaxPoise;
        }
    }

    public void ChangeStamina(float delta)
    {
        if (m_GodmodeEnabled) return;
        m_CurrentStamina = Mathf.Clamp(m_CurrentStamina + delta, 0, m_MaxStamina);
    }

    public void ChangeArrows(int delta)
    {
        if (m_GodmodeEnabled) return;
        m_CurrentArrows = Mathf.Clamp(m_CurrentArrows + delta, 0, m_MaxArrows);
        _PlayerManager._PlayerUIHandler.SetArrowCount(m_CurrentArrows);
    }

    public void ChangeKeys(int delta)
    {
        m_CurrentKeys = Mathf.Clamp(m_CurrentKeys + delta, 0, 1000);
        _PlayerManager._PlayerUIHandler.SetKeyCount(m_CurrentKeys);
    }

    public float GetAttackDamage(PlayerManager.AttackState type)
    {
        float baseDamage = 0f;
        switch(type)
        {
            case PlayerManager.AttackState.L_A_1:
                baseDamage = 10f;
                break;
            case PlayerManager.AttackState.L_A_2:
                baseDamage = 14f;
                break;
            case PlayerManager.AttackState.L_A_3:
                baseDamage = 18f;
                break;
            case PlayerManager.AttackState.H_A_1:
                baseDamage = 22f;
                break;
            case PlayerManager.AttackState.H_A_2:
                baseDamage = 26f;
                break;
            case PlayerManager.AttackState.BOW_PULL:
                baseDamage = 10f;
                break;
            default:
                Debug.LogError("Unrecognized attack type.");
                return 0f;
        }

        if (m_GodmodeEnabled) baseDamage = 99999;

        return baseDamage + (Level * 1.5f);
    }

    public float GetPoiseDamage(PlayerManager.AttackState type)
    {
        float baseDamage = 0f;
        switch (type)
        {
            case PlayerManager.AttackState.L_A_1:
                baseDamage = 12f;
                break;
            case PlayerManager.AttackState.L_A_2:
                baseDamage = 18f;
                break;
            case PlayerManager.AttackState.L_A_3:
                baseDamage = 24f;
                break;
            case PlayerManager.AttackState.H_A_1:
                baseDamage = 30f;
                break;
            case PlayerManager.AttackState.H_A_2:
                baseDamage = 36f;
                break;
            case PlayerManager.AttackState.BOW_PULL:
                baseDamage = 0f;
                break;
            default:
                Debug.LogError("Unrecognized attack type.");
                return 0f;
        }

        if (m_GodmodeEnabled) baseDamage = 99999;

        return (baseDamage * (Level * 0.2f));
    }

    // Actions that cost stamina

    public bool DodgeChangeStamina()
    {
        if (m_GodmodeEnabled) return true;

        ChangeStamina(-DodgeStaminaCost);
        return true;
    }

    public bool LightAttackChangeStamina()
    {
        if (m_GodmodeEnabled) return true;

        ChangeStamina(-LightAttackStaminaCost);
        return true;
    }

    public bool HeavyAttackChangeStamina()
    {
        if (m_GodmodeEnabled) return true;

        ChangeStamina(-HeavyAttackStaminaCost);
        return true;
    }

    public bool BlockAttack(float damage, float poise)
    {
        _PlayerManager._PlayerAnimationHandler.TriggerStagger();
        m_LastBlockTime = Time.time;

        if (m_GodmodeEnabled) return true;

        ChangeHealth(damage * 0.1f);
        ChangeStamina(poise + (damage * 0.1f));

        if (GetCurrentStamina() <= 1f)
        {
            ChangePoise(-float.MaxValue);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SprintChangeStamina()
    {
        if (m_GodmodeEnabled) return true;

        if (m_CurrentStamina > 1)
        {
            ChangeStamina(-SprintStaminaCost * Time.deltaTime);
            return true;
        }

        return false;
    }

    public DungeonShrine GetLastVisitedShrine()
    {
        return m_VisitedShrines[m_LastVisitedShrineIndex];
    }

    public List<DungeonShrine> GetVisitedShrines()
    {
        return m_VisitedShrines;
    }

    public int GetLastVisitedShrineIndex()
    {
        return m_LastVisitedShrineIndex;
    }    

    public void AddToVisitedDungeonShrines(DungeonShrine shrine)
    {
        if (!m_VisitedShrines.Contains(shrine))
            m_VisitedShrines.Add(shrine);

        SetAsCurrentDungeonShrine(shrine);
    }

    public void SetAsCurrentDungeonShrine(DungeonShrine shrine)
    {
        // Reset stats
        ResetStats();

        // Change to selected index
        int index = 0;
        foreach (DungeonShrine s in m_VisitedShrines)
        {
            if (s == shrine)
            {
                m_LastVisitedShrineIndex = index;
                return;
            }
            index++;
        }

        Debug.LogError("Could not locate shrine " + shrine.ShrineName);
    }

    public void WarpToDungeonShrine(DungeonShrine shrine)
    {
        // Change to selected index
        SetAsCurrentDungeonShrine(shrine);

        // Get shrine's warp point
        Transform warpPoint = shrine.GetSpawnPoint();

        WarpToLocation(warpPoint);

        // Set dungeon zone to the shrine's zone
        DungeonZoneSystem.TransitionZone(shrine.Zone);

        // Set minimap name
        _PlayerManager._PlayerUIHandler.SetMinimapAreaName(shrine.Zone.Name);
    }

    public void WarpToLocation(Transform location)
    {
        // Momentarily disable the character controller
        _PlayerManager._PlayerMover.EnableCharacterController(false);

        // Reset player's position
        transform.position = location.position;
        transform.rotation = location.rotation;

        // Ignore camera's lerp, recenter camera
        if (_PlayerManager._PlayerCameraMover) _PlayerManager._PlayerCameraMover.ResetCamera();

        // Reenable character controller
        _PlayerManager._PlayerMover.EnableCharacterController(true);
    }

    public bool PeekCanDodge() { return m_CurrentStamina - (DodgeStaminaCost / 2) > 0f; }
    public bool PeekCanLightAttack() { return m_CurrentStamina - (LightAttackStaminaCost / 2) > 0f; }
    public bool PeekCanHeavyAttack() { return m_CurrentStamina - (HeavyAttackStaminaCost / 2) > 0f; }
    public bool PeekCanBlock(float poise) { return m_CurrentStamina - poise > 0f; }
    public bool PeekCanSprint() { return m_CurrentStamina >= (0.2f) * m_MaxStamina; }
    public bool ToggleGodmode()
    {
        m_GodmodeEnabled = !m_GodmodeEnabled;

        if (m_GodmodeEnabled)
        {
            ResetStats();
            ChangeKeys(99);
        }

        PauseMenu.SetSubMenuMessageText("Godmode " + (m_GodmodeEnabled ? "Enabled" : "Disabled"));

        return true;
    }

    // Animation

    public void RespawnAfterDeath()
    {
        _PlayerManager._PlayerCollisionHandler.EnableBodyHitbox(true);
        _PlayerManager._PlayerEquipmentHandler.EquipSwordShield(false);
        _PlayerManager._PlayerEquipmentHandler.EquipBowArrow(false);

        WarpToDungeonShrine(m_VisitedShrines[m_LastVisitedShrineIndex]);
    }

    public void RespawnRecover()
    {
        _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
    }

    // Util

    private void ResetStats()
    {
        m_CurrentHealth = m_MaxHealth;
        m_CurrentStamina = m_MaxStamina;
        m_CurrentPoise = m_MaxPoise;
        m_CurrentArrows = m_MaxArrows;

        _PlayerManager._PlayerUIHandler.SetArrowCount(m_CurrentArrows);
    }
}
