﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudioHandler : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public AudioClip[] Footsteps;
    [SerializeField]
    public AudioClip[] SwordSlashes;
    [SerializeField]
    public AudioClip SwordHit;
    [SerializeField]
    public AudioClip SwordDraw;
    [SerializeField]
    public AudioClip SwordSheathe;
    [SerializeField]
    public AudioClip BowDraw;
    [SerializeField]
    public AudioClip BowSheathe;
    [SerializeField]
    public AudioClip BowReady;
    [SerializeField]
    public AudioClip BowPull;
    [SerializeField]
    public AudioClip BlockHit;
    [SerializeField]
    public AudioClip BlockBreak;
    [SerializeField]
    public AudioClip RollStart;
    [SerializeField]
    public AudioClip RollEnd;
    [SerializeField]
    public AudioClip Focus;
    [SerializeField]
    public AudioClip Unfocus;
    [SerializeField]
    public AudioClip MenuToggle;
    [SerializeField]
    public AudioClip MenuSelect;

    // Super-scripts
    private PlayerManager _PlayerManager;
    private NPCManager _NPCManager;
    private bool m_IsPlayer;

    // Components
    private AudioSource m_AudioSource;

    // Constants
    private readonly System.Random c_Random = new System.Random();

    // Members
    private float m_LastRollStart;
    private float m_LastRollEnd;

    private void Awake()
    {
        if (Footsteps.Length == 0 ||
            SwordSlashes.Length == 0 ||
            SwordDraw == null ||
            SwordSheathe == null ||
            RollStart == null ||
            RollEnd == null ||
            Focus == null ||
            Unfocus == null) 
            Debug.LogError("A sound effect is null! Audio source will not work correctly.");

        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();
        _NPCManager = transform.parent.gameObject.GetComponent<NPCManager>();

        m_IsPlayer = _PlayerManager != null;

        m_AudioSource = GetComponent<AudioSource>();

        m_LastRollStart = 0f;
        m_LastRollEnd = 0f;
    }

    // Interface
    public void PlayFootstep() {
        // Will sometimes play when player is not moving, so check speed
        if (_PlayerManager._PlayerMover.GetVelocityXZMagnitude() > 0.05f)
        {
            m_AudioSource.PlayOneShot(Footsteps[c_Random.Next(0, Footsteps.Length)], 0.5f);

            // Also play a poof effect for footstep
            ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.STEP_DUST, transform.position, transform.rotation, new Vector3(1.5f, 1.5f, 1.5f));
        }
    }

    public void PlaySwordSlash()
    { 
        m_AudioSource.PlayOneShot(SwordSlashes[c_Random.Next(0, SwordSlashes.Length)], 0.7f);
        XInputSystem.AddVibration(0.06f, 0.1f, 0.6f);
    }

    public void PlaySwordHit()
    {
        m_AudioSource.PlayOneShot(SwordHit, 0.8f);
        XInputSystem.AddVibration(0.2f, 0.8f, 0.8f);
    }

    public void PlaySwordSheathe(bool isEquipping)
    {
        if (isEquipping)
            m_AudioSource.PlayOneShot(SwordDraw, 0.7f);
        else
            m_AudioSource.PlayOneShot(SwordSheathe, 0.7f);

        XInputSystem.AddVibration(0.1f, 0.3f, 0.3f);
    }

    public void PlayBowStretch()
    {
        m_AudioSource.PlayOneShot(BowReady, 1.2f);
        XInputSystem.AddVibration(0.4f, 0.2f, 0.2f);
    }

    public void PlayBowRelease()
    {
        m_AudioSource.PlayOneShot(BowPull, 1.2f);
        XInputSystem.AddVibration(0.13f, 0.5f, 0.5f);
    }

    public void PlayBowSheathe(bool isEquipping)
    {
        if (isEquipping)
            m_AudioSource.PlayOneShot(BowDraw, 2.0f);
        else
            m_AudioSource.PlayOneShot(BowSheathe, 2.0f); ;

        XInputSystem.AddVibration(0.1f, 0.3f, 0.3f);
    }

    public void PlayBlock(bool blockBreak)
    {
        if (!blockBreak)
            m_AudioSource.PlayOneShot(BlockHit, 0.7f);
        else
            m_AudioSource.PlayOneShot(BlockBreak, 0.7f);

        XInputSystem.AddVibration(0.25f, 0.8f, 0.3f);
    }

    // 1 = start, 0 = end
    public void PlayRoll(int isStart)
    {
        if (isStart == 1 && Time.time - m_LastRollStart > 0.25f)
        {
            m_AudioSource.PlayOneShot(RollStart, 0.7f);
            m_LastRollStart = Time.time;

            // Spawn a dodge-start poof particle
            ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.DODGE_DUST, transform.position, transform.rotation, new Vector3(1.5f, 1.5f, 1.5f));

            // Vibrate controller
            XInputSystem.AddVibration(0.1f, 0.6f, 0.6f);
        }
        else if (isStart == 0 && Time.time - m_LastRollEnd > 0.25f)
        {
            m_AudioSource.PlayOneShot(RollEnd, 0.38f);
            m_LastRollEnd = Time.time;

            // Spawn a dodge-end poof particle
            ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.FALL_DUST, transform.position, transform.rotation, new Vector3(1.5f, 1.5f, 1.5f));

            // Vibrate controller
            XInputSystem.AddVibration(0.18f, 0.8f, 0.8f);
        }
    }

    public void PlayFocus(bool isStart)
    {
        if (isStart)
        {
            m_AudioSource.PlayOneShot(Focus, 0.3f);
        }
        else
        {
            m_AudioSource.PlayOneShot(Unfocus, 0.3f);
        }

        XInputSystem.AddVibration(0.01f, 0.04f, 0.04f);
    }

    public void PlayMenuToggle() => m_AudioSource.PlayOneShot(MenuToggle, 0.3f);

    public void PlayMenuSelect() => m_AudioSource.PlayOneShot(MenuSelect, 0.7f);

}
