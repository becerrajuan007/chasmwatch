﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    [SerializeField]
    public Transform PlayerTransform;
    [SerializeField]
    public float CameraDistance = 30f;

    private void Awake()
    {
        if (PlayerTransform == null)
            Debug.LogError("A transform field is null! Minimap renderer will not work correctly.");
    }

    private void Update()
    {
        // We want the the camera to be positioned at the player location plus CameraDistance meters up
        transform.position = PlayerTransform.position + new Vector3(0f, CameraDistance, 0f);
    }
}
