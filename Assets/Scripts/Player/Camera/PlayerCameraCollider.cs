﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraCollider : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float MinDistance = 1.0f;
    [SerializeField]
    public float MaxDistance = 4.0f;
    [SerializeField]
    public float SmoothFactor = 10.0f;
    [SerializeField]
    public float HitDistanceCorrection = 1.0f;
    [SerializeField]
    public float FloorDistanceCorrection = 1.0f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private Vector3 m_BoomDirection;
    private float m_CurrentMaxDistance;
    private float m_BoomDistance;
    private LayerMask m_LayerMask;

    // Constants
    private readonly string[] CollisionLayers = { "Default", "Ground" };

    private void Awake()
    {
        _PlayerManager = transform.parent.parent.gameObject.GetComponent<PlayerManager>();

        m_BoomDirection = transform.localPosition.normalized;
        m_CurrentMaxDistance = MaxDistance;
        m_BoomDistance = transform.localPosition.magnitude;
        m_LayerMask = LayerMask.GetMask(CollisionLayers);
    }

    // Interface

    public void FixCameraCollisions()
    {
        // Get the desired camera position
        Vector3 desiredCameraPosition = transform.parent.TransformPoint(m_BoomDirection * m_CurrentMaxDistance);

        // Raycast towards the player, determine the boom distance
        RaycastHit hit;
        if (Physics.Linecast(transform.parent.position, desiredCameraPosition, out hit, m_LayerMask))
            m_BoomDistance = Mathf.Clamp(hit.distance - HitDistanceCorrection, MinDistance, m_CurrentMaxDistance);
        else
            m_BoomDistance = m_CurrentMaxDistance;

        // Finally, set where the camera should be placed
        transform.localPosition = Vector3.Lerp(transform.localPosition, 
            m_BoomDirection * m_BoomDistance + new Vector3(0f, FloorDistanceCorrection, 0f), 
            Time.deltaTime * SmoothFactor);
    }

    public void ZoomCamera(float distanceCoefficient)
    {
        m_CurrentMaxDistance = distanceCoefficient * MaxDistance;
    }
}
