﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTargettingSystem : MonoBehaviour
{
    // Fields
    [SerializeField]
    public Transform EyePoint;
    [SerializeField]
    public float ColliderDistance = 20f;

    // Super-script
    public PlayerManager _PlayerManager { get; private set; }

    // Members
    private MeshCollider m_FrustumCollider;
    private Mesh m_FrustumMesh;
    private HashSet<Collider> m_ColliderSet;
    private LayerMask m_LayerMask;

    // Constants
    private readonly string[] c_Tags = { "Enemy", "NPC" };
    private readonly string[] c_PossibleColliders = { "Default", "Enemy", "NPC", "Vendor" };

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();

        m_ColliderSet = new HashSet<Collider>();
        m_LayerMask = LayerMask.GetMask(c_PossibleColliders);
    }

    private void Start()
    {
        m_FrustumCollider = gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
    }

    private void OnEnable()
    {
        if (_PlayerManager && _PlayerManager._PlayerCameraMover) InitTargetingMesh();
    }

    public void InitTargetingMesh()
    {
        m_FrustumMesh = GenerateFrustumMesh();

        if (!m_FrustumCollider) m_FrustumCollider = gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
        m_FrustumCollider.sharedMesh = m_FrustumMesh;
        m_FrustumCollider.convex = true;
        m_FrustumCollider.isTrigger = true;
    }

    private Mesh GenerateFrustumMesh()
    {
        // Retrieve a reference to the camera
        Camera playerCamera = _PlayerManager._PlayerCameraMover.GetPlayerCamera();

        // Create a vertex buffer to create a mesh out of the frustum
        Vector3[] vertexBuffer;
        {
            // Calculate the position of the four camera frustum positions
            Vector3[] frustumCorners = new Vector3[4];
            playerCamera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), ColliderDistance, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);

            // Add the vertex positions to the vertex buffer
            vertexBuffer = new Vector3[]
            {
                // I don't know why the hell this works. Don't ask.
                EyePoint.position + (EyePoint.forward * 23f) - (EyePoint.right * 4f),
                EyePoint.transform.TransformVector(frustumCorners[0]),
                EyePoint.transform.TransformVector(frustumCorners[1]),
                EyePoint.transform.TransformVector(frustumCorners[2]),
                EyePoint.transform.TransformVector(frustumCorners[3])
            };
        }

        // Create an index buffer that binds these vertices together
        int[] indexBuffer = new int[]
        {
            // Sides
            0, 1, 2,
            0, 2, 3, 
            0, 3, 4,
            0, 4, 1,

            // Far plane
            3, 2, 1,
            1, 4, 3
        };

        // Create a mesh object and assign the vertices, bind them together using an index buffer
        Mesh frustumMesh = new Mesh
        {
            vertices = vertexBuffer,
            triangles = indexBuffer
        };

        // Recalc normals buffer given vertex/index buffers
        frustumMesh.RecalculateNormals();
        frustumMesh.Optimize();
        frustumMesh.name = "Frustum Mesh";

        // Return mesh
        return frustumMesh;
    }

    // Events

    private void OnTriggerEnter(Collider other)
    {
        if (CorrectTag(other.tag))
        {
            m_ColliderSet.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (CorrectTag(other.tag))
        {
            m_ColliderSet.Remove(other);
        }

        // If currently locked-on enemy is no longer in range, switch states
        if (other.transform == _PlayerManager._PlayerCameraMover.GetLastTargetedTransform() &&
            _PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON)
        {
            _PlayerManager._FocusState = PlayerManager.FocusState.FOCUSSED;
        }
    }

    // Util
    private bool CorrectTag(string tag)
    {
        foreach (string t in c_Tags)
            if (tag.Equals(t)) return true;

        return false;
    }

    // Interface

    public Transform GetClosestEnemy()
    {
        // Return null if no enemies are in collider set
        if (m_ColliderSet.Count == 0) return null;

        // Record the closest position
        Transform closestEnemyLocation = null;
        float closestEnemyDistance = float.MaxValue;

        // Iterate through enemies in vision collision box
        foreach (Collider c in m_ColliderSet)
        {
            // Perform a linecast to the enemy to see it's in view (eyepoint -> enemy)
            RaycastHit hit;
            Vector3 goal = c.transform.position + new Vector3(0f, 1f, 0f);  // Pushing up transform a bit so it doesn't hit the ground

            if (Physics.Linecast(EyePoint.position, goal, out hit, m_LayerMask) && CorrectTag(hit.collider.tag))
            {
                // Check if this ray is the closest one
                float distance = Vector3.Distance(EyePoint.position, c.transform.position);
                if (distance < closestEnemyDistance)
                {
                    closestEnemyDistance = distance;
                    closestEnemyLocation = c.transform;
                }
                
            }
        }

        // May return null if no enemies can be seen from eyepoint
        return closestEnemyLocation;
    }

    public Transform GetAdjacentEnemy(bool left, Transform lastReturnedTransform)
    {
        // Return the current targetted enemy if there is only one in the set
        if (m_ColliderSet.Count == 1) return lastReturnedTransform;

        // Adjust angle of last returned transform
        Vector3 lastReturnedAdjustedPosition = new Vector3(lastReturnedTransform.position.x, EyePoint.position.y, lastReturnedTransform.position.z);

        // Determine what sort of angle we need
        float bestAngle = left ? -float.MaxValue : float.MaxValue;
        Transform bestTransform = null;

        foreach (Collider c in m_ColliderSet)
        {
            // Skip this iteration if we are trying to target out current
            if (c.transform == lastReturnedTransform) continue;

            // Get the angle between the eye point and this collider
            Vector3 adjustedColliderPosition = new Vector3(c.transform.position.x, EyePoint.position.y, c.transform.position.z);
            float angle = Vector3.SignedAngle(EyePoint.position - lastReturnedAdjustedPosition,
                EyePoint.position - adjustedColliderPosition, 
                Vector3.up);

            if (left && angle < 0f)
            {
                // Search for the smallest negative angle
                if (angle > bestAngle)
                {
                    bestAngle = angle;
                    bestTransform = c.transform;
                }
            }
            else if (angle > 0f)
            {
                // Search for the smallest positive angle
                if (angle < bestAngle)
                {
                    bestAngle = angle;
                    bestTransform = c.transform;
                }
            }
        }

        // If we found a new target, return that. Otherwise, stay locked on the last returned target.
        if (bestTransform != null) return bestTransform;
        else return lastReturnedTransform;
    }

    public HashSet<Collider> GetColliderSet() { return m_ColliderSet; }

    public bool RemoveCollider(Collider collider)
    {
        if (m_ColliderSet.Contains(collider))
        {
            m_ColliderSet.Remove(collider);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RemoveAllColliders()
    {
        m_ColliderSet.Clear();
    }

}
