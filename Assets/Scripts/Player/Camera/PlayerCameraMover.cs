﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerCameraMover : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public Transform FollowObject;
    [SerializeField]
    public Transform PlayerHeadTransform;

    // Fields
    [SerializeField]
    public float CameraSensetivityX = 150f;
    [SerializeField]
    public float CameraSensetivityY = 120f;
    [SerializeField]
    public float VerticalClampAngle = 70f;
    [SerializeField]
    public float CameraFocusAngle = 15f;
    [SerializeField]
    public float CameraFollowSpeed = 15f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Sub-scripts
    private PlayerCameraCollider _PlayerCameraCollider;
    private PlayerTargettingSystem _PlayerTargettingSystem;

    // Components
    private Camera m_PlayerCamera;

    // Members
    private Transform m_CameraBoomTransform;
    private Vector2 m_CameraBoomEulerRotation;
    private Vector3 m_OriginalFollowPosition;
    private float m_OriginalFollowSpeed;
    private RaycastHit m_LastReturnedArrowHit;

    private Transform m_LastTargettedCharacter;

    private void Awake()
    {
        if (FollowObject == null)
            Debug.LogError("A critical field is null! Camera will not behave correctly.");

        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        _PlayerCameraCollider = GetComponentInChildren<PlayerCameraCollider>();
        _PlayerTargettingSystem = GetComponentInChildren<PlayerTargettingSystem>();

        m_PlayerCamera = GetComponentInChildren<Camera>();

        m_CameraBoomTransform = transform;
        m_CameraBoomEulerRotation = new Vector2(m_CameraBoomTransform.rotation.eulerAngles.x, m_CameraBoomTransform.rotation.eulerAngles.y);
        m_OriginalFollowPosition = FollowObject.localPosition;
        m_OriginalFollowSpeed = CameraFollowSpeed;
    }

    // Util

    private void CastBowRay()
    {
        // Cast a ray directly through the camera forward
        if (!Physics.Raycast(m_PlayerCamera.transform.position, m_PlayerCamera.transform.forward, out m_LastReturnedArrowHit))
        {
            // If we did not hit, assign the hit at a distance
            m_LastReturnedArrowHit.point = m_PlayerCamera.transform.position + (m_PlayerCamera.transform.forward * 500f);
        }

        // Aim the player's bow line at the hit position
        _PlayerManager._PlayerEquipmentHandler.AimArrowLine(m_LastReturnedArrowHit.point);
    }

    private Transform GetTargettingTransform(Transform enemyTransform)
    {
        return enemyTransform.GetComponent<EnemyAttributes>().GetTargetTransform();
    }

    // Update

    public void LateMoveCamera()
    {
        // Lerp to the desired position
        //m_CameraBoomTransform.position = Vector3.Lerp(m_CameraBoomTransform.position, FollowObject.position, CameraFollowSpeed * Time.deltaTime);
        m_CameraBoomTransform.position += (FollowObject.position - m_CameraBoomTransform.position) * CameraFollowSpeed * Time.deltaTime;

        // Fix camera collisions
        _PlayerCameraCollider.FixCameraCollisions();
    }

    // Interface

    public void RotateCamera(Vector2 input)
    {
        float rotationAngle;

        switch(_PlayerManager._FocusState)
        {
            case PlayerManager.FocusState.UNFOCUSSED:

                // TODO: Make the input transition smooth

                // Change euler rotation of camera
                m_CameraBoomEulerRotation.y += input.x * CameraSensetivityX * SettingsSystem.GetCameraSensitivity() * Time.deltaTime;
                m_CameraBoomEulerRotation.x -= input.y * CameraSensetivityY * SettingsSystem.GetCameraSensitivity() * Time.deltaTime;

                // Clamp the x rotation
                m_CameraBoomEulerRotation.x = Mathf.Clamp(m_CameraBoomEulerRotation.x, -VerticalClampAngle, VerticalClampAngle);

                break;
            case PlayerManager.FocusState.FOCUSSED:

                // Get the most recent rotation angle of the player
                rotationAngle = _PlayerManager._PlayerMover.GetActorRotation();

                // Change euler rotation of camera
                m_CameraBoomEulerRotation.y = Mathf.LerpAngle(m_CameraBoomEulerRotation.y, rotationAngle, CameraFollowSpeed * Time.deltaTime);
                m_CameraBoomEulerRotation.x = Mathf.LerpAngle(m_CameraBoomEulerRotation.x, _PlayerManager._ArmState != PlayerManager.ArmState.BOW ? CameraFocusAngle : 0f, CameraFollowSpeed * Time.deltaTime);

                break;
            case PlayerManager.FocusState.LOCKEDON:

                // Get the enemy's target transform
                Transform targetTransform = GetTargettingTransform(m_LastTargettedCharacter);

                // Calculate look rotation
                Vector3 lookAt = new Vector3(targetTransform.position.x, transform.position.y, targetTransform.position.z);
                rotationAngle = Quaternion.LookRotation(lookAt - _PlayerManager._PlayerMover.transform.position).eulerAngles.y;

                // Account for rotation to dip in for bow
                if (_PlayerManager._ArmState == PlayerManager.ArmState.BOW)
                {
                    // Get adjusted enemy and player position
                    Vector2 adjustedEnemyPosition = new Vector2(targetTransform.position.x, targetTransform.position.z);
                    Vector2 adjustedPlayerPosition = new Vector2(_PlayerManager._PlayerMover.transform.position.x, _PlayerManager._PlayerMover.transform.position.z);

                    // Offset the rotational angle based off of the enemy->hit and player->hit line segments
                    rotationAngle -= Mathf.Atan(1f / Vector2.Distance(adjustedEnemyPosition, adjustedPlayerPosition)) * Mathf.Rad2Deg;
                }

                // Adjust yaw
                float playerEnemyXAngle = Quaternion.LookRotation(targetTransform.position - new Vector3(0f, 0.7f, 0f) - _PlayerManager._PlayerCameraMover.transform.position).eulerAngles.x +
                    (_PlayerManager._ArmState != PlayerManager.ArmState.BOW ? CameraFocusAngle : 0f);

                // Rotate camera towards target immediately
                m_CameraBoomEulerRotation.y = Mathf.LerpAngle(m_CameraBoomEulerRotation.y, rotationAngle, CameraFollowSpeed * Time.deltaTime);
                m_CameraBoomEulerRotation.x = Mathf.LerpAngle(m_CameraBoomEulerRotation.x, playerEnemyXAngle, CameraFollowSpeed * Time.deltaTime);

                break;
            default:
                Debug.LogError("Invalid focus state!");
                break;
        }

        // Handle bow view
        if (_PlayerManager._ArmState == PlayerManager.ArmState.BOW)
        {
            ZoomCamera(0.3f);
            OffsetCameraFollow(1.0f);
            ScaleFollowSpeed(5.0f);

            CastBowRay();
        }
        else
        {
            ZoomCamera(1.0f);
            OffsetCameraFollow(0.0f);
            ScaleFollowSpeed(1.0f);
        }

        // Finally, update the camera rotation
        transform.rotation = Quaternion.Euler(m_CameraBoomEulerRotation.x, m_CameraBoomEulerRotation.y, 0f);
    }

    public void RotateCamera()
    {
        // NOTE: This is used for conversations, door transitions, and death

        // Set camera rotation equal to the player orientation with y dipped inwards a bit
        float rotationAngle = _PlayerManager._PlayerMover.GetActorRotation() - 12f;
        m_CameraBoomEulerRotation.y = Mathf.Lerp(m_CameraBoomEulerRotation.y, rotationAngle, 0.8f);
        m_CameraBoomEulerRotation.x = Mathf.Lerp(m_CameraBoomEulerRotation.x, 14f, 0.8f);

        // Offset the camera
        OffsetCameraFollow(0.75f);

        // Zoom the camera
        _PlayerCameraCollider.ZoomCamera(0.5f);

        // Finally, update the camera rotation
        transform.rotation = Quaternion.Euler(m_CameraBoomEulerRotation.x, m_CameraBoomEulerRotation.y, 0f);
    }

    public void ResetCamera()
    {
        // Change euler rotation of camera
        m_CameraBoomEulerRotation.y = _PlayerManager._PlayerMover.GetActorRotation();
        m_CameraBoomEulerRotation.x = CameraFocusAngle;

        // Directly alter the position of the camera
        m_CameraBoomTransform.position = FollowObject.position;
    }

    public void TargetClosestEnemy()
    {
        m_LastTargettedCharacter = _PlayerTargettingSystem.GetClosestEnemy();

        if (m_LastTargettedCharacter != null) _PlayerManager._FocusState = PlayerManager.FocusState.LOCKEDON;
    }

    public void TargetAdjacentEnemy(bool left)
    {
        m_LastTargettedCharacter = _PlayerTargettingSystem.GetAdjacentEnemy(left, m_LastTargettedCharacter);
    }

    public Camera GetPlayerCamera() { return m_PlayerCamera; }
    public Transform GetLastTargetedTransform() { return m_LastTargettedCharacter; }
    public HashSet<Collider> GetSightColliderSet() { return _PlayerTargettingSystem.GetColliderSet(); }
    public void ZoomCamera(float distanceCoefficient) { _PlayerCameraCollider.ZoomCamera(distanceCoefficient); }
    public void OffsetCameraFollow(float xOffset)
    {
        FollowObject.localPosition = m_OriginalFollowPosition + new Vector3(xOffset, 0f, 0f);
    }
    public void ScaleFollowSpeed(float speedCoefficient)
    {
        CameraFollowSpeed = m_OriginalFollowSpeed * speedCoefficient;
    }
    public Tuple<Vector3, Quaternion> GetArrowHitPosition()
    {
        return new Tuple<Vector3, Quaternion>(
            m_LastReturnedArrowHit.point,
            m_PlayerCamera.transform.rotation
        );
    }

    public Collider GetLastReturnedHitTarget() { return m_LastReturnedArrowHit.collider; }
}
