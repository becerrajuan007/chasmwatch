﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    // Sub-scripts
    public PlayerInputHandler _PlayerInputHandler { get; private set; }
    public PlayerMover _PlayerMover { get; private set; }
    public PlayerAttacker _PlayerAttacker { get; private set; }
    public PlayerCameraMover _PlayerCameraMover { get; private set; }
    public PlayerEquipmentHandler _PlayerEquipmentHandler { get; private set; }
    public PlayerAnimationHandler _PlayerAnimationHandler { get; private set; }
    public PlayerUIHandler _PlayerUIHandler { get; private set; }
    public PlayerAudioHandler _PlayerAudioHandler { get; private set; }
    public PlayerAttributes _PlayerAttributes { get; private set; }
    public PlayerCollisionHandler _PlayerCollisionHandler { get; private set; }
    public PlayerInteractionSystem _PlayerInteractionSystem { get; private set; }
    public PlayerPools _PlayerPools { get; private set; }

    // States
    public enum InteractionState { NORMAL, CONV_SCROLL, CONV_NEXT, DOOR_MOVE, SHRINE_MENU, PAUSE_MENU, DEAD }
    public enum MovementState { GROUNDED, SPRINTING, FALLING }
    public enum FocusState { UNFOCUSSED, FOCUSSED, LOCKEDON }
    public enum DodgeState { NONE, FORWARD, RIGHT, BACKWARD, LEFT }
    public enum ArmState { UNARMED, TRANSITION, SWORD_SHIELD, BOW }
    public enum AttackState
    { 
        // Default stance state
        IDLE, 

        // Light attack and buffer states
        L_A_1, L_B_1,
        L_A_2, L_B_2,
        L_A_3, L_B_3,

        // Heavy attack and buffer states
        H_A_1, H_B_1,
        H_A_2, H_B_2,

        // Blocking state
        BLOCK_IDLE, BLOCK_HIT, BLOCK_STAGGER,

        // Bow states
        BOW_DRAW, BOW_HOLD, BOW_PULL
    }

    // Member states
    public InteractionState _InteractionState { get; set; }
    public MovementState _MovementState { get; set; }
    public FocusState _FocusState { get; set; }
    public DodgeState _DodgeState { get; set; }
    public ArmState _ArmState { get; set; }
    public AttackState _AttackState { get; set; }

    // Public members
    public float InitTime { get; private set; }

    private void Awake()
    {
        _PlayerInputHandler = GetComponentInChildren<PlayerInputHandler>();
        _PlayerMover = GetComponentInChildren<PlayerMover>();
        _PlayerAttacker = GetComponentInChildren<PlayerAttacker>();
        _PlayerCameraMover = GetComponentInChildren<PlayerCameraMover>();
        _PlayerEquipmentHandler = GetComponentInChildren<PlayerEquipmentHandler>();
        _PlayerAnimationHandler = GetComponentInChildren<PlayerAnimationHandler>();
        _PlayerUIHandler = GetComponentInChildren<PlayerUIHandler>();
        _PlayerAudioHandler = GetComponentInChildren<PlayerAudioHandler>();
        _PlayerAttributes = GetComponentInChildren<PlayerAttributes>();
        _PlayerCollisionHandler = GetComponentInChildren<PlayerCollisionHandler>();
        _PlayerInteractionSystem = transform.Find("PlayerActor/Helm5/PlayerInteractionField").GetComponent<PlayerInteractionSystem>();
        _PlayerPools = GetComponentInChildren<PlayerPools>();

        _InteractionState = InteractionState.NORMAL;
        _MovementState = MovementState.GROUNDED;
        _FocusState = FocusState.UNFOCUSSED;
        _DodgeState = DodgeState.NONE;
        _ArmState = ArmState.UNARMED;
        _AttackState = AttackState.IDLE;

        InitTime = Time.time;
    }

    private void OnEnable()
    {
        InitTime = Time.time;

        if (_PlayerUIHandler) _PlayerUIHandler.PushMessage("Press Start/ESC to see controls", false);
    }

    private void Update()
    {
        // Handle inputs
        _PlayerInputHandler.HandleInputs();

        // Handle camera
        _PlayerCameraMover.LateMoveCamera();

        // Update attributes
        _PlayerAttributes.UpdateAttributes();

        // Update animator data
        _PlayerAnimationHandler.UpdateAnimatorData();

        // Update UI
        _PlayerUIHandler.UpdateUI();

        //PrintPlayerStates();    // For debug purposes
    }

    private void PrintPlayerStates()
    {
        Debug.Log(
            "Interaction: " + _InteractionState + "\n" +
            "Movement: "    + _MovementState    + "\n" +
            "Focus: "       + _FocusState       + "\n" +
            "Dodge:"        + _DodgeState       + "\n" +
            "Arm: "         + _ArmState         + "\n" +
            "Attack: "      + _AttackState
        );
    }

}
