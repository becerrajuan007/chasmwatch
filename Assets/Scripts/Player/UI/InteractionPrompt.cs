﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionPrompt : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private Canvas m_Canvas;
    private Camera m_Camera;
    private Image m_Image;

    private void Awake()
    {
        _PlayerManager = transform.parent.parent.GetComponent<PlayerManager>();

        m_Canvas = transform.parent.GetComponent<Canvas>();
        m_Image = GetComponent<Image>();
    }

    private void Start()
    {
        m_Camera = _PlayerManager._PlayerCameraMover.GetPlayerCamera();
    }

    // Interface

    public void RenderInteractionPrompt(bool show)
    {
        if (show)
        {
            // Get the transform of the closest interactable object (may be null)
            Transform interactableTransform = _PlayerManager._PlayerInteractionSystem.GetClosestInteractable();

            // Only render interaction prompt if we have a nearby interactables
            if (interactableTransform)
            {
                m_Image.enabled = true;

                // Get the interactable's prompt transform
                Transform promptTransform = GetInteractionPromptTransform(interactableTransform);

                // Display the prompt on the screen
                m_Image.transform.position = m_Camera.WorldToScreenPoint(promptTransform.position);
            }
            else
            {
                m_Image.enabled = false;
            }
        }
        else
        {
            m_Image.enabled = false;
        }
    }

    // Util
    
    private Transform GetInteractionPromptTransform(Transform interactableTransform)
    {
        return interactableTransform.gameObject.GetComponentInChildren<InteractionPromptContainer>().InteractionPromptTransform;
    }
}
