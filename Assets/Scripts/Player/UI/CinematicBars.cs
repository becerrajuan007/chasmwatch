﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CinematicBars : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float AnimationTime = 0.5f;

    // Members
    private Canvas m_Canvas;
    private RectTransform m_BlackBarTop, m_BlackBarBottom;

    private float m_LastStartTime = 0f;
    private float m_FillAmount = 0f;

    private void Awake()
    {
        // Set up parent canvas
        m_Canvas = transform.parent.gameObject.GetComponent<Canvas>();

        // Set up black bars
        GameObject barTemplate;

        barTemplate = new GameObject("BlackBarTop", typeof(Image));
        barTemplate.transform.SetParent(transform, false);
        barTemplate.GetComponent<Image>().color = Color.black;
        m_BlackBarTop = barTemplate.GetComponent<RectTransform>();
        m_BlackBarTop.anchorMin = new Vector2(0, 1);
        m_BlackBarTop.anchorMax = new Vector2(1, 1);
        m_BlackBarTop.sizeDelta = new Vector2(0, 0);

        barTemplate = new GameObject("BlackBarBottom", typeof(Image));
        barTemplate.transform.SetParent(transform, false);
        barTemplate.GetComponent<Image>().color = Color.black;
        m_BlackBarBottom = barTemplate.GetComponent<RectTransform>();
        m_BlackBarBottom.anchorMin = new Vector2(0, 0);
        m_BlackBarBottom.anchorMax = new Vector2(1, 0);
        m_BlackBarBottom.sizeDelta = new Vector2(0, 0);
    }

    public bool StepCinematicBars(bool stepStart, bool shouldFill)
    {
        // Record the last start time
        if (stepStart) m_LastStartTime = Time.time;

        // Record the lerp amount
        float lerp = Mathf.Clamp((Time.time - m_LastStartTime) / AnimationTime, 0f, 1f);

        // Calculate height
        float step;
        float barHeight = m_Canvas.GetComponent<RectTransform>().rect.height / 3;
        if (shouldFill) step = Mathf.Lerp(m_FillAmount, barHeight, Mathf.Clamp(lerp, 0f, 1f));
        else            step = Mathf.Lerp(m_FillAmount, 0f, Mathf.Clamp(lerp, 0f, 1f));

        // Change bar heights
        m_FillAmount = step;
        m_BlackBarTop.sizeDelta = new Vector2(0, m_FillAmount);
        m_BlackBarBottom.sizeDelta = new Vector2(0, m_FillAmount);

        // Are we done?
        return (lerp == 1f);
    }

}
 