﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyCounter : MonoBehaviour
{
    // Members
    private Text m_KeyCount;
    
    private void Awake()
    {
        m_KeyCount = transform.Find("KeyCount").GetComponent<Text>();
    }

    private void Start()
    {
        var uiHandler = transform.parent.GetComponent<PlayerUIHandler>();
        SetKeyCount(uiHandler._PlayerManager._PlayerAttributes.GetCurrentKeys());
    }

    // Interface

    public void ShowKeyCount(bool show)
    {
        gameObject.SetActive(show);
    }

    public void SetKeyCount(int keyCount)
    {
        m_KeyCount.text = keyCount.ToString();
    }
}
