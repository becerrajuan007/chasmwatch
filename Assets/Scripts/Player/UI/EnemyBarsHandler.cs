﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBarsHandler : MonoBehaviour
{
    // Super-script
    private PlayerUIHandler _PlayerUIHandler;

    // Constants
    private const int BAR_COUNT = 10;
    private float REG_BAR_SIZE;

    // Components
    private Transform[] m_BarTransforms;
    private RectTransform[] m_BarRects;
    private Text[] m_BarTexts;
    private Text[] m_DamageNumbers;

    private Camera m_Camera;

    private void Awake()
    {
        _PlayerUIHandler = transform.parent.GetComponent<PlayerUIHandler>();

        m_BarTransforms = new Transform[BAR_COUNT];
        m_BarRects = new RectTransform[BAR_COUNT];
        m_BarTexts = new Text[BAR_COUNT];
        m_DamageNumbers = new Text[BAR_COUNT];

        int index = 0;
        foreach (RectTransform rt in GetComponentsInChildren<RectTransform>())
        {
            if (rt.name.Equals("HealthForeground"))
            {
                m_BarRects[index] = rt;
                m_BarTransforms[index] = rt.parent.transform;

                m_BarTexts[index] = m_BarTransforms[index].Find("HealthText").GetComponent<Text>();
                m_DamageNumbers[index] = m_BarTransforms[index].Find("DamageNumber").GetComponent<Text>();

                m_DamageNumbers[index].text = "";
                m_BarTransforms[index].gameObject.SetActive(false);

                REG_BAR_SIZE = m_BarRects[index].transform.localScale.x;

                index++;
            }
        }
    }

    private void Start()
    {
        m_Camera = _PlayerUIHandler._PlayerManager._PlayerCameraMover.GetPlayerCamera();
    }

    // Util

    private EnemyAttributes GetEnemyAttributes(Collider enemyCollider) { return enemyCollider.GetComponent<EnemyAttributes>(); }
    private string ConvertToSpacedString(string str)
    {
        string retval = "";

        foreach (char c in str.ToCharArray())
        {
            retval += (c + " ");
        }

        return retval;
    }

    // Interface
    public void UpdateEnemyBars(bool show)
    {
        enabled = show;

        if (show)
        {
            // Retreive the set of colliders that the player can see
            HashSet<Collider> colliderSet = _PlayerUIHandler._PlayerManager._PlayerCameraMover.GetSightColliderSet();

            // Assign values to each of the usable health bars
            int index = 0;
            foreach (Collider c in colliderSet)
            {
                if (index >= BAR_COUNT) break;

                m_BarTransforms[index].gameObject.SetActive(true);

                // Set the position of the bar (scaling up to not lie in the middle of the enemy transform)
                EnemyAttributes attributes = GetEnemyAttributes(c);
                Vector3 targetPosition = attributes.GetTargetTransform().position;
                m_BarTransforms[index].position = m_Camera.WorldToScreenPoint(targetPosition + new Vector3(0f, 0.75f, 0f));

                // Display the enemy's name and current health to the bar
                var enemyAttrib = GetEnemyAttributes(c);

                // Display recent damage taken
                int recentDamage = attributes.GetRecentDamageTaken();
                m_DamageNumbers[index].text = recentDamage == 0 ? "" : recentDamage.ToString();

                m_BarTexts[index].text = enemyAttrib.Name;
                m_BarRects[index].transform.localScale = new Vector3(
                    REG_BAR_SIZE * ((float)enemyAttrib.GetCurrentHealth() / enemyAttrib.GetMaxHealth()),
                    m_BarRects[index].transform.localScale.y,
                    m_BarRects[index].transform.localScale.z);

                index++;
            }

            // Hide any unused health bars
            for (int i = index; i < BAR_COUNT; i++)
                m_BarTransforms[i].gameObject.SetActive(false);
        }
    }
}
