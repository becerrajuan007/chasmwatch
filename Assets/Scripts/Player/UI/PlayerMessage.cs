﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMessage : MonoBehaviour
{
    // Consts
    private const float MESSAGE_TIME = 5f;
    private readonly Color REGULAR_COLOR = new Color(1f, 1f, 1f);
    private readonly Color WARNING_COLOR = new Color(0.8f, 0.1f, 0.1f);

    // Components
    private Text m_MessageText;

    // Members
    private float m_LastMessageTime;

    private void Awake()
    {
        m_MessageText = GetComponent<Text>();

        m_MessageText.text = "";
    }

    // Interface
    
    public void UpdateMessageSystem()
    {
        m_MessageText.enabled = Time.time - m_LastMessageTime <= MESSAGE_TIME;
    }

    public void PushMessage(string message, bool isWarning)
    {
        m_LastMessageTime = Time.time;

        m_MessageText.text = message;
        m_MessageText.color = isWarning ? WARNING_COLOR : REGULAR_COLOR;
    }

}
