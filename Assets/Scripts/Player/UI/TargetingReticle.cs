﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetingReticle : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float RotationPerSecond = 180f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private Canvas m_Canvas;
    private Camera m_Camera;
    private Image m_Image;

    private float m_Rotation;

    private void Awake()
    {
        _PlayerManager = transform.parent.parent.gameObject.GetComponent<PlayerManager>();

        m_Canvas = transform.parent.gameObject.GetComponent<Canvas>();
        m_Image = GetComponent<Image>();

        m_Rotation = 0f;
    }

    private void Start()
    {
        m_Camera = _PlayerManager._PlayerCameraMover.GetPlayerCamera();
    }

    // Util

    private Transform GetTargettingTransform(Transform enemyTransform)
    {
        return enemyTransform.GetComponent<EnemyAttributes>().GetTargetTransform();
    }

    // Interface

    public void RenderTargetingReticle(bool show)
    {
        if (show)
        {
            Transform enemyTransform = _PlayerManager._PlayerCameraMover.GetLastTargetedTransform();

            if (enemyTransform)
            {
                // Swap over to the proper targetting transform
                enemyTransform = GetTargettingTransform(enemyTransform);

                m_Image.enabled = true;

                // Calculate position
                m_Image.transform.position = m_Camera.WorldToScreenPoint(enemyTransform.position);

                // Calculate rotation
                m_Rotation += RotationPerSecond * Time.deltaTime;
                m_Rotation %= 360f;
                m_Image.transform.rotation = Quaternion.Euler(0f, 0f, m_Rotation);
            }
        }
        else
        {
            m_Image.enabled = false;
        }
    }


}
