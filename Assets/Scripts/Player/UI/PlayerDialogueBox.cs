﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDialogueBox : MonoBehaviour
{
    // Fields
    [SerializeField]
    public int CharsPerSecond = 20;

    // Super-script
    private PlayerUIHandler _PlayerUIHandler;

    // Constants
    private const float c_InViewPosition = -360f;
    private const float c_OutViewPosition = -700f;

    // Members
    private RectTransform m_RectTransform;
    private Text m_NameText;
    private Text m_DialogueText;
    private Image m_NextButton;

    private bool m_RushDialogue;
    private float m_CharsThisFrame = 0f;
    private string m_CurrentDialogueText = "";
    private int m_LastDialogueStringIndex = 0;

    private void Awake()
    {
        _PlayerUIHandler = transform.parent.GetComponent<PlayerUIHandler>();

        m_RectTransform = GetComponent<RectTransform>();
        m_NameText = transform.Find("DialogueName").GetComponent<Text>();
        m_DialogueText = transform.Find("DialogueText").GetComponent<Text>();
        m_NextButton = transform.Find("DialogueContinue").GetComponent<Image>();
    }

    private void Start()
    {
        ShowBox(false);
        m_NextButton.enabled = false;
    }

    // Util

    private string GetDialogueSubstring(int charCount)
    {
        int toIndex = m_LastDialogueStringIndex + charCount;

        toIndex = Math.Min(toIndex, m_CurrentDialogueText.Length);

        string retVal = "";
        for (int i = m_LastDialogueStringIndex; i < toIndex; i++)
        {
            retVal += m_CurrentDialogueText[i];
        }
        m_LastDialogueStringIndex = toIndex;

        return retVal;
    }

    // Interface

    public bool StepText(bool stepStart)
    {
        // Record last start time
        if (stepStart)
        {
            m_NextButton.enabled = false;

            m_CharsThisFrame = 0f;
            m_LastDialogueStringIndex = 0;
            m_DialogueText.text = "";
        }

        // Determine the amount of time that has passed and get the length of the subtring to request this frame
        m_CharsThisFrame += CharsPerSecond * Time.deltaTime;
        m_DialogueText.text += GetDialogueSubstring((int)m_CharsThisFrame * (m_RushDialogue ? 2 : 1));
        m_CharsThisFrame %= 1f;

        // Are we done?
        if (m_LastDialogueStringIndex / m_CurrentDialogueText.Length == 1)
        {
            m_NextButton.enabled = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ShowBox(bool show)
    {
        m_RectTransform.anchoredPosition = show ? new Vector2(0f, c_InViewPosition) : new Vector2(0f, c_OutViewPosition);
    }

    public void SetText(string dialogue)
    {
        SetText(m_NameText.text, dialogue);
    }

    public void SetText(string name, string dialogue)
    {
        m_NameText.text = name;
        m_CurrentDialogueText = dialogue;
    }

    public void RushDialogue(bool rush) { m_RushDialogue = rush; }

}
