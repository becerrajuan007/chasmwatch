﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUIHandler : MonoBehaviour
{
    // Super-script
    public PlayerManager _PlayerManager;

    // Sub-scripts
    private CinematicBars _CinematicBars;
    private PlayerBarsHandler _PlayerBarsHandler;
    private TargetingReticle _TargetingReticle;
    private InteractionPrompt _InteractionPrompt;
    private PlayerDialogueBox _PlayerDialogueBox;
    private PlayerMinimap _PlayerMinimap;
    private EnemyBarsHandler _EnemyBarsHandler;
    private ShrineBox _ShrineBox;
    private PauseMenu _PauseMenu;
    private ArrowCounter _ArrowCounter;
    private KeyCounter _KeyCounter;
    private GameObject _BowReticle;
    private PlayerMessage _PlayerMessage;

    // Members

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        _CinematicBars = GetComponentInChildren<CinematicBars>();
        _PlayerBarsHandler = GetComponentInChildren<PlayerBarsHandler>();
        _TargetingReticle = GetComponentInChildren<TargetingReticle>();
        _InteractionPrompt = GetComponentInChildren<InteractionPrompt>();
        _PlayerDialogueBox = GetComponentInChildren<PlayerDialogueBox>();
        _PlayerMinimap = GetComponentInChildren<PlayerMinimap>();
        _EnemyBarsHandler = GetComponentInChildren<EnemyBarsHandler>();
        _ShrineBox = GetComponentInChildren<ShrineBox>();
        _PauseMenu = GetComponentInChildren<PauseMenu>();
        _ArrowCounter = GetComponentInChildren<ArrowCounter>();
        _KeyCounter = GetComponentInChildren<KeyCounter>();
        _BowReticle = transform.Find("BowReticle").gameObject;
        _PlayerMessage = transform.Find("Message").GetComponent<PlayerMessage>();

        m_LastReturnedFocusState = PlayerManager.FocusState.UNFOCUSSED;
        m_LastReturnedInteractionState = PlayerManager.InteractionState.NORMAL;
        _BowReticle.SetActive(false);
    }

    private void Start()
    {
        SetMinimapAreaName(DungeonZoneSystem.GetCurrentZone().Name);
    }

    // Update

    public void UpdateUI()
    {
        UpdateStaticUI();
        UpdateDynamicUI();
    }

    // Util

    private void UpdateStaticUI()
    {
        // Update attribute bars
        _PlayerBarsHandler.UpdateAttributeBars(m_LastReturnedInteractionState == PlayerManager.InteractionState.NORMAL);

        // Render targeting reticle
        _TargetingReticle.RenderTargetingReticle(_PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON);

        // Render interaction prompt
        _InteractionPrompt.RenderInteractionPrompt(_PlayerManager._InteractionState == PlayerManager.InteractionState.NORMAL);

        // Update orientation of minimap
        _PlayerMinimap.UpdateMinimapOrientation(_PlayerManager._InteractionState == PlayerManager.InteractionState.NORMAL);

        // Update enemy bars
        _EnemyBarsHandler.UpdateEnemyBars(true);

        // Update shrine box
        _ShrineBox.UpdateShrineBox(_PlayerManager._InteractionState == PlayerManager.InteractionState.SHRINE_MENU);

        // Update pause menu
        _PauseMenu.UpdatePauseMenu(_PlayerManager._InteractionState == PlayerManager.InteractionState.PAUSE_MENU);

        // Update arrow counter
        _ArrowCounter.ShowArrowCount(m_LastReturnedInteractionState == PlayerManager.InteractionState.NORMAL);

        // Update key counter
        _KeyCounter.ShowKeyCount(m_LastReturnedInteractionState == PlayerManager.InteractionState.NORMAL);

        // Update bow reticle
        _BowReticle.SetActive(_PlayerManager._PlayerEquipmentHandler.HasBowEquipped());

        // Update message
        _PlayerMessage.UpdateMessageSystem();
    }

    private void UpdateDynamicUI()
    {
        // Update animation UI elements
        CheckUpdateAnimUI();

        // Step cinematic bars
        if (m_AnimateBars)
        {
            if (_CinematicBars.StepCinematicBars(m_StartBarsAnimation, m_ShouldFillBars)) m_AnimateBars = false;
            m_StartBarsAnimation = false;
        }

        // Step dialogue box
        if (m_AnimateDialogueBox)
        {
            if (_PlayerDialogueBox.StepText(m_StartDialogueAnimation))
            {
                m_AnimateDialogueBox = false;
                _PlayerManager._InteractionState = PlayerManager.InteractionState.CONV_NEXT;
            }

            m_StartDialogueAnimation = false;
        }
    }

    // I'm so sorry
    private PlayerManager.FocusState m_LastReturnedFocusState;
    private PlayerManager.InteractionState m_LastReturnedInteractionState;

    private bool m_AnimateBars = false;
    private bool m_StartBarsAnimation = false;
    private bool m_ShouldFillBars = false;

    private bool m_AnimateDialogueBox = false;
    private bool m_StartDialogueAnimation = false;

    private void CheckUpdateAnimUI()
    {
        // If focus state has changed
        if (m_LastReturnedFocusState != _PlayerManager._FocusState)
        {
            m_LastReturnedFocusState = _PlayerManager._FocusState;

            // Cinematic bars
            m_AnimateBars = true;
            m_StartBarsAnimation = true;
            m_ShouldFillBars = m_LastReturnedFocusState != PlayerManager.FocusState.UNFOCUSSED;
            _PlayerManager._PlayerAudioHandler.PlayFocus(m_ShouldFillBars);
        }

        // If interaction state has changed
        else if (m_LastReturnedInteractionState != _PlayerManager._InteractionState)
        {
            m_LastReturnedInteractionState = _PlayerManager._InteractionState;

            // Cinematic bars
            m_AnimateBars = true;
            m_StartBarsAnimation = true;
            m_ShouldFillBars = m_LastReturnedInteractionState != PlayerManager.InteractionState.NORMAL;
            _PlayerManager._PlayerAudioHandler.PlayFocus(m_ShouldFillBars);

            // Dialogue box
            if (m_LastReturnedInteractionState == PlayerManager.InteractionState.CONV_SCROLL)
            {
                m_AnimateDialogueBox = true;
                m_StartDialogueAnimation = true;
            }
        }
    }

    // Interface

    public void ShowDialogueBox(bool show) { _PlayerDialogueBox.ShowBox(show); }
    public void SetDialogueText(string dialogue) { _PlayerDialogueBox.SetText(dialogue); }
    public void SetDialogueText(string name, string dialogue) { _PlayerDialogueBox.SetText(name, dialogue); }
    public void RushDialogue(bool rush) { _PlayerDialogueBox.RushDialogue(rush); }
    public void ChangeShrine(bool left) { _ShrineBox.ChangeShrine(left); }
    public DungeonShrine GetSelectedShrine() { return _ShrineBox.GetSelectedShrine(); }
    public void SetMinimapAreaName(string name) { _PlayerMinimap.SetAreaName(name); }
    public void TogglePauseMenuItem(bool down) { _PauseMenu.ToggleMenuSelection(down); }
    public void TogglePauseMenuItemSide(bool left) { _PauseMenu.ToggleMenuSelectionSide(left); }
    public void SelectMenuItem() { _PauseMenu.SelectMenuItem(); }
    public bool BacktrackMenuItem() { return _PauseMenu.BacktrackMenuItem(); }
    public void SetArrowCount(int arrowCount) { _ArrowCounter.SetArrowCount(arrowCount); }
    public void SetKeyCount(int keyCount) { _KeyCounter.SetKeyCount(keyCount); }
    public void PushMessage(string message, bool isWarning) { _PlayerMessage.PushMessage(message, isWarning); }

}
