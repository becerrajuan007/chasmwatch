﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    // Super-script
    private PlayerUIHandler _PlayerUIHandler;

    // Constants
    private const float c_OutViewPosition = 1000f;
    private const string MAIN_MENU_PATH = "Scenes/MainMenu";

    // Members
    private RectTransform m_RectTransform;
    private RectTransform m_PointerTransform;
    private GameObject m_Controls;

    private List<Vector2> m_MenuButtons;
    private int m_MenuIndex;
    private bool m_PreviouslyActivated;

    private Transform m_SubMenusContainer;
    private UnityEngine.Object m_SubMenuPrefab;
    private static SubMenuAbs m_ActiveSubMenu;
    private SubMenuAbs m_DebugSubMenu;
    private SubMenuAbs m_OptionsSubMenu;

    private void Awake()
    {
        _PlayerUIHandler = transform.parent.GetComponent<PlayerUIHandler>();

        m_RectTransform = GetComponent<RectTransform>();
        m_PointerTransform = transform.Find("MenuPointer").GetComponent<RectTransform>();
        m_Controls = transform.Find("Controls").gameObject;

        m_MenuButtons = new List<Vector2>();

        var menuObjects = transform.Find("MenuItems");
        foreach (RectTransform rt in menuObjects.GetComponentInChildren<RectTransform>())
        {
            if (rt != menuObjects.GetComponent<RectTransform>())
                m_MenuButtons.Add(rt.anchoredPosition);
        }

        InitSubMenusAwake();
    }

    private void Start()
    {
        InitSubMenusStart();
    }

    private void InitSubMenusAwake()
    {
        m_SubMenuPrefab = (GameObject)Resources.Load("Menus/SubMenu");
        m_SubMenusContainer = transform.Find("SubMenus");

        m_ActiveSubMenu = null;

        // Allocate menus
        m_DebugSubMenu = AllocateSubMenu();
        m_DebugSubMenu.InitSubMenu("Debug");

        m_OptionsSubMenu = AllocateSubMenu();
        m_OptionsSubMenu.InitSubMenu("Options");
    }

    private void InitSubMenusStart()
    {
        // Debug items
        m_DebugSubMenu.AddEventOption("Toggle Godmode", _PlayerUIHandler._PlayerManager._PlayerAttributes.ToggleGodmode);
        m_DebugSubMenu.AddEventOption("Open Final Boss Door", FinalBossBlock.ActivateAllLights);
        m_DebugSubMenu.AddEventOption("Clear Enemies in Room", DungeonZoneSystem.ClearRoomEnemies);
        m_DebugSubMenu.AddEventOption("Open All Dungeon Gates", DungeonZoneSystem.OpenAllDungeonGates);
        m_DebugSubMenu.AddEventOption("Unlock All Shrines", DungeonZoneSystem.UnlockAllShrines);

        // Options items
        // Screen
        m_OptionsSubMenu.AddSliderOption("Resolution", SettingsSystem.SetResolution, SettingsSystem.GetResolution,
            SettingsSystem.GetResolutionAsString, GetResolutionStrings(SettingsSystem.GetAllResolutions()));
        m_OptionsSubMenu.AddCheckboxOption("Fullscreen", SettingsSystem.SetFullscreen, SettingsSystem.IsFullscreen);
        // Input
        m_OptionsSubMenu.AddSliderOption("Camera Sensitivity", SettingsSystem.SetCameraSensitivity, SettingsSystem.GetCameraSensitivity, 
            SettingsSystem.GetCameraSensitivityRange());
        m_OptionsSubMenu.AddCheckboxOption("Rumble", SettingsSystem.SetEnableRumble, SettingsSystem.IsRumble);
        // Audio
        m_OptionsSubMenu.AddSliderOption("Master Volume", SettingsSystem.SetMasterVolume, SettingsSystem.GetMasterVolume, 
            SettingsSystem.GetVolumeRange());
        //m_OptionsSubMenu.AddSliderOption("Ambient Volume", SettingsSystem.SetAmbientVolume, SettingsSystem.GetAmbientVolume,
        //    SettingsSystem.GetVolumeRange());
        //m_OptionsSubMenu.AddSliderOption("Effects Volume", SettingsSystem.SetEffectsVolume, SettingsSystem.GetEffectsVolume,
        //    SettingsSystem.GetVolumeRange());
    }

    private string[] GetResolutionStrings(Vector2[] resolutions)
    {
        string[] strings = new string[resolutions.Length];

        for (int i = 0; i < resolutions.Length; i++)
        {
            strings[i] = SettingsSystem.GetResolutionAsString(resolutions[i]);
        }

        return strings;
    }

    private SubMenuAbs AllocateSubMenu()
    {
        var debugInstance = Instantiate(m_SubMenuPrefab) as GameObject;
        debugInstance.transform.SetParent(m_SubMenusContainer);
        return debugInstance.GetComponent<SubMenuAbs>();
    }

    // Update
    public void UpdatePauseMenu(bool show)
    {
        if (show)
        {
            // Show box
            m_RectTransform.anchoredPosition = Vector2.zero;

            if (!m_PreviouslyActivated)
            {
                m_MenuIndex = 0;
                UpdateMenuPointerPosition();
            }
        }
        else
        {
            // Hide box
            m_RectTransform.anchoredPosition = new Vector2(0f, c_OutViewPosition);

            // Hide the submenu
            if (m_ActiveSubMenu)
            {
                m_ActiveSubMenu.ShowSubMenu(false);
                m_ActiveSubMenu = null;
            }
        }

        m_PreviouslyActivated = show;
    }

    public void ToggleMenuSelection(bool down)
    {
        // If the submenu is active, propegate input to it
        if (m_ActiveSubMenu)
        {
            m_ActiveSubMenu.ToggleMenuSelection(down);
            return;
        }

        if (down)
        {
            m_MenuIndex++;
            if (m_MenuIndex > m_MenuButtons.Count - 1) m_MenuIndex = 0;
        }
        else
        {
            m_MenuIndex--;
            if (m_MenuIndex < 0) m_MenuIndex = m_MenuButtons.Count - 1;
        }

        // Play audio
        _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuToggle();

        UpdateMenuPointerPosition();
    }

    public void ToggleMenuSelectionSide(bool left)
    {
        if (m_ActiveSubMenu)
            m_ActiveSubMenu.ToggleMenuSelectionSide(left);
    }

    public void SelectMenuItem()
    {
        if (m_ActiveSubMenu)
        {
            m_ActiveSubMenu.SelectMenuItem();
            return;
        }

        switch (m_MenuIndex)
        {
            case 0:
                // Continue
                _PlayerUIHandler._PlayerManager._PlayerInputHandler.FlushInputs();
                _PlayerUIHandler._PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
                break;
            case 1:
                // Options
                m_ActiveSubMenu = m_OptionsSubMenu;
                m_OptionsSubMenu.ShowSubMenu(true);
                ShowControls(false);
                break;
            case 2:
                // Debug
                m_ActiveSubMenu = m_DebugSubMenu;
                m_DebugSubMenu.ShowSubMenu(true);
                ShowControls(false);
                break;
            case 3:
                // Main Menu
                SceneManager.LoadScene(MAIN_MENU_PATH);
                break;
            case 4:
                // Quit Game
                Application.Quit();
                break;
            default:
                Debug.LogError("Invalid menu index! (" + m_MenuIndex + ")");
                break;
        }

        // Play audio
        _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuSelect();
    }

    public bool BacktrackMenuItem()
    {
        if (m_ActiveSubMenu)
        {
            // Save settings to disk if we exited the options menu
            if (m_ActiveSubMenu == m_OptionsSubMenu)
                SettingsSystem.SaveSettings();

            m_ActiveSubMenu.ShowSubMenu(false);
            m_ActiveSubMenu = null;
            ShowControls(true);

            // Play audio
            _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuSelect();

            return false;
        }

        return true;
    }

    public static void SetSubMenuMessageText(string text)
    {
        if (m_ActiveSubMenu)
            m_ActiveSubMenu.SetMessageText(text);
    }

    // Util

    private void UpdateMenuPointerPosition()
    {
        m_PointerTransform.anchoredPosition = new Vector2(m_PointerTransform.anchoredPosition.x,
                                                          m_MenuButtons[m_MenuIndex].y);
    }

    private void ShowControls(bool show) => m_Controls.SetActive(show);
}
