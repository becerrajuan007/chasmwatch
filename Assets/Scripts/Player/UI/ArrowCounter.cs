﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowCounter : MonoBehaviour
{
    // Constants
    private readonly Color OUT_OF_ARROWS = new Color(0.9f, 0.15f, 0.08f);
    private readonly Color INTERMEDIATE_ARROWS = new Color(1f, 1f, 1f);
    private readonly Color FULL_OF_ARROWS = new Color(0.12f, 0.9f, 0.16f);

    // Members
    private Text m_ArrowCount;
    private int m_MaxArrowCount;

    private void Awake()
    {
        m_ArrowCount = transform.Find("ArrowCount").GetComponent<Text>();
    }

    private void Start()
    {
        var uiHandler = transform.parent.GetComponent<PlayerUIHandler>();
        m_MaxArrowCount = uiHandler._PlayerManager._PlayerAttributes.GetMaxArrows();

        SetArrowCount(m_MaxArrowCount);
    }

    public void ShowArrowCount(bool show)
    {
        gameObject.SetActive(show);
    }

    // Interface
    public void SetArrowCount(int arrowCount)
    {
        m_ArrowCount.text = arrowCount.ToString();

        // Set color of prompt
        if (arrowCount == 0)
            m_ArrowCount.color = OUT_OF_ARROWS;
        else if (arrowCount == m_MaxArrowCount)
            m_ArrowCount.color = FULL_OF_ARROWS;
        else
            m_ArrowCount.color = INTERMEDIATE_ARROWS;
    }
}
