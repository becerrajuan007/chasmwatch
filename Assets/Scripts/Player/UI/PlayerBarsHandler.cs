﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBarsHandler : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Constants
    private readonly Vector3 c_DefaultFill = new Vector3(2.0f, 0.2f, 1.0f);

    // Components
    private RectTransform m_HealthRect, m_StaminaRect;
    private Text m_HealthText, m_StaminaText;

    // Structs
    private struct AttributeData
    {
        public int CH, CS; // Current
        public int MH, MS; // Max

        public bool Equals(AttributeData other)
        {
            return (CH == other.CH &&
                    CS == other.CS &&
                    MH == other.MH &&
                    MS == other.MS);
        }
    }

    // Members
    private AttributeData m_LastRecordedAttributes;

    private void Awake()
    {
        _PlayerManager = transform.parent.parent.gameObject.GetComponent<PlayerManager>();

        // Search for rects
        RectTransform[] rectList = GetComponentsInChildren<RectTransform>();
        foreach (RectTransform rt in rectList)
        {
            switch(rt.name)
            {
                case "HealthForeground":
                    m_HealthRect = rt;
                    break;
                case "StaminaForeground":
                    m_StaminaRect = rt;
                    break;
                default:
                    break;
            }
        }

        // Search for texts
        Text[] textList = GetComponentsInChildren<Text>();
        foreach (Text t in textList)
        {
            switch (t.name)
            {
                case "HealthText":
                    m_HealthText = t;
                    break;
                case "StaminaText":
                    m_StaminaText = t;
                    break;
                default:
                    break;
            }
        }
    }

    private void Start()
    {
        m_LastRecordedAttributes = new AttributeData();
        m_LastRecordedAttributes.CH = _PlayerManager._PlayerAttributes.GetCurrentHealth();
        m_LastRecordedAttributes.CS = _PlayerManager._PlayerAttributes.GetCurrentStamina();
        m_LastRecordedAttributes.MH = _PlayerManager._PlayerAttributes.GetMaxHealth();
        m_LastRecordedAttributes.MS = _PlayerManager._PlayerAttributes.GetMaxStamina();

        UpdateHealthBar();
        UpdateStaminaBar();
    }

    // Util
    private void UpdateHealthBar()
    {
        m_HealthRect.transform.localScale = new Vector3(
            c_DefaultFill.x * ((float)m_LastRecordedAttributes.CH / m_LastRecordedAttributes.MH),
            c_DefaultFill.y,
            c_DefaultFill.z);

        m_HealthText.text = m_LastRecordedAttributes.CH + " / " + m_LastRecordedAttributes.MH;
    }

    private void UpdateStaminaBar()
    {
        m_StaminaRect.transform.localScale = new Vector3(
            c_DefaultFill.x * ((float)m_LastRecordedAttributes.CS / m_LastRecordedAttributes.MS),
            c_DefaultFill.y,
            c_DefaultFill.z);

        m_StaminaText.text = m_LastRecordedAttributes.CS + " / " + m_LastRecordedAttributes.MS;
    }

    // Interface
    public void UpdateAttributeBars(bool show)
    {
        // Record current data
        AttributeData currentAttributes = new AttributeData();
        currentAttributes.CH = _PlayerManager._PlayerAttributes.GetCurrentHealth();
        currentAttributes.CS = _PlayerManager._PlayerAttributes.GetCurrentStamina();
        currentAttributes.MH = _PlayerManager._PlayerAttributes.GetMaxHealth();
        currentAttributes.MS = _PlayerManager._PlayerAttributes.GetMaxStamina();

        if (!currentAttributes.Equals(m_LastRecordedAttributes))
        {
            // Update UI
            m_LastRecordedAttributes = currentAttributes;

            UpdateHealthBar();
            UpdateStaminaBar();
        }

        // Show/hide bars
        gameObject.SetActive(show);
    }
}
