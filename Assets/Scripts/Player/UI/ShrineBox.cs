﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShrineBox : MonoBehaviour
{
    // Super-script
    private PlayerUIHandler _PlayerUIHandler;

    // Constants
    private const float c_OutViewPosition = -700f;

    // Members
    private RectTransform m_RectTransform;
    private Text m_ShrineName;

    private List<DungeonShrine> m_VisitedShrines;
    private int m_SelectedShrineIndex;
    private bool m_FirstActivate;

    private void Awake()
    {
        _PlayerUIHandler = transform.parent.GetComponent<PlayerUIHandler>();

        m_RectTransform = GetComponent<RectTransform>();
        m_ShrineName = transform.Find("ShrineName").GetComponent<Text>();
    }

    private void Start()
    {
        m_VisitedShrines = _PlayerUIHandler._PlayerManager._PlayerAttributes.GetVisitedShrines();
        m_SelectedShrineIndex = 0;
        m_FirstActivate = true;
    }

    // Update

    public void UpdateShrineBox(bool show)
    {
        if (show)
        {
            // Show box
            m_RectTransform.anchoredPosition = Vector2.zero;

            if (m_FirstActivate)
            {
                m_FirstActivate = false;
                m_VisitedShrines = _PlayerUIHandler._PlayerManager._PlayerAttributes.GetVisitedShrines();
                m_SelectedShrineIndex = _PlayerUIHandler._PlayerManager._PlayerAttributes.GetLastVisitedShrineIndex();
            }

            SetShrineName(m_VisitedShrines[m_SelectedShrineIndex].ShrineName);

        }
        else
        {
            // Hide box
            m_RectTransform.anchoredPosition = new Vector2(0f, c_OutViewPosition);
            m_FirstActivate = true;
        }
    }

    // Util

    private void SetShrineName(string name)
    {
        m_ShrineName.text = name;
    }

    // Interface

    public void ChangeShrine(bool left)
    {
        if (left)
        {
            if (m_SelectedShrineIndex - 1 > 0)
                m_SelectedShrineIndex--;
            else
                m_SelectedShrineIndex = m_VisitedShrines.Count - 1;
        }
        else
        {
            if (m_SelectedShrineIndex + 1 < m_VisitedShrines.Count)
                m_SelectedShrineIndex++;
            else
                m_SelectedShrineIndex = 0;
        }
    }

    public DungeonShrine GetSelectedShrine()
    {
        return m_VisitedShrines[m_SelectedShrineIndex];
    }
}
