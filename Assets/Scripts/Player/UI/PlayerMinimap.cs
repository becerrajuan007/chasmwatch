﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMinimap : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public Transform CameraBoomTransform;

    // Super-script
    private PlayerUIHandler _PlayerUIHandler;

    // Members
    private Transform m_CompassTransform;
    private Text m_AreaText;

    private void Awake()
    {
        if (CameraBoomTransform == null)
            Debug.LogError("Camera boom transform field is null! Minimap will not work correctly.");

        _PlayerUIHandler = transform.parent.GetComponent<PlayerUIHandler>();

        m_CompassTransform = transform.Find("Compass");

        m_AreaText = transform.Find("AreaName").GetComponent<Text>();
    }

    // Interface

    public void UpdateMinimapOrientation(bool show)
    {
        gameObject.SetActive(show);

        if (show)
            m_CompassTransform.localRotation = Quaternion.Euler(0f, 0f, CameraBoomTransform.rotation.eulerAngles.y - 90f);
    }

    public void SetAreaName(string name)
    {
        m_AreaText.text = name;
    }
}
