﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallingBehaviour : StateMachineBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    I put the audio here and it scared the shit out of me
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Play fall sound effect
        // This time check is in place in order to avoid playing annoying fall sound effect on startup
        if (Time.time > _PlayerManager.InitTime + 1f) _PlayerManager._PlayerAudioHandler.PlayRoll(0);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
