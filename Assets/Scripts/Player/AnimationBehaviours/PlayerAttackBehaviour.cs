﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackBehaviour : StateMachineBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // If player is sprinting, revert to grounded
        if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
            _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

        // Spend stamina
        if (IsLightAttack())
            _PlayerManager._PlayerAttributes.LightAttackChangeStamina();
        else if (IsHeavyAttack())
            _PlayerManager._PlayerAttributes.HeavyAttackChangeStamina();
        else
            Debug.LogError("Invalid attack state! (" + _PlayerManager._AttackState + ")");

        // Generate a unique attack ID (so this attack doesn't collide multiple times on a single enemy)
        _PlayerManager._PlayerCollisionHandler.GenerateAttackID();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float progress = stateInfo.normalizedTime % 1;

        // The hitbox is only enabled after and before the first and last 20% respectively
        _PlayerManager._PlayerCollisionHandler.EnableSwordHitbox(progress > 0.2f && progress < 0.8f);

        // If we buffered during attack 1 or 2, go straight to the next corresponding attack in the combo
        if (progress >= 0.75 && _PlayerManager._PlayerAttacker.BufferedDuringAttack())
        {
            // Trigger a StateExit
            if (IsLightAttack())
                _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
            else if (IsHeavyAttack())
                _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
            else
                Debug.LogError("Invalid attack state!");
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_PlayerManager._PlayerAttacker.BufferedDuringAttack())
        {
            // Reset the attack buffer
            _PlayerManager._PlayerAttacker.ResetAttackBuffer();

            // Skip the buffer and go to the next state
            switch (_PlayerManager._AttackState)
            {
                case PlayerManager.AttackState.L_A_1:
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_2;
                    break;
                case PlayerManager.AttackState.L_A_2:
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_3;
                    break;
                case PlayerManager.AttackState.H_A_1:
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_A_2;
                    break;
            }
        }
        else
        {
            // This transitions players from an attack state to a buffer state
            switch (_PlayerManager._AttackState)
            {
                case PlayerManager.AttackState.IDLE:
                    // Ignore
                    break;
                case PlayerManager.AttackState.L_A_1:
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_B_1;
                    break;
                case PlayerManager.AttackState.L_A_2:
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_B_2;
                    break;
                case PlayerManager.AttackState.L_A_3:
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_B_3;
                    break;
                case PlayerManager.AttackState.H_A_1:
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_B_1;
                    break;
                case PlayerManager.AttackState.H_A_2:
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_B_2;
                    break;
                default:
                    Debug.LogError("Invalid combo buffer transition! (" + _PlayerManager._AttackState + ")");
                    break;
            }
        }

        // Disable sword hitbox
        _PlayerManager._PlayerCollisionHandler.EnableSwordHitbox(false);
    }

    // Util

    private bool IsLightAttack()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState == PlayerManager.AttackState.L_A_1 ||
               attackState == PlayerManager.AttackState.L_A_2 ||
               attackState == PlayerManager.AttackState.L_A_3;
    }

    private bool IsHeavyAttack()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState == PlayerManager.AttackState.H_A_1 ||
               attackState == PlayerManager.AttackState.H_A_2;
    }
}
