﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDodgeBehaviour : StateMachineBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private bool m_SlowedDownDodge;

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();

        m_SlowedDownDodge = false;
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_SlowedDownDodge = false;

        // Enable invincibility frames
        _PlayerManager._PlayerCollisionHandler.EnableBodyHitbox(false);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // The buffer at the end of the dodge. Player is slow and can be attacked here.
        if (!m_SlowedDownDodge && stateInfo.normalizedTime % 1 >= 0.8f)
        {
            m_SlowedDownDodge = true;
            _PlayerManager._PlayerMover.SlowDownDodge(true);

            // Disable invincibility frames
            _PlayerManager._PlayerCollisionHandler.EnableBodyHitbox(true);
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_SlowedDownDodge = false;
        _PlayerManager._PlayerInputHandler.EndDodge();
        _PlayerManager._PlayerMover.SlowDownDodge(false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
