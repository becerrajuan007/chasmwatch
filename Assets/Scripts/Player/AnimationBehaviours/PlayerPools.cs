﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerPools : MonoBehaviour
{
    // Fields
    [SerializeField]
    public GameObject ArrowPrefab;
    [SerializeField]
    public int ArrowPoolSize = 5;

    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private GameObject[] m_ArrowArray;
    private Queue<Transform> m_ArrowQueue;

    private void Awake()
    {
        _PlayerManager = transform.parent.GetComponent<PlayerManager>();

        m_ArrowArray = new GameObject[ArrowPoolSize];
        m_ArrowQueue = new Queue<Transform>();

        for (int i = 0; i < ArrowPoolSize; i++)
        {
            m_ArrowArray[i] = Instantiate(ArrowPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            Transform arrowObjTransform = m_ArrowArray[i].GetComponent<Transform>();
            arrowObjTransform.parent = transform;

            m_ArrowQueue.Enqueue(arrowObjTransform);
            m_ArrowArray[i].SetActive(false);
        }
    }

    // Interface

    public Transform SpawnArrow(Vector3 position, Quaternion rotation)
    {
        Transform spawnedArrow = m_ArrowQueue.Dequeue();

        spawnedArrow.gameObject.SetActive(true);
        spawnedArrow.position = position;
        spawnedArrow.rotation = rotation;

        m_ArrowQueue.Enqueue(spawnedArrow);

        return spawnedArrow;
    }

}
