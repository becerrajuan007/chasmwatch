﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArrow : MonoBehaviour
{
    // Consts 
    private const float ARROW_LIFETIME = 5.0f;

    // Members
    private float m_LastEnableTime = 0f;

    private void OnEnable()
    {
        m_LastEnableTime = Time.time;
    }

    private void Update()
    {
        if (Time.time >= m_LastEnableTime + ARROW_LIFETIME)
            gameObject.SetActive(false);
    }
}
