﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBowDrawBehavior : StateMachineBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private HumanBodyBones m_Spine;

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();

        m_Spine = HumanBodyBones.Spine;
        //HumanBodyBones leftWeapon = 
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_PlayerManager._AttackState == PlayerManager.AttackState.BOW_DRAW &&
            !stateInfo.IsName("Bow_Attack_Draw"))
        {
            _PlayerManager._AttackState = PlayerManager.AttackState.BOW_HOLD;
            _PlayerManager._PlayerEquipmentHandler.ShowArrowLine(true);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Quaternion cameraRotation = _PlayerManager._PlayerCameraMover.GetPlayerCamera().transform.rotation;
    //    animator.bodyRotation = cameraRotation;
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    _PlayerManager._AttackState = PlayerManager.AttackState.BOW_HOLD;
    //    _PlayerManager._PlayerEquipmentHandler.ShowArrowLine(true);
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Quaternion cameraRotation = _PlayerManager._PlayerCameraMover.GetPlayerCamera().transform.rotation;

        // Get proper spine rotation
        Quaternion spineRotation;

        if (IsInAtackState())
            spineRotation = Quaternion.Euler(0f, -cameraRotation.eulerAngles.x, 0f);
        else
            spineRotation = Quaternion.Euler(0f, 0f, cameraRotation.eulerAngles.x);

        animator.SetBoneLocalRotation(m_Spine, spineRotation);
    }

    // Util

    private bool IsInAtackState()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState == PlayerManager.AttackState.BOW_DRAW ||
               attackState == PlayerManager.AttackState.BOW_HOLD ||
               attackState == PlayerManager.AttackState.BOW_PULL;
    }
}
