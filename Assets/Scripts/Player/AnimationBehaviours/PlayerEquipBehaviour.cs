﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipBehaviour : StateMachineBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private PlayerManager.ArmState m_StartState;
    private bool m_SwappedModels;

    private void Awake()
    {
        _PlayerManager = FindObjectOfType<PlayerManager>();

        m_SwappedModels = false;
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_StartState = _PlayerManager._ArmState;

        _PlayerManager._ArmState = PlayerManager.ArmState.TRANSITION;

        m_SwappedModels = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Perform a model swap / sound effect / state change half way through the animation
        if (!m_SwappedModels && stateInfo.normalizedTime % 1 >= 0.5f)
        {
            // What are we switching to?
            switch (_PlayerManager._PlayerEquipmentHandler.GetToState())
            {
                case PlayerManager.ArmState.UNARMED:
                    _PlayerManager._PlayerEquipmentHandler.EquipSwordShield(false);
                    _PlayerManager._PlayerEquipmentHandler.EquipBowArrow(false);
                    
                    if (_PlayerManager._PlayerEquipmentHandler.GetFromState() == PlayerManager.ArmState.SWORD_SHIELD)
                        _PlayerManager._PlayerAudioHandler.PlaySwordSheathe(false);
                    else if (_PlayerManager._PlayerEquipmentHandler.GetFromState() == PlayerManager.ArmState.BOW)
                        _PlayerManager._PlayerAudioHandler.PlayBowSheathe(false);

                    break;
                case PlayerManager.ArmState.SWORD_SHIELD:
                    _PlayerManager._PlayerEquipmentHandler.EquipSwordShield(true);
                    _PlayerManager._PlayerEquipmentHandler.EquipBowArrow(false);
                    _PlayerManager._PlayerAudioHandler.PlaySwordSheathe(true);
                    break;
                case PlayerManager.ArmState.BOW:
                    _PlayerManager._PlayerEquipmentHandler.EquipSwordShield(false);
                    _PlayerManager._PlayerEquipmentHandler.EquipBowArrow(true);
                    _PlayerManager._PlayerAudioHandler.PlayBowSheathe(true);
                    break;
                default:
                    Debug.LogError("Invalid equip state!");
                    break;
            }

            _PlayerManager._ArmState = _PlayerManager._PlayerEquipmentHandler.GetToState();

            m_SwappedModels = true;
        }
    }
}
