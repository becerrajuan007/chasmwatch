﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionHandler : MonoBehaviour
{
    // Fields
    [SerializeField]
    public int AttackIDSetSize = 3;
    [SerializeField]
    public float BlockingAngle = 120f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Sub-scripts
    public WeaponCollider _WeaponCollider;

    // Components
    private CapsuleCollider m_BodyHitbox;

    // Members
    private Queue<int> m_AttackIDSet;
    private bool m_IsInvincible;

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        _WeaponCollider = FindDeepChild(transform, "weaponShield_r").GetComponent<WeaponCollider>();

        m_BodyHitbox = GetComponent<CapsuleCollider>();

        m_AttackIDSet = new Queue<int>();
        m_IsInvincible = false;
    }

    // Util

    private Transform FindDeepChild(Transform parent, string name)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(parent);

        while (queue.Count > 0)
        {
            Transform transform = queue.Dequeue();
            if (transform.name == name)
                return transform;
            foreach (Transform t in transform)
                queue.Enqueue(t);
        }

        Debug.LogError(name + " could not be found.");
        return null;
    }

    private void PushAttackID(int attackID)
    {
        m_AttackIDSet.Enqueue(attackID);

        if (m_AttackIDSet.Count > AttackIDSetSize)
            m_AttackIDSet.Dequeue();
    }

    private bool CheckAttackID(int attackID)
    {
        return m_AttackIDSet.Contains(attackID);
    }

    private bool IsBlocking()
    {
        return _PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_IDLE ||
               _PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_HIT;
    }

    private bool CheckBlockingAngle(Transform source)
    {
        // Adjust the position of this transform to match collision handler
        Vector3 adjustedPosition = new Vector3(source.position.x, transform.position.y, source.position.z);

        // Determine the angle of where the hit came from
        float angle = Vector3.SignedAngle(-transform.forward, transform.position - adjustedPosition, Vector3.up);

        // If the angle was within our blocking threshold, then the shield was at the correct angle
        return Mathf.Abs(angle) <= (BlockingAngle / 2);
    }

    // Interface
    public Collider GetBodyHitbox() { return m_BodyHitbox; }
    public void EnableBodyHitbox(bool isEnabled)
    {
        m_IsInvincible = !isEnabled;
    }
    public void EnableSwordHitbox(bool isEnabled) { _WeaponCollider.EnableWeaponHitbox(isEnabled); }
    public void GenerateAttackID() { _WeaponCollider.GenerateAttackID(); }

    public bool ReceiveDamageSignal(float damage, float poise, int attackID, Transform source)
    {
        if (!CheckAttackID(attackID))
        {
            PushAttackID(attackID);

            // Are we dodging?
            if (!m_IsInvincible)
            {
                // Are we blocking?
                if (IsBlocking() &&
                    _PlayerManager._PlayerAttributes.PeekCanBlock(poise) &&
                    CheckBlockingAngle(source))
                {
                    // Block was broken
                    if (_PlayerManager._PlayerAttributes.BlockAttack(-damage, -poise))
                    {
                        _PlayerManager._PlayerAudioHandler.PlayBlock(true);

                        _PlayerManager._AttackState = PlayerManager.AttackState.BLOCK_STAGGER;
                    }

                    // Sustained block
                    else
                    {
                        _PlayerManager._PlayerAudioHandler.PlayBlock(false);

                        _PlayerManager._AttackState = PlayerManager.AttackState.BLOCK_HIT;
                    }

                    ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.HIT_SPARK, transform.position + (transform.up * 0.5f) + (transform.forward * 0.25f),
                        transform.rotation, Vector3.one);
                }
                // Take regular damage
                else
                {
                    _PlayerManager._PlayerAttributes.ChangeHealth(-damage);
                    _PlayerManager._PlayerAttributes.ChangePoise(-poise);
                    _PlayerManager._PlayerAudioHandler.PlaySwordHit();

                    _PlayerManager._PlayerMover.ApplyForceToPlayer(source, 10.0f);

                    ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.HIT_SPARK, transform.position + (transform.up * 0.5f),
                        transform.rotation, Vector3.one);
                }
            }

            return true;
        }
        return false;
    }
}
