﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class PlayerMover : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public Transform GroundCasterTransform;

    // Fields
    [SerializeField]
    public float MaxXZSpeed = 4.0f;
    [SerializeField]
    public float MaxYSpeed = 8.0f;
    [Range(0.01f, 20.0f)]
    public float AccelerationFactor = 10.0f;
    [SerializeField]
    public float TurnSmoothTime = 0.15f;
    [Range(0.01f, 1.0f)]
    public float FocusMovementScale = 0.75f;
    [Range(0.5f, 2.0f)]
    public float DodgeMovementScale = 1.3f;
    [Range(0.01f, 2.0f)]
    public float GroundCastDistance = 0.5f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Constants
    private LayerMask c_GroundMask;

    // Components
    private CharacterController m_CharacterController;

    // Members
    private Vector3 m_DesiredVelocity;
    private Vector3 m_Velocity;
    private float m_TurnSmoothVelocity;
    private LayerMask m_LayerMask;
    private bool m_DodgeSlowed;

    private void Awake()
    {
        if (GroundCasterTransform == null) Debug.LogError("Ground caster is null! Grounds checking will not work correctly.");

        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        c_GroundMask = LayerMask.GetMask(new string[] { "Ground" });

        m_Velocity.y = -1.0f;
        m_LayerMask = LayerMask.GetMask(new string[] { "Ground", "Enemy" });
        m_DodgeSlowed = false;

        m_CharacterController = GetComponent<CharacterController>();
    }

    // Update

    // ----------------------------------------------------------------------------------------------------------------
    // In player control ----------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------------------

    public void MovePlayer(Vector2 input)
    {
        // Orient and move (changes m_Velocity)
        UpdateSprintState(input);
        OrientCharacter(input);
        MoveCharacter(input);
        ApplyGravity();

        // Finally, move the player based on modified velocity (from previous method calls)
        //m_Velocity = Vector3.Lerp(m_Velocity, m_DesiredVelocity, AccelerationFactor);
        m_Velocity += (m_DesiredVelocity - m_Velocity) * AccelerationFactor * Time.deltaTime;
        m_CharacterController.Move(m_Velocity * Time.deltaTime);
    }

    private void UpdateSprintState(Vector2 input)
    {
        // If the player is in a sprinting state but
        //      - does not have the required stamina
        //      - is no longer inputing on the left stick
        // Then stop sprinting
        if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
            if (!_PlayerManager._PlayerAttributes.SprintChangeStamina() || input == Vector2.zero)
                _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;
    }

    private void OrientCharacter(Vector2 input)
    {
        // Regular orientation
        if (input != Vector2.zero &&
            _PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
            _PlayerManager._FocusState == PlayerManager.FocusState.UNFOCUSSED &&
            _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
            _PlayerManager._ArmState != PlayerManager.ArmState.BOW &&
            !IsAttacking())
        {
            // Calculate the target rotation angle based on the transform of the camera
            float targetRotation = Mathf.Atan2(input.x, input.y) *
                Mathf.Rad2Deg + _PlayerManager._PlayerCameraMover.GetPlayerCamera().transform.eulerAngles.y;

            // Smoothly rotate the character
            transform.eulerAngles = Vector3.up *
                Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref m_TurnSmoothVelocity, TurnSmoothTime);
        }

        // Lock-on orientation
        else if (_PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON && 
            _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE)
        {
            Transform target = _PlayerManager._PlayerCameraMover.GetLastTargetedTransform();

            if (target == null)
            {
                _PlayerManager._FocusState = PlayerManager.FocusState.UNFOCUSSED;
            }
            else
            {
                Transform targetTransform = GetTargettingTransform(target);

                Vector3 lookAt = new Vector3(targetTransform.position.x, transform.position.y, targetTransform.position.z);  // Fix Y axis to avoid weird stuff
                Quaternion targetRotation = Quaternion.LookRotation(lookAt - transform.position);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 20f * Time.deltaTime);
            }
        }

        // Bow orientation
        else if (_PlayerManager._ArmState == PlayerManager.ArmState.BOW)
        {
            Quaternion cameraRotation = _PlayerManager._PlayerCameraMover.GetPlayerCamera().transform.rotation;
            transform.rotation = Quaternion.Euler(0f, cameraRotation.eulerAngles.y, 0f);
        }

        // Otherwise, if player is focused or falling, do nothing
    }

    private void MoveCharacter(Vector2 input)
    {
        // Grounded + Unfocuseed + Not dodging + Not attacking + No Bow Equipped
        if (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
            _PlayerManager._FocusState == PlayerManager.FocusState.UNFOCUSSED &&
            _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
            _PlayerManager._ArmState != PlayerManager.ArmState.BOW &&
            !IsAttacking())
        {
            m_DesiredVelocity = transform.forward * input.magnitude * ((2f / 3f) * MaxXZSpeed) *
                (_PlayerManager._ArmState == PlayerManager.ArmState.TRANSITION || 
                IsComboBuffering() ||
                _PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_IDLE 
                    ? FocusMovementScale : 1.0f);
        }

        // Grounded + Focussed/Locked-on/Bow Equipped + Not dodging + Not attacking
        else if (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                 (_PlayerManager._FocusState != PlayerManager.FocusState.UNFOCUSSED || _PlayerManager._ArmState == PlayerManager.ArmState.BOW) &&
                 _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                 !IsAttacking())
        {
            m_DesiredVelocity = transform.right * input.x * (((2f / 3f) * MaxXZSpeed) * FocusMovementScale) + transform.forward * input.y * (((2f / 3f) * MaxXZSpeed) * FocusMovementScale);
        }

        // Sprinting
        else if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
        {
            m_DesiredVelocity = transform.forward * input.magnitude * MaxXZSpeed;
            m_Velocity = m_DesiredVelocity;
        }

        // Forward dodge
        else if (_PlayerManager._DodgeState == PlayerManager.DodgeState.FORWARD)
        {
            m_DesiredVelocity = transform.forward * (MaxXZSpeed *
                (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED && !m_DodgeSlowed ? DodgeMovementScale : 0.1f));
            m_Velocity = m_DesiredVelocity;
        }

        // Right dodge
        else if (_PlayerManager._DodgeState == PlayerManager.DodgeState.RIGHT)
        {
            m_DesiredVelocity = transform.right * (MaxXZSpeed *
                (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED && !m_DodgeSlowed ? DodgeMovementScale : 0.1f));
            m_Velocity = m_DesiredVelocity;
        }

        // Backward dodge
        else if (_PlayerManager._DodgeState == PlayerManager.DodgeState.BACKWARD)
        {
            m_DesiredVelocity = -transform.forward * (MaxXZSpeed *
                (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED && !m_DodgeSlowed ? DodgeMovementScale : 0.1f));
            m_Velocity = m_DesiredVelocity;
        }

        // Left dodge
        else if (_PlayerManager._DodgeState == PlayerManager.DodgeState.LEFT)
        {
            m_DesiredVelocity = -transform.right * (MaxXZSpeed *
                (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED && !m_DodgeSlowed ? DodgeMovementScale : 0.1f));
            m_Velocity = m_DesiredVelocity;
        }

        // Falling
        else
        {
            m_DesiredVelocity = Vector3.zero;
        }
    }

    // ----------------------------------------------------------------------------------------------------------------
    // Out of player control ------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------------------

    public void MovePlayer()
    {
        // Halt x/z velocity and orient player
        OrientCharacter();
        MoveCharacter();
        ApplyGravity();

        // Finally, move the player based on modified velocity (from previous method calls)
        m_Velocity += (m_DesiredVelocity - m_Velocity) * AccelerationFactor * Time.deltaTime;
        m_CharacterController.Move(m_Velocity * Time.deltaTime);
    }

    // Util

    private void OrientCharacter()
    {
        if (_PlayerManager._InteractionState != PlayerManager.InteractionState.NORMAL &&
            _PlayerManager._InteractionState != PlayerManager.InteractionState.DEAD &&
            _PlayerManager._InteractionState != PlayerManager.InteractionState.PAUSE_MENU)
        {
            Transform target = _PlayerManager._PlayerInteractionSystem.GetLastReturnedInteractable().transform;
            Vector3 lookAt = new Vector3(target.position.x, transform.position.y, target.position.z);  // Fix Y axis to avoid weird stuff

            Vector3 direction = lookAt - transform.position;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), Time.time);
        }
    }

    private void MoveCharacter()
    {
        // Halt x/z velocity
        m_DesiredVelocity.x = 0f;
        m_DesiredVelocity.z = 0f;

        m_Velocity.x = 0f;
        m_Velocity.z = 0f;
    }

    // ----------------------------------------------------------------------------------------------------------------
    // Door opening track ---------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------------------

    private float m_DoorClearingSpeed;
    private float m_DoorStartTime;
    private bool m_FirstDoorMoveFrame;

    public void InitDoorTrack(Transform doorTransform, bool ownedSide)
    {
        // NOTE: The player should start and end ~three door lengths away

        // Momentarily disable the character controller
        EnableCharacterController(false);

        if (ownedSide)
        {
            transform.position = doorTransform.position
                + (-doorTransform.forward.normalized * (4.5f))
                + (-doorTransform.right.normalized * (doorTransform.localScale.x * 1.5f));
            transform.rotation = Quaternion.LookRotation(doorTransform.forward, transform.up);
        }
        else
        {
            transform.position = doorTransform.position 
                + (doorTransform.forward.normalized * (4.5f))
                + (-doorTransform.right.normalized * (doorTransform.localScale.x * 1.5f));
            transform.rotation = Quaternion.LookRotation(-doorTransform.forward, transform.up);
        }

        // Attempt to move the player to the floor
        RaycastHit groundHit;
        if (Physics.Raycast(transform.position, -transform.up, out groundHit, 5f, c_GroundMask))
            transform.position = new Vector3(transform.position.x, groundHit.point.y, transform.position.z);

        m_DoorClearingSpeed = (8f) / DungeonDoor.OPENING_TIME;
        m_DoorStartTime = Time.time;
        m_FirstDoorMoveFrame = true;

        // Reenable character controller
        EnableCharacterController(true);
    }

    public void AnimateDoorTrack()
    {
        if (m_FirstDoorMoveFrame)
        {
            m_FirstDoorMoveFrame = false;
            return;
        }

        // Set velocity to what is required to clar door
        m_Velocity = transform.forward.normalized * m_DoorClearingSpeed;

        // Apply gravity
        ApplyGravity();

        // Apply movement
        m_CharacterController.Move(m_Velocity * Time.deltaTime);

        // Finished door animation
        if (Time.time - m_DoorStartTime >= DungeonDoor.OPENING_TIME)
            _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
    }

    // Util

    private void ApplyGravity()
    {
        m_DesiredVelocity.y = -MaxYSpeed;
        m_Velocity.y = -MaxYSpeed;

        if (!GetIsGrounded())
            _PlayerManager._MovementState = PlayerManager.MovementState.FALLING;
        else if (_PlayerManager._MovementState != PlayerManager.MovementState.SPRINTING)
            _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

        
    }

    private bool CastToGround()
    {
        RaycastHit hit;

        if (Physics.Raycast(GroundCasterTransform.position, -Vector3.up, out hit, GroundCastDistance, m_LayerMask))
        {
            if (hit.transform.tag.Equals("Ground"))
                return true;
            else if (hit.transform.tag.Equals("Enemy"))
            {
                CombatEventSystem.SendForceSignal(hit.collider, 15f, transform);
                m_DesiredVelocity += transform.forward;
                m_Velocity += transform.forward;

                return false;
            }
        }

        return false;
    }
    private bool GetIsGrounded() { return m_CharacterController.isGrounded || CastToGround(); }
    private bool IsAttacking()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;
        return attackState == PlayerManager.AttackState.L_A_1 || 
               attackState == PlayerManager.AttackState.L_A_2 || 
               attackState == PlayerManager.AttackState.L_A_3 ||
               attackState == PlayerManager.AttackState.H_A_1 ||
               attackState == PlayerManager.AttackState.H_A_2 ||
               attackState == PlayerManager.AttackState.BOW_PULL;
    }
    private bool IsComboBuffering()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;
        return attackState == PlayerManager.AttackState.L_B_1 || 
               attackState == PlayerManager.AttackState.L_B_2 || 
               attackState == PlayerManager.AttackState.L_B_3 ||
               attackState == PlayerManager.AttackState.H_B_1 ||
               attackState == PlayerManager.AttackState.H_B_2 ||
               attackState == PlayerManager.AttackState.BOW_DRAW ||
               attackState == PlayerManager.AttackState.BOW_HOLD;
    }

    private Transform GetTargettingTransform(Transform enemyTransform)
    {
        return enemyTransform.GetComponent<EnemyAttributes>().GetTargetTransform();
    }

    // Public interface
    public float GetVelocityXZMagnitude() { return new Vector3(m_Velocity.x, 0.0f, m_Velocity.z).magnitude / MaxXZSpeed; }
    public float GetVelocityYMagnitude() { return GetIsGrounded() ? 0f : -m_Velocity.y / MaxYSpeed; }
    public Vector3 GetVelocityMovementVector() { return  (transform.forward * m_Velocity.x / (MaxXZSpeed * FocusMovementScale)) + 
                                                         (transform.right   * m_Velocity.z / (MaxXZSpeed * FocusMovementScale)); }
    public float GetActorRotation() { return transform.rotation.eulerAngles.y; }
    public void ApplyForceToPlayer(Transform source, float force)
    {
        // Adjust position of source transform
        Vector3 adjustedSource = new Vector3(source.position.x, transform.position.y, source.position.z);

        // Determine angle between the enemy and the hit source
        Vector3 forcePosition = Vector3.RotateTowards(transform.position, adjustedSource, float.MaxValue, 0.0f).normalized;

        m_DesiredVelocity += forcePosition * force;
        m_Velocity = m_DesiredVelocity;
    }

    public void EnableCharacterController(bool enable) { m_CharacterController.enabled = enable; }

    // Animator methods
    public void SlowDownDodge(bool slowDown) { m_DodgeSlowed = slowDown; }

}
