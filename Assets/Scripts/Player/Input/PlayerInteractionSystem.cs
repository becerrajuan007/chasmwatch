﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionSystem : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Interaction result callback states
    public enum InteractionResult { LOOT, CONVERSATION, DOOR_OPEN, SHRINE, PICKUP, FAILURE }

    // Members
    private HashSet<Collider> m_ColliderSet;
    private Collider m_LastReturnedCollider;

    // Constants
    private readonly string[] c_Tags = { "NPC", "Vendor", "Door", "Shrine" };

    private void Awake()
    {
        _PlayerManager = transform.parent.parent.parent.GetComponent<PlayerManager>();

        m_ColliderSet = new HashSet<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (CorrectTag(other.tag))
            m_ColliderSet.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (CorrectTag(other.tag))
            m_ColliderSet.Remove(other);
    }

    // Util

    private bool CorrectTag(string tag)
    {
        foreach (string t in c_Tags)
            if (tag.Equals(t)) return true;

        return false;
    }



    // Interface

    public Transform GetClosestInteractable()
    {
        // Return null if no interactables are in the collider set
        if (m_ColliderSet.Count == 0)
        {
            m_LastReturnedCollider = null;
            return null;
        }

        // Record the closest interactable
        Transform closestInteractableTransform = null;
        float closestInteractableDistance = float.MaxValue;

        // Iterate through each interactable
        foreach (Collider c in m_ColliderSet)
        {
            // Get the distance from the player interaction field transform to the interactable
            float distance = Vector3.Distance(transform.position, c.transform.position);
            if (distance < closestInteractableDistance)
            {
                closestInteractableDistance = distance;
                closestInteractableTransform = c.transform;
                m_LastReturnedCollider = c;
            }
        }

        return closestInteractableTransform;
    }

    public InteractionResult SendInteractionSignal(Transform playerTransform)
    {
        // No interactables in range
        if (m_LastReturnedCollider == null) return InteractionResult.FAILURE;

        // Strike a friendly conversation
        if (m_LastReturnedCollider.tag.Equals("NPC") ||
            m_LastReturnedCollider.tag.Equals("Vendor"))
        {
            // Was our attempt a success?
            if (InteractionEventSystem.SendConversationSignal(m_LastReturnedCollider, playerTransform)) return InteractionResult.CONVERSATION;
        }
        // Open a door, triggering a room transition
        else if (m_LastReturnedCollider.tag.Equals("Door"))
        {   
            // Able to open door?
            if (InteractionEventSystem.SendOpenDoorSignal(m_LastReturnedCollider, _PlayerManager._PlayerAttributes))
            {
                m_ColliderSet.Remove(m_LastReturnedCollider);
                return InteractionResult.DOOR_OPEN;
            }
        }
        // Activate a shrine
        else if (m_LastReturnedCollider.tag.Equals("Shrine"))
        {
            // Activate the shrine
            bool firstActivation = InteractionEventSystem.SendActivateShrineSignal(m_LastReturnedCollider);
            m_ColliderSet.Remove(m_LastReturnedCollider);

            // If it's first activation, add to list of visited shrines
            if (firstActivation)
                _PlayerManager._PlayerAttributes.AddToVisitedDungeonShrines(m_LastReturnedCollider.gameObject.GetComponent<DungeonShrine>());
            else
                _PlayerManager._PlayerAttributes.SetAsCurrentDungeonShrine(m_LastReturnedCollider.gameObject.GetComponent<DungeonShrine>());

            return InteractionResult.SHRINE;
        }

        // Could not interact
        return InteractionResult.FAILURE;
    }

    public void RequestNextDialogueMessage()
    {
        string message = InteractionEventSystem.RequestNextDialogueMessage(m_LastReturnedCollider);

        if (message != null)
        {
            _PlayerManager._PlayerUIHandler.SetDialogueText(message);
            _PlayerManager._InteractionState = PlayerManager.InteractionState.CONV_SCROLL;
        }
        else
        {
            _PlayerManager._PlayerUIHandler.ShowDialogueBox(false);
            _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
        }
    }

    public void ReceiveDialogue(string npcName, string message)
    {
        _PlayerManager._PlayerUIHandler.ShowDialogueBox(true);
        _PlayerManager._PlayerUIHandler.SetDialogueText(npcName, message);
    }

    public Collider GetLastReturnedInteractable() { return m_LastReturnedCollider; }

    public void ReceivePickupSignal(PickupAbs.PickupType type)
    {
        switch (type)
        {
            case PickupAbs.PickupType.ARROWS:
                // Add three arrows
                _PlayerManager._PlayerAttributes.ChangeArrows(3);
                break;
            case PickupAbs.PickupType.HEART:
                // Add 20 health
                _PlayerManager._PlayerAttributes.ChangeHealth(20);
                break;
            case PickupAbs.PickupType.KEY:
                // Add 1 key
                _PlayerManager._PlayerAttributes.ChangeKeys(1);
                break;
            default:
                Debug.LogError("Invalid pickup type!");
                break;
        }
    }
}
