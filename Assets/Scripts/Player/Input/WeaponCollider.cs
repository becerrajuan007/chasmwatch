﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCollider : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Components
    private CapsuleCollider m_WeaponCollider;
    private MeleeWeaponTrail m_WeaponTrail;

    // Members
    private readonly System.Random c_Random = new System.Random();
    private int m_AttackID;

    private void Awake()
    {
        _PlayerManager = FindParentTransform(transform, "PlayerCharacter").GetComponent<PlayerManager>();

        m_WeaponCollider = GetComponent<CapsuleCollider>();
        m_WeaponTrail = GetComponentInChildren<MeleeWeaponTrail>();

        // By default, weapon collider is disabled
        m_WeaponCollider.enabled = false;
        m_WeaponTrail.Emit = false;

        m_AttackID = -1;
    }

    // Events

    private void OnTriggerEnter(Collider other)
    {
        // Send attack signal to enemy using the CombatEventSystem
        if (other.tag.Equals("Enemy") || other.tag.Equals("DungeonSwitch"))
        {
            if(CombatEventSystem.SendAttackSignal(other, m_AttackID,
                _PlayerManager._PlayerAttributes.GetAttackDamage(_PlayerManager._AttackState),
                _PlayerManager._PlayerAttributes.GetPoiseDamage(_PlayerManager._AttackState),
                _PlayerManager._PlayerMover.transform))
            {
                ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.HIT_SPARK, 
                                              transform.position + -transform.up * 0.5f,
                                              _PlayerManager._PlayerMover.transform.rotation,
                                              new Vector3(1f, 1f, 1f));
                _PlayerManager._PlayerAudioHandler.PlaySwordHit();
            }
        }
    }

    // Util

    private Transform FindParentTransform(Transform child, string name)
    {
        Transform parent = child.parent;

        if (parent == null)
        {
            Debug.LogError("Could not find parent " + name);
            return null;
        }

        if (parent.name.Equals(name))
            return parent;
        else
            return FindParentTransform(parent, name);
    }

    // Interface

    public void EnableWeaponHitbox(bool isEnabled)
    {
        m_WeaponCollider.enabled = isEnabled;
        m_WeaponTrail.Emit = isEnabled;
    }

    public void GenerateAttackID() { m_AttackID = c_Random.Next(); }

    public int GetAttackID()
    {
        GenerateAttackID();
        return m_AttackID;
    }
 
}
