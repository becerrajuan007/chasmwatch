﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttacker : MonoBehaviour
{
    // Super-script
    private PlayerManager _PlayerManager;

    // Members
    private bool m_BufferedDuringAttack;

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        m_BufferedDuringAttack = false;
    }

    public void EnqueueLightAttack()
    {
        // Cool rolling attack
        if (_PlayerManager._DodgeState != PlayerManager.DodgeState.NONE)
        {
            // Go straight to jab attack (L_A_3)
            _PlayerManager._AttackState = PlayerManager.AttackState.L_A_3;
            _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
        }
        // Cool sprinting attack
        else if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
        {
            // Go straight into sweep attack (L_A_2)
            _PlayerManager._AttackState = PlayerManager.AttackState.L_A_2;
            _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
        }
        else
        {
            // Enqueue a normal attack
            switch (_PlayerManager._AttackState)
            {
                case PlayerManager.AttackState.IDLE:
                    // Go from idle to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_1;
                    _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
                    break;
                case PlayerManager.AttackState.BLOCK_IDLE:
                    // Go from block to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_1;
                    m_BufferedDuringAttack = true;
                    _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
                    break;
                case PlayerManager.AttackState.BLOCK_HIT:
                    // Go from block to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_1;
                    m_BufferedDuringAttack = true;
                    _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
                    break;
                case PlayerManager.AttackState.L_A_1:
                    // Go from attack 1 straight to attack 2 (after 75% of attack animation)
                    m_BufferedDuringAttack = true;
                    break;
                case PlayerManager.AttackState.L_B_1:
                    // Go from buffer 1 to attack 2
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_2;
                    _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
                    break;
                case PlayerManager.AttackState.L_A_2:
                    // Go from attack 2 straight to attack 3 (after 75% of attack animation)
                    m_BufferedDuringAttack = true;
                    break;
                case PlayerManager.AttackState.L_B_2:
                    // Go from buffer 2 to attack 3
                    _PlayerManager._AttackState = PlayerManager.AttackState.L_A_3;
                    _PlayerManager._PlayerAnimationHandler.TriggerLightAttack();
                    break;
                default:
                    break;
            }
        }
    }

    public void EnqueueHeavyAttack()
    {
        // Cool rolling attack
        if (_PlayerManager._DodgeState != PlayerManager.DodgeState.NONE)
        {
            // Go straight to downward slash (H_A_2)
            _PlayerManager._AttackState = PlayerManager.AttackState.H_A_2;
            _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
        }
        // Cool sprinting attack
        else if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
        {
            // Go straight to downward slash (H_A_2)
            _PlayerManager._AttackState = PlayerManager.AttackState.H_A_2;
            _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
        }
        else
        {
            switch (_PlayerManager._AttackState)
            {
                case PlayerManager.AttackState.IDLE:
                    // Go from idle to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_A_1;
                    _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
                    break;
                case PlayerManager.AttackState.BLOCK_IDLE:
                    // Go from block to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_A_1;
                    m_BufferedDuringAttack = true;
                    _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
                    break;
                case PlayerManager.AttackState.BLOCK_HIT:
                    // Go from block to attack 1
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_A_1;
                    m_BufferedDuringAttack = true;
                    _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
                    break;
                case PlayerManager.AttackState.H_A_1:
                    // Go from attack 1 straight to attack 2 (after 75% of attack animation)
                    m_BufferedDuringAttack = true;
                    break;
                case PlayerManager.AttackState.H_B_1:
                    // Go from buffer 1 to attack 2
                    _PlayerManager._AttackState = PlayerManager.AttackState.H_A_2;
                    _PlayerManager._PlayerAnimationHandler.TriggerHeavyAttack();
                    break;
            }
        }
    }

    public void DrawBow()
    {
        // Only draw bow if we have at least one arrow
        if (_PlayerManager._PlayerAttributes.GetCurrentArrows() >= 1)
        {
            // Transition from idle to draw, animBehavior will transition to hold
            _PlayerManager._AttackState = PlayerManager.AttackState.BOW_DRAW;
            _PlayerManager._PlayerEquipmentHandler.ShowArrowLine(false);
        }
        else
        {
            // Display warning message
            _PlayerManager._PlayerUIHandler.PushMessage("You are out of arrows!", true);
        }
    }

    public void PullBow()
    {
        // Transition from hold to pull, loose an arrow
        _PlayerManager._AttackState = PlayerManager.AttackState.BOW_PULL;
        _PlayerManager._PlayerEquipmentHandler.ShowArrowLine(false);

        // Retreive hit position
        var hitPosition = _PlayerManager._PlayerCameraMover.GetArrowHitPosition();

        // Spawn arrow at hit location
        Transform arrow = _PlayerManager._PlayerPools.SpawnArrow(hitPosition.Item1, hitPosition.Item2);

        // Spawn hit effect
        ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.HIT_SPARK, hitPosition.Item1, hitPosition.Item2, new Vector3(1f, 1f, 1f));

        // Play arrow hit sound
        AudioSystem.SpawnSound(AudioSystem.SoundType.ARROW, hitPosition.Item1, 1.5f);

        // Send damage signal to enemy
        Collider hitTarget = _PlayerManager._PlayerCameraMover.GetLastReturnedHitTarget();

        if (hitTarget)
        {
            CombatEventSystem.SendAttackSignal(hitTarget, _PlayerManager._PlayerCollisionHandler._WeaponCollider.GetAttackID(),
                _PlayerManager._PlayerAttributes.GetAttackDamage(_PlayerManager._AttackState),
                _PlayerManager._PlayerAttributes.GetPoiseDamage(_PlayerManager._AttackState),
                _PlayerManager._PlayerEquipmentHandler.transform);

            if (hitTarget.tag.Equals("Enemy"))
            {
                // Immediately hide arrow if we hit an enemy
                arrow.gameObject.SetActive(false);
            }
        }

        // Spawn arrow line hit
        ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.ARROW_FIRE,
                                      _PlayerManager._PlayerEquipmentHandler.GetArrowStartPosition(), 
                                      hitPosition.Item2, 
                                      new Vector3(1f, 1f, 1f));

        // Decrement an arrow
        _PlayerManager._PlayerAttributes.ChangeArrows(-1);

        // If we ran out of arrows, hide the arrow
        if (_PlayerManager._PlayerAttributes.GetCurrentArrows() < 1)
            _PlayerManager._PlayerEquipmentHandler.ShowArrow(0);
    }

    public void CancelBow()
    {
        _PlayerManager._AttackState = PlayerManager.AttackState.IDLE;
        _PlayerManager._PlayerEquipmentHandler.ShowArrowLine(false);
    }

    // Interface
    public bool BufferedDuringAttack() { return m_BufferedDuringAttack; }
    public void ResetAttackBuffer() { m_BufferedDuringAttack = false; }
}
