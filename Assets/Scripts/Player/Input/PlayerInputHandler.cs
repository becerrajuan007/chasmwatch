﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

public class PlayerInputHandler : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float DodgeBufferTime = 0.4f;

    // Super-script
    private PlayerManager _PlayerManager;

    // Left stick
    private const string LeftStickHorz = "LeftStickHorz";
    private const string LeftStickVert = "LeftStickVert";
    private const string SprintButton = "SprintButton";

    // Right stick
    private const string RightStickHorz = "RightStickHorz";
    private const string RightStickVert = "RightStickVert";

    // Control pad
    private const string ControlPadHorz = "ControlPadHorz";
    private const string ControlPadVert = "ControlPadVert";

    // LB/LT
    private const string EquipButton = "EquipButton";
    private const string FocusButton = "FocusButton";

    // RB/RT
    private const string BlockButton = "BlockButton";
    private const string BowButton = "BowButton";

    // Face buttons
    private const string InteractButton = "InteractButton";
    private const string DodgeButton = "DodgeButton";
    private const string LightAttackButton = "LightAttackButton";
    private const string HeavyAttackButton = "HeavyAttackButton";

    // Start button
    private const string StartButton = "StartButton";

    // Universal
    private const string Tilde = "Tilde";

    // Consts
    private const float WASD_MOVE_MAX = 0.75f;

    // Members
    private float m_LastDodgeEndTime;
    private bool m_ShowCursor = false;

    private void Awake()
    {
        _PlayerManager = transform.parent.gameObject.GetComponent<PlayerManager>();

        m_LastDodgeEndTime = 0f;
    }

    private void Start()
    {
        ShowMouseCursor(m_ShowCursor);
    }

    private void ShowMouseCursor(bool show)
    {
        m_ShowCursor = show;

        Cursor.visible = show;
        Cursor.lockState = show ? CursorLockMode.None : CursorLockMode.Locked;
    }

    public void HandleInputs()
    {
        HandleUniversalInputs();

        switch (_PlayerManager._InteractionState)
        {
            case PlayerManager.InteractionState.NORMAL:
                HandleGameplayInputs();
                break;
            case PlayerManager.InteractionState.CONV_SCROLL:
                HandleDialogueInputs();
                break;
            case PlayerManager.InteractionState.CONV_NEXT:
                HandleDialogueInputs();
                break;
            case PlayerManager.InteractionState.DOOR_MOVE:
                HandleDoorOpeningInputs();
                break;
            case PlayerManager.InteractionState.SHRINE_MENU:
                HandleShrineInputs();
                break;
            case PlayerManager.InteractionState.DEAD:
                HandleDeathInputs();
                break;
            case PlayerManager.InteractionState.PAUSE_MENU:
                HandlePauseMenuInputs();
                break;
            default:
                Debug.LogError("Invalid interaction state!");
                break;
        }
    }

    // -----------------------------------------------------------------------------------------------
    // Universal inputs ------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandleUniversalInputs()
    {
        // Listen to tilde
        ToggleCursor(Input.GetAxisRaw(Tilde) > 0);
    }

    private bool m_ToggleCursorPressed;
    private void ToggleCursor(bool isHeld)
    {
        if (isHeld && !m_ToggleCursorPressed)
        {
            m_ToggleCursorPressed = true;

            ShowMouseCursor(!m_ShowCursor);
        }
        else if (!isHeld)
        {
            m_ToggleCursorPressed = false;
        }
    }

    // -----------------------------------------------------------------------------------------------
    // Gameplay inputs -------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandleGameplayInputs()
    {
        // Clean left stick input
        Vector2 leftStickInput = CleanWASDInput(new Vector2(Input.GetAxis(LeftStickHorz), -Input.GetAxis(LeftStickVert)));
        _PlayerManager._PlayerMover.MovePlayer(leftStickInput);

        // Right stick movement
        Vector2 rightStickInput = new Vector2(Input.GetAxis(RightStickHorz), -Input.GetAxis(RightStickVert));
        _PlayerManager._PlayerCameraMover.RotateCamera(rightStickInput);

        // In addition, check if we can swap targets if we are locked on
        if (_PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON)
            SwapTargets(rightStickInput);

        // Interact button
        Interact(Input.GetAxisRaw(InteractButton) > 0);

        // Focus button
        Focus(Input.GetAxisRaw(FocusButton) > 0.3f);

        // Equip button
        Equip(Input.GetAxisRaw(EquipButton) > 0);

        // Dodge button
        Dodge(Input.GetAxisRaw(DodgeButton) > 0);

        // Block button
        Block(Input.GetAxisRaw(BlockButton) > 0);

        // Light attack
        LightAttack(Input.GetAxisRaw(LightAttackButton) > 0);

        // Heavy attack
        HeavyAttack(Input.GetAxisRaw(HeavyAttackButton) > 0);

        // Bow button
        DrawBow(Input.GetAxisRaw(BowButton) > 0.3f);

        // Sprint button
        Sprint(Input.GetAxis(SprintButton) > 0);

        // Pause button
        Pause(Input.GetAxis(StartButton) > 0);
    }

    private Vector2 CleanWASDInput(Vector2 input)
    {
        // This is done as both input.x and input.y can end up being maxed out in WASD

        if (Mathf.Abs(input.x) >= WASD_MOVE_MAX && Mathf.Abs(input.y) >= WASD_MOVE_MAX)
        {
            input.x = Mathf.Clamp(input.x, -WASD_MOVE_MAX, WASD_MOVE_MAX);
            input.y = Mathf.Clamp(input.y, -WASD_MOVE_MAX, WASD_MOVE_MAX);
        }

        return input;
    }

    private bool m_FocusPressed;
    private void Focus(bool isHeld)
    {
        if (isHeld && !m_FocusPressed)
        {
            m_FocusPressed = true;

            if (_PlayerManager._MovementState != PlayerManager.MovementState.FALLING)
            {
                // First, ensure that we stop sprinting
                _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

                if (_PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON)
                {
                    // Toggle to unfocus if we are locked on
                    _PlayerManager._FocusState = PlayerManager.FocusState.UNFOCUSSED;
                }   
                else
                {
                    // Try to target the closest player (This may put us in LOCKEDON state)
                    _PlayerManager._FocusState = PlayerManager.FocusState.FOCUSSED;
                    _PlayerManager._PlayerCameraMover.TargetClosestEnemy();
                }
            }
        }
        else if (!isHeld)
        {
            // Unfocus if we are focussed, but not if we're locked on
            if (_PlayerManager._FocusState == PlayerManager.FocusState.FOCUSSED)
                _PlayerManager._FocusState = PlayerManager.FocusState.UNFOCUSSED;

            m_FocusPressed = false;
        }
    }

    private float m_PreviousFlickTime = 0f;
    private void SwapTargets(Vector2 input)
    {
        // Check if the current x input is +-1.0 and previous input was low
        if (Mathf.Abs(input.x) >= 0.9f && Time.time - m_PreviousFlickTime > 0.4f)
        {
            // Flick detected, attempt to switch targets
            _PlayerManager._PlayerCameraMover.TargetAdjacentEnemy(input.x < 0f);

            m_PreviousFlickTime = Time.time;
        }
    }

    private bool m_InteractPressed;
    private void Interact(bool isHeld)
    {
        if (isHeld && !m_InteractPressed)
        {
            m_InteractPressed = true;

            // Only enter an interaction if the player is grounded, not dodging, not arm transitioning, and not attacking
            if (_PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._AttackState == PlayerManager.AttackState.IDLE)
            {
                PlayerInteractionSystem.InteractionResult interactionResult = _PlayerManager._PlayerInteractionSystem.SendInteractionSignal(transform);

                // Enter a conversation
                if (interactionResult == PlayerInteractionSystem.InteractionResult.CONVERSATION)
                {
                    _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

                    // Enter conversation state 
                    _PlayerManager._InteractionState = PlayerManager.InteractionState.CONV_SCROLL;

                    // If we were focussed/targetted, exit that state
                    _PlayerManager._FocusState = PlayerManager.FocusState.UNFOCUSSED;

                    // If we were armed, unarm
                    if (_PlayerManager._ArmState != PlayerManager.ArmState.UNARMED)
                    {
                        _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.UNARMED);
                        _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
                    }

                    // Reset currently held inputs
                    FlushInputs();
                }
                // Open a door
                else if (interactionResult == PlayerInteractionSystem.InteractionResult.DOOR_OPEN)
                {
                    _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

                    // Enter the door opening state
                    _PlayerManager._InteractionState = PlayerManager.InteractionState.DOOR_MOVE;

                    // Cache important door opening data
                    _PlayerManager._PlayerMover.InitDoorTrack(InteractionEventSystem.GetLastDoorTransform(), InteractionEventSystem.GetLastDoorFromOwnedArea());

                    // If we were armed, unarm
                    if (_PlayerManager._ArmState != PlayerManager.ArmState.UNARMED)
                    {
                        _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.UNARMED);
                        _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
                    }

                    // Set new zone name on UI
                    _PlayerManager._PlayerUIHandler.SetMinimapAreaName(DungeonZoneSystem.GetCurrentZone().Name);

                    // Reset currently held inputs
                    FlushInputs();
                }
                // Activate a shrine
                else if (interactionResult == PlayerInteractionSystem.InteractionResult.SHRINE)
                {
                    _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

                    // If we were armed, unarm
                    if (_PlayerManager._ArmState != PlayerManager.ArmState.UNARMED)
                    {
                        _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.UNARMED);
                        _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
                    }

                    // Deactivate enemies in area
                    _PlayerManager._PlayerAttributes.GetLastVisitedShrine().Zone.SpawnEmemies(false);

                    // Activate shrine menu
                    _PlayerManager._InteractionState = PlayerManager.InteractionState.SHRINE_MENU;
                    m_ShrineInteractPressed = true;

                    // Reset currently held inputs
                    FlushInputs();
                }
            }

        }
        else if (!isHeld)
        {
            m_InteractPressed = false;
        }
    }

    private bool m_DodgePressed;
    private void Dodge(bool isHeld)
    {
        if (isHeld && !m_DodgePressed)
        {
            m_DodgePressed = true;
            
            // Only dodge if the player is not falling, not currently dodging, not in a dodge buffer state, and not attacking
            if (_PlayerManager._MovementState != PlayerManager.MovementState.FALLING && 
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                Time.time - m_LastDodgeEndTime >= DodgeBufferTime &&
                _PlayerManager._PlayerAttributes.PeekCanDodge())
            {
                if (!IsAttacking())
                {
                    // First, do we have enough stamina?
                    if (!_PlayerManager._PlayerAttributes.DodgeChangeStamina()) return;

                    // If the player was sprinting, transition to a grounded state
                    _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;

                    Vector3 movementVector = _PlayerManager._PlayerMover.GetVelocityMovementVector();

                    // Determine the prominent movement direction
                    if (Mathf.Abs(movementVector.x) <= 0.01f && Mathf.Abs(movementVector.z) <= 0.01f)
                        // If standing still, do a backflip
                        _PlayerManager._DodgeState = PlayerManager.DodgeState.BACKWARD;
                    else if (Mathf.Abs(movementVector.z) >= Mathf.Abs(movementVector.x))
                    {
                        if (movementVector.z >= 0f)
                            // Dodge right
                            _PlayerManager._DodgeState = PlayerManager.DodgeState.RIGHT;
                        else
                            // Dodge left
                            _PlayerManager._DodgeState = PlayerManager.DodgeState.LEFT;
                    }
                    else
                    {
                        if (movementVector.x >= 0f)
                            // Dodge forward
                            _PlayerManager._DodgeState = PlayerManager.DodgeState.FORWARD;
                        else
                            // Dodge backward
                            _PlayerManager._DodgeState = PlayerManager.DodgeState.BACKWARD;
                    }

                    // If the player had a bow drawn, undraw it
                    if (IsUsingBow())
                        _PlayerManager._PlayerAttacker.CancelBow();

                }
            }
        }
        else if (!isHeld)
        {
            m_DodgePressed = false;
        }
    }

    private bool m_EquipPressed;
    private void Equip(bool isHeld)
    {
        if (isHeld && !m_EquipPressed)
        {
            m_EquipPressed = true;
            
            if (_PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION)
            {
                if (_PlayerManager._ArmState == PlayerManager.ArmState.UNARMED)
                    _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.SWORD_SHIELD);
                else
                    _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.UNARMED);

                _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
            }

        }
        else if (!isHeld)
        {
            m_EquipPressed = false;
        }
    }

    private bool m_LightAttackPressed;
    private void LightAttack(bool isHeld)
    {
        if (isHeld && !m_LightAttackPressed)
        {
            m_LightAttackPressed = true;

            // First equip?
            if (_PlayerManager._ArmState != PlayerManager.ArmState.SWORD_SHIELD &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE)
            {
                _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.SWORD_SHIELD);
                _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
            } 
            else if (_PlayerManager._ArmState == PlayerManager.ArmState.SWORD_SHIELD &&
                     _PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                     IsInAttackableState() &&
                     _PlayerManager._PlayerAttributes.PeekCanLightAttack())
            {
                _PlayerManager._PlayerAttacker.EnqueueLightAttack();
            }

        }
        else if (!isHeld)
        {
            m_LightAttackPressed = false;
        }
    }

    private bool m_HeavyAttackPressed;
    private void HeavyAttack(bool isHeld)
    {
        if (isHeld && !m_HeavyAttackPressed)
        {
            m_HeavyAttackPressed = true;

            // First equip?
            if (_PlayerManager._ArmState != PlayerManager.ArmState.SWORD_SHIELD &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE)
            {
                _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.SWORD_SHIELD);
                _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
            }
            else if (_PlayerManager._ArmState == PlayerManager.ArmState.SWORD_SHIELD &&
                     _PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                     IsInAttackableState() &&
                     _PlayerManager._PlayerAttributes.PeekCanHeavyAttack())
            {
                _PlayerManager._PlayerAttacker.EnqueueHeavyAttack();
            }
        }
        else if (!isHeld)
        {
            m_HeavyAttackPressed = false;
        }
    }

    private bool m_SprintPressed;
    private void Sprint(bool isHeld)
    {
        if (isHeld && !m_SprintPressed)
        {
            m_SprintPressed = true;

            // You can only sprint if the player is not falling, unfocussed, not dodging, not arm transitioning, not attacking, and have <20% of stamina
            if (_PlayerManager._MovementState != PlayerManager.MovementState.FALLING &&
                _PlayerManager._FocusState == PlayerManager.FocusState.UNFOCUSSED &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._AttackState == PlayerManager.AttackState.IDLE &&
                _PlayerManager._PlayerAttributes.PeekCanSprint())
            {
                // Toggle from grounded -> sprinting and vice-versa
                if (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED)
                    _PlayerManager._MovementState = PlayerManager.MovementState.SPRINTING;
                else if (_PlayerManager._MovementState == PlayerManager.MovementState.SPRINTING)
                    _PlayerManager._MovementState = PlayerManager.MovementState.GROUNDED;
            }
        }
        else if (!isHeld)
        {
            m_SprintPressed = false;
        }

    }

    private void Block(bool isHeld)
    {
        if (isHeld)
        {
            // First equip?
            if (_PlayerManager._ArmState != PlayerManager.ArmState.SWORD_SHIELD &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE)
            {
                _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.SWORD_SHIELD);
                _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
            }
            // Only block if the player is grounded, not dodging, has sword and shield out, and is not attacking
            else if (_PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                _PlayerManager._ArmState == PlayerManager.ArmState.SWORD_SHIELD &&
                CanBlock())
            {
                // Do block
                _PlayerManager._AttackState = PlayerManager.AttackState.BLOCK_IDLE;
            }
        }
        else
        {
            // Drop shield
            if (_PlayerManager._AttackState == PlayerManager.AttackState.BLOCK_IDLE)
                _PlayerManager._AttackState = PlayerManager.AttackState.IDLE;
        }
    }

    private void DrawBow(bool isHeld)
    {
        if (isHeld)
        {
            // First equip?
            if (_PlayerManager._ArmState != PlayerManager.ArmState.BOW &&
                _PlayerManager._ArmState != PlayerManager.ArmState.TRANSITION &&
                _PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE)
            {
                _PlayerManager._PlayerEquipmentHandler.SetTransitionStates(_PlayerManager._ArmState, PlayerManager.ArmState.BOW);
                _PlayerManager._PlayerAnimationHandler.TriggerArmTransition();
            }
            // Draw the bow and maintain stance
            else if (_PlayerManager._ArmState == PlayerManager.ArmState.BOW &&
                     _PlayerManager._MovementState == PlayerManager.MovementState.GROUNDED &&
                     _PlayerManager._DodgeState == PlayerManager.DodgeState.NONE &&
                     IsInBowDrawableState())
            {
                _PlayerManager._PlayerAttacker.DrawBow();
            }
        }
        // Release bow, shoot an arrow
        else if (!isHeld && _PlayerManager._AttackState == PlayerManager.AttackState.BOW_HOLD)
        {
            _PlayerManager._PlayerAttacker.PullBow();
        }
    }

    private bool m_PauseButtonPressed;
    private void Pause(bool isHeld)
    {
        if (isHeld && !m_PauseButtonPressed)
        {
            m_PauseButtonPressed = true;

            // Switch to pause state
            _PlayerManager._InteractionState = PlayerManager.InteractionState.PAUSE_MENU;

            FlushInputs();
        }
        else if (!isHeld)
        {
            m_PauseButtonPressed = false;
        }
    }

    // -----------------------------------------------------------------------------------------------
    // Dialogue inputs -------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandleDialogueInputs()
    {
        // Call overload functions of move/rotate with no input
        _PlayerManager._PlayerMover.MovePlayer();
        _PlayerManager._PlayerCameraMover.RotateCamera();

        // Listen for dialogue inputs
        Continue(Input.GetAxisRaw(InteractButton) > 0f);
        Rush(Input.GetAxisRaw(DodgeButton) > 0f);
    }

    private bool m_ContinuePressed;
    private void Continue(bool isHeld)
    {
        if (isHeld && !m_ContinuePressed)
        {
            m_ContinuePressed = true;

            // Request next message. This will either take us to the next message, or end the conversation
            if (_PlayerManager._InteractionState == PlayerManager.InteractionState.CONV_NEXT)
            {
                _PlayerManager._PlayerInteractionSystem.RequestNextDialogueMessage();

                // Check if we exited the conversation state
                if (_PlayerManager._InteractionState == PlayerManager.InteractionState.NORMAL)
                {
                    FlushInputs();
                    m_InteractPressed = true;
                }
            }
                
        }
        else if (!isHeld)
        {
            m_ContinuePressed = false;
        }
    }

    private bool m_RushPressed;
    private void Rush(bool isHeld)
    {
        if (isHeld && !m_RushPressed)
        {
            m_RushPressed = true;

            // Request next message. This will either take us to the next message, or end the conversation
            if (_PlayerManager._InteractionState == PlayerManager.InteractionState.CONV_NEXT)
                _PlayerManager._PlayerInteractionSystem.RequestNextDialogueMessage();

            // Check if we exited the conversation state
            if (_PlayerManager._InteractionState == PlayerManager.InteractionState.NORMAL)
            {
                FlushInputs();
                m_DodgePressed = true;
            }
        }
        else if (isHeld)
        {
            _PlayerManager._PlayerUIHandler.RushDialogue(true);
        }
        else if (!isHeld)
        {
            m_RushPressed = false;

            _PlayerManager._PlayerUIHandler.RushDialogue(false);
        }
    }

    // -----------------------------------------------------------------------------------------------
    // Door opening inputs ---------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandleDoorOpeningInputs()
    {
        // Simply move and orient camera
        _PlayerManager._PlayerCameraMover.RotateCamera();
        _PlayerManager._PlayerMover.AnimateDoorTrack();
    }

    // -----------------------------------------------------------------------------------------------
    // Shrine inputs ---------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private bool m_ShrineInteractPressed = false;

    private void HandleShrineInputs()
    {
        // Call overload functions of move/rotate with no input
        _PlayerManager._PlayerMover.MovePlayer();
        _PlayerManager._PlayerCameraMover.RotateCamera();

        if (m_ShrineInteractPressed && Input.GetAxisRaw(InteractButton) < 0.1f)
            m_ShrineInteractPressed = false;

        if (Input.GetAxisRaw(InteractButton) > 0 && !m_ShrineInteractPressed)
            ShrineWarp();

        if (Input.GetAxisRaw(DodgeButton) > 0)
            ShrineCancel();

        // Listen for stick/pad inputs
        Vector2 stickInput = new Vector2(Input.GetAxis(LeftStickHorz), -Input.GetAxis(LeftStickVert));
        Vector2 controlInput = new Vector2(Input.GetAxis(ControlPadHorz), Input.GetAxis(ControlPadVert));

        if (stickInput != Vector2.zero)
            ToggleShrine(stickInput);
        else
            ToggleShrine(controlInput);
    }

    private float m_LastToggleShrineTime = 0f;
    private bool m_ToggleShrineHeld = false;
    private void ToggleShrine(Vector2 input)
    {
        if (Mathf.Abs(input.x) > 0.5f)
        {
            if (!m_ToggleShrineHeld || Time.time - m_LastToggleShrineTime > 0.4f)
            {
                _PlayerManager._PlayerUIHandler.ChangeShrine(input.x < -0.5f);
                m_LastToggleShrineTime = Time.time;
            }
        }

        m_ToggleShrineHeld = input != Vector2.zero;
    }

    private void ShrineWarp()
    {
        FlushInputs();
        m_InteractPressed = true;

        _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;

        DungeonShrine selectedShrine = _PlayerManager._PlayerUIHandler.GetSelectedShrine();
        _PlayerManager._PlayerAttributes.WarpToDungeonShrine(selectedShrine);
    }

    private void ShrineCancel()
    {
        _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
        FlushInputs();
        m_DodgePressed = true;

        // Activate enemies in area
        _PlayerManager._PlayerAttributes.GetLastVisitedShrine().Zone.SpawnEmemies(true);
    }

    // -----------------------------------------------------------------------------------------------
    // Death Inputs ----------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandleDeathInputs()
    {
        // Call overload functions of move/rotate with no input
        _PlayerManager._PlayerMover.MovePlayer();
        _PlayerManager._PlayerCameraMover.RotateCamera();
    }

    // -----------------------------------------------------------------------------------------------
    // Pause Menu Inputs -----------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------

    private void HandlePauseMenuInputs()
    {
        // Overload move function
        _PlayerManager._PlayerMover.MovePlayer();

        // Listen for stick/pad inputs
        Vector2 stickInput = new Vector2(Input.GetAxis(LeftStickHorz), -Input.GetAxis(LeftStickVert));
        Vector2 controlInput = new Vector2(Input.GetAxis(ControlPadHorz), Input.GetAxis(ControlPadVert));

        if (stickInput != Vector2.zero)
            ToggleMenuSelection(stickInput);
        else
            ToggleMenuSelection(controlInput);

        // Listen for interact button
        SelectMenuItem(Input.GetAxis(InteractButton) > 0);

        // Listen for cancel button
        CancelPause(Input.GetAxis(DodgeButton) > 0);
    }

    private float m_LastPauseMenuToggleTime = -1;
    private bool m_ToggledPauseMenuSelection = false;

    private void ToggleMenuSelection(Vector2 input)
    {
        if (Mathf.Abs(input.x) >= Mathf.Abs(input.y) && Mathf.Abs(input.x) > 0.5f)
        {
            if (!m_ToggledPauseMenuSelection || Time.time - m_LastPauseMenuToggleTime > 0.4f)
            {
                _PlayerManager._PlayerUIHandler.TogglePauseMenuItemSide(input.x < 0);
                m_LastPauseMenuToggleTime = Time.time;
            }
        }
        else if (Mathf.Abs(input.x) <= Mathf.Abs(input.y) && Mathf.Abs(input.y) > 0.5f)
        {
            if (!m_ToggledPauseMenuSelection || Time.time - m_LastPauseMenuToggleTime > 0.4f)
            {
                _PlayerManager._PlayerUIHandler.TogglePauseMenuItem(input.y < 0);
                m_LastPauseMenuToggleTime = Time.time;
            }
        }

        m_ToggledPauseMenuSelection = input != Vector2.zero;
    }

    private bool m_SelectMenuItemPressed = false;
    private void SelectMenuItem(bool isHeld)
    {
        if (isHeld && !m_SelectMenuItemPressed)
        {
            m_SelectMenuItemPressed = true;

            // Select the currently highlighted menu item
            _PlayerManager._PlayerUIHandler.SelectMenuItem();
        }
        else if (!isHeld)
        {
            m_SelectMenuItemPressed = false;
        }    
    }

    private bool m_CancelMenuPressed = false;
    private void CancelPause(bool isHeld)
    {
        if (isHeld && !m_CancelMenuPressed)
        {
            m_CancelMenuPressed = true;

            // Go back a menu, or exit the menu entirely
            // If true, the menu has been exited

            if (_PlayerManager._PlayerUIHandler.BacktrackMenuItem())
            {
                FlushInputs();
                m_DodgePressed = true;
                _PlayerManager._InteractionState = PlayerManager.InteractionState.NORMAL;
            }
        }
        else if (!isHeld)
        {
            m_CancelMenuPressed = false;
        }
    }

    // Util

    public void FlushInputs()
    {
        m_FocusPressed = false;
        m_InteractPressed = false;
        m_DodgePressed = false;
        m_EquipPressed = false;
        m_LightAttackPressed = false;
        m_HeavyAttackPressed = false;
        m_SprintPressed = false;
        m_PauseButtonPressed = false;
        m_SelectMenuItemPressed = false;
        m_CancelMenuPressed = false;
    }

    private bool IsAttacking()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState == PlayerManager.AttackState.L_A_1 ||
               attackState == PlayerManager.AttackState.L_A_2 ||
               attackState == PlayerManager.AttackState.L_A_3 ||
               attackState == PlayerManager.AttackState.H_A_1 ||
               attackState == PlayerManager.AttackState.H_A_2 ||
               attackState == PlayerManager.AttackState.BOW_PULL;
    }

    private bool IsUsingBow()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState == PlayerManager.AttackState.BOW_DRAW ||
               attackState == PlayerManager.AttackState.BOW_HOLD ||
               attackState == PlayerManager.AttackState.BOW_PULL;
    }

    private bool CanBlock()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState != PlayerManager.AttackState.L_A_1 &&
               attackState != PlayerManager.AttackState.L_A_2 &&
               attackState != PlayerManager.AttackState.L_A_3 &&
               attackState != PlayerManager.AttackState.H_A_1 &&
               attackState != PlayerManager.AttackState.H_A_2 &&
               attackState != PlayerManager.AttackState.BOW_PULL;
    }

    private bool IsInAttackableState()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;

        return attackState != PlayerManager.AttackState.BLOCK_STAGGER &&
               attackState != PlayerManager.AttackState.BOW_DRAW && 
               attackState != PlayerManager.AttackState.BOW_HOLD;
    }

    private bool IsInBowDrawableState()
    {
        PlayerManager.AttackState attackState = _PlayerManager._AttackState;
        return attackState == PlayerManager.AttackState.IDLE || 
               attackState == PlayerManager.AttackState.BOW_PULL;
    }

    // Interface
    public void EndDodge()
    {
        _PlayerManager._DodgeState = PlayerManager.DodgeState.NONE;
        m_LastDodgeEndTime = Time.time;
    }

}
