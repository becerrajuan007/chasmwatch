﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSounds : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Slime_Move;
    [SerializeField]
    public AudioClip Slime_Pop;
    [SerializeField]
    public AudioClip Slime_Attack;
    [SerializeField]
    public AudioClip Slime_Pain;
    [SerializeField]
    public AudioClip Slime_Death;

    // Interface

    public void PlayMoveSound() { p_AudioSource.PlayOneShot(Slime_Move, 0.4f); }
    public void PlayPopSound() { p_AudioSource.PlayOneShot(Slime_Pop, 0.4f); }
    public void PlayAttackSound() { p_AudioSource.PlayOneShot(Slime_Attack); }
    public void PlayPainSound() { p_AudioSource.PlayOneShot(Slime_Pain); }
    public void PlayDeathSound() { p_AudioSource.PlayOneShot(Slime_Death); }
}
