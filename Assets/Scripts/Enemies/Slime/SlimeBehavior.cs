﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SlimeBehavior : EnemyBehaviorAbs
{
    // Fields
    [SerializeField]
    public float IdleTime = 5.0f;
    [SerializeField]
    public float PatrolDistance = 5.0f;
    [SerializeField]
    public float AttackDistance = 1.0f;
    [SerializeField]
    public float AttackBufferTime = 2.0f;
    [SerializeField]
    public float AttackRotationSpeed = 5.0f;
    [SerializeField]
    public float AttackResetScale = 0.75f;

    // Consts
    private System.Random RAND = new System.Random();

    public override void UpdateBehavior()
    {
        if (CurrentState == AI_STATE.IDLE || CurrentState == AI_STATE.PATROL)
        {
            if (p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.CHASE;
        }
        else if (CurrentState == AI_STATE.CHASE || CurrentState == AI_STATE.ATTACK)
        {
            if (!p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.IDLE;
        }
    }

    // ----------------------------------------------------------------------------------
    // Non-aggro State Machine ----------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Idle()
    {
        while(p_CurrentState == AI_STATE.IDLE)
        {
            p_Agent.isStopped = true;

            // Wait X seconds
            if (Time.time - p_LastIdleTime >= IdleTime)
            {
                // Transition to patrol. Get a destination.
                p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);
                CurrentState = AI_STATE.PATROL;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Patrol()
    {
        while (p_CurrentState == AI_STATE.PATROL)
        {
            p_Agent.isStopped = false;
            p_Agent.SetDestination(p_CurrentPatrolDestination);

            // Wait for A*
            while (p_Agent.pathPending)
            {
                // If path is invalid, then look for a new destination
                if (p_Agent.pathStatus == NavMeshPathStatus.PathInvalid ||
                    p_Agent.pathStatus == NavMeshPathStatus.PathPartial)
                    p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);

                yield return null;
            }

            // Get disitance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_CurrentPatrolDestination);

            // Check for invalid path
            if (float.IsInfinity(destinationDistance))
            {
                p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);
                yield return null;
            }
            
            // Are we close to the destination?
            else if (destinationDistance <= AttackDistance)
            {
                // Transition to idle. Set the last idle time.
                CurrentState = AI_STATE.IDLE;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // ----------------------------------------------------------------------------------
    // Aggro State Machine --------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Chase()
    {
        while (p_CurrentState == AI_STATE.CHASE)
        {
            p_Agent.isStopped = false;
            p_Agent.SetDestination(p_Manager._Percepts.GetPlayerTransform().position);

            // Get distance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // In attack range?
            if (destinationDistance <= (GetNextAttackType() == ATTACK_TYPE.ONE ? AttackDistance : AttackDistance * 2f))
            {
                // Transition to attack. Record last attack time (forwarded to attack faster).
                p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
                CurrentState = AI_STATE.ATTACK;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Attack()
    {
        while (p_CurrentState == AI_STATE.ATTACK)
        {
            p_Agent.isStopped = true;
            
            // Trigger an attack animation
            if (Time.time - p_LastAttackTime >= AttackBufferTime)
            {
                TriggerAttack();
                GenerateNextAttackType((float)RAND.NextDouble());
                p_LastAttackTime = Time.time;
            }
            // Rotate towards player
            else if (!p_Manager._Animator.IsAttacking())
            {
                RotateTowards(p_Manager._Percepts.GetPlayerTransform());
            }

            // Get disitance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // Out of attack range?
            if (destinationDistance > (GetNextAttackType() == ATTACK_TYPE.ONE ? AttackDistance : AttackDistance * 2f))
            {
                // Determine next attack
                GenerateNextAttackType((float)RAND.NextDouble());

                // Transition to attack.
                CurrentState = AI_STATE.CHASE;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // Util

    protected override void TriggerAttack()
    {
        if (GetNextAttackType() == ATTACK_TYPE.ONE)
            ((SlimeAnimator)p_Manager._Animator).TriggerAttack1();
        else
            ((SlimeAnimator)p_Manager._Animator).TriggerAttack2();
    }
    
    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;

        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * AttackRotationSpeed);
    }

    // Interface

    public override void ResetAttackTime()
    {
        p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
    }

}
