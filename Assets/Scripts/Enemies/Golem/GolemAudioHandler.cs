﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Golem_Hover;
    [SerializeField]
    public AudioClip Golem_Alert;
    [SerializeField]
    public AudioClip Golem_Death;

    // Interface

    public void PlayHover() { p_AudioSource.PlayOneShot(Golem_Hover, 1.0f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Golem_Alert, 1.5f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Golem_Alert, 1.5f); }
}
