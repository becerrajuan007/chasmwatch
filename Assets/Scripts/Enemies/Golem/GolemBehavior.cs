﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GolemBehavior : EnemyBehaviorAbs
{
    // Fields
    [SerializeField]
    public float IdleTime = 5.0f;
    [SerializeField]
    public float PatrolDistance = 5.0f;
    [SerializeField]
    public float AttackDistance = 1.0f;
    [SerializeField]
    public float AttackBufferTime = 2.0f;
    [SerializeField]
    public float AttackRotationSpeed = 5.0f;
    [SerializeField]
    public float AttackResetScale = 0.75f;

    public override void UpdateBehavior()
    {
        if (CurrentState == AI_STATE.IDLE || CurrentState == AI_STATE.PATROL)
        {
            if (p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.CHASE;
        }
        else if (CurrentState == AI_STATE.CHASE || CurrentState == AI_STATE.ATTACK)
        {
            if (!p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.IDLE;
        }
    }

    // ----------------------------------------------------------------------------------
    // Non-aggro State Machine ----------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Idle()
    {
        while (p_CurrentState == AI_STATE.IDLE)
        {
            p_Agent.isStopped = true;

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Patrol()
    {
        // Boss enemies do not patrol. Simply return to Idle();
        CurrentState = AI_STATE.IDLE;
        yield break;
    }

    // ----------------------------------------------------------------------------------
    // Aggro State Machine --------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Chase()
    {
        while (p_CurrentState == AI_STATE.CHASE)
        {
            p_Agent.isStopped = false;
            p_Agent.SetDestination(p_Manager._Percepts.GetPlayerTransform().position);

            // Get distance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // In attack range?
            if (destinationDistance <= AttackDistance)
            {
                // Transition to attack. Record last attack time (forwarded to attack faster).
                p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
                CurrentState = AI_STATE.ATTACK;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Attack()
    {
        while (p_CurrentState == AI_STATE.ATTACK)
        {
            p_Agent.isStopped = true;

            // Trigger an attack animation
            if (Time.time - p_LastAttackTime >= AttackBufferTime)
            {
                TriggerAttack();
                p_LastAttackTime = Time.time;
            }
            // Rotate towards player
            else if (!p_Manager._Animator.IsAttacking())
            {
                RotateTowards(p_Manager._Percepts.GetPlayerTransform());
            }

            // Get disitance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // Out of attack range?
            if (destinationDistance > AttackDistance)
            {
                // Transition to attack.
                CurrentState = AI_STATE.CHASE;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // Util

    protected override void TriggerAttack()
    {
        System.Random rand = new System.Random();

        if (rand.NextDouble() < 0.5f)
            ((GolemAnimator)p_Manager._Animator).TriggerAttack1();
        else
            ((GolemAnimator)p_Manager._Animator).TriggerAttack2();
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;

        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * AttackRotationSpeed);
    }

    // Interface

    public override void ResetAttackTime()
    {
        p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
    }
}
