﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Bat_Flap;
    [SerializeField]
    public AudioClip Bat_Alert;
    [SerializeField]
    public AudioClip Bat_Attack;
    [SerializeField]
    public AudioClip Bat_Death;

    // Interface

    public void PlayFlap() { p_AudioSource.PlayOneShot(Bat_Flap, 0.3f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Bat_Alert, 0.6f); }
    public void PlayAttack() { p_AudioSource.PlayOneShot(Bat_Attack, 0.6f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Bat_Death, 0.6f); }

}
