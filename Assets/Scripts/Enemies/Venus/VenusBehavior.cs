﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class VenusBehavior : EnemyBehaviorAbs
{
    // Fields
    [SerializeField]
    public float AttackDistance = 1.0f;
    [SerializeField]
    public float AttackBufferTime = 2.0f;
    [SerializeField]
    public float AttackRotationSpeed = 5.0f;
    [SerializeField]
    public float AttackResetScale = 0.75f;

    // ----------------------------------------------------------------------------------
    // Non-aggro State Machine ----------------------------------------------------------
    // ----------------------------------------------------------------------------------

    public override void UpdateBehavior()
    {
        if (CurrentState == AI_STATE.IDLE || CurrentState == AI_STATE.PATROL)
        {
            if (p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.CHASE;
        }
        else if (CurrentState == AI_STATE.CHASE || CurrentState == AI_STATE.ATTACK)
        {
            if (!p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.IDLE;
        }
    }

    protected override IEnumerator Idle()
    {
        while (p_CurrentState == AI_STATE.IDLE)
        {
            p_Agent.isStopped = true;

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Patrol()
    {
        while (p_CurrentState == AI_STATE.PATROL)
        {
            // Transition to idle. Set the last idle time.
            CurrentState = AI_STATE.IDLE;

            yield break;
        }
    }

    protected override IEnumerator Chase()
    {
        while (p_CurrentState == AI_STATE.CHASE)
        {
            // Transition to idle. Set the last idle time.
            CurrentState = AI_STATE.ATTACK;

            yield break;
        }
    }

    // ----------------------------------------------------------------------------------
    // Aggro State Machine --------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Attack()
    {
        while (p_CurrentState == AI_STATE.ATTACK)
        {
            p_Agent.isStopped = true;

            RotateTowards(p_Manager._Percepts.GetPlayerTransform());

            // Trigger an attack animation
            if (Time.time - p_LastAttackTime >= AttackBufferTime)
            {
                TriggerAttack();
                p_LastAttackTime = Time.time;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // Util

    protected override void TriggerAttack()
    {
        ((VenusAnimator)p_Manager._Animator).TriggerAttack1();
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;

        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * AttackRotationSpeed);
    }

    // Interface 

    public override void ResetAttackTime()
    {
        p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
    }

    public void FairResetAttackTime()
    {
        p_LastAttackTime = Time.time;
    }

    public void SpawnEggs()
    {
        VenusBossSystem.SpawnEggs(true);
    }
}
