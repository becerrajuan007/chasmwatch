﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class VenusPercepts : EnemyPerceptsAbs
{
    public override void UpdatePercepts()
    {
        bool playerInSight = (p_PlayerTransform != null);

        p_InSight = playerInSight;
        p_Aggroed = playerInSight;
    }

    public override Vector3 GetRandomPatrolDestination(float walkDistance)
    {
        // Not used
        return Vector3.zero;
    }
}
