﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusAudioBehavior : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Venus_Roar;
    [SerializeField]
    public AudioClip Venus_Attack;
    [SerializeField]
    public AudioClip Venus_Dizzy;
    [SerializeField]
    public AudioClip Venus_Fall;

    // Interface

    public void PlayRoar() { p_AudioSource.PlayOneShot(Venus_Roar, 1.8f); }
    public void PlayAttack() { p_AudioSource.PlayOneShot(Venus_Attack, 1.8f); }
    public void PlayDizzy() { p_AudioSource.PlayOneShot(Venus_Dizzy, 0.8f); }
    public void PlayFall() { p_AudioSource.PlayOneShot(Venus_Fall, 1.2f); }

}
