﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusAnimator : EnemyAnimatorAbs
{
    // Member constants
    private const float STAGGER_TIME = 12f;

    private readonly int h_Attack1 = Animator.StringToHash("Attack1");
    private readonly int h_Attack2 = Animator.StringToHash("Attack2");
    private readonly int h_StaggerRecover = Animator.StringToHash("StaggerRecover");

    private float m_LastStaggerTime;
    private bool m_PushedPlayer;

    public override void UpdateAnimatorData()
    {
        p_Animator.SetFloat(p_ForwardVelocity, p_Manager._Behavior.GetCurrentForwardVelocity());
        p_Animator.SetBool(p_IsInCombat, p_Manager._Percepts.HasBeenAggroed());

        // Do gate and stagger events

        if (p_Animator.GetCurrentAnimatorStateInfo(0).IsName("Stagger") &&
            Time.time - m_LastStaggerTime >= STAGGER_TIME)
        {
            RecoverFromStagger();
        }
        else if (!m_PushedPlayer)
        {
            float remainingTime = m_LastStaggerTime + STAGGER_TIME - Time.time;

            if (remainingTime <= 1f && remainingTime >= 0f)
            {
                PushPlayer();
            }
        }
    }

    // Util

    private void RecoverFromStagger()
    {
        VenusBossSystem.ResetTentacles();
        ((VenusBehavior)p_Manager._Behavior).FairResetAttackTime();
        p_Animator.SetTrigger(h_StaggerRecover);
    }

    private void PushPlayer()
    {
        VenusBossSystem.PushPlayerFromGate();
        p_Manager._Hurtbox.EnableHurtbox(false);
        m_PushedPlayer = true;
    }

    // Interface 

    public void TriggerBossStagger()
    {
        m_LastStaggerTime = Time.time;
        m_PushedPlayer = false;
        p_Manager._Hurtbox.EnableHurtbox(true);
        TriggerStagger();
    }

    public void TriggerAttack1() { p_Animator.SetTrigger(h_Attack1); }
    public void TriggerAttack2() { p_Animator.SetTrigger(h_Attack2); }
    public override bool IsAttacking()
    {
        return p_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1") ||
               p_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2");
    }
}
