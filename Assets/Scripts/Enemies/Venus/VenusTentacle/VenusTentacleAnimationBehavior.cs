﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusTentacleAnimationBehavior : StateMachineBehaviour
{
    private const float START = 0.45f;
    private const float END = 0.72432f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float progress = stateInfo.normalizedTime % 1;

        if (stateInfo.IsName("IdleLoop") && progress < START)
        {
            animator.Play("IdleLoop", -1, START);
        }
        else if (stateInfo.IsName("Death") && progress < END)
        {
            animator.Play("Death", -1, END);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float progress = stateInfo.normalizedTime % 1;

        if (IsNonDeathState(stateInfo) && progress >= END)
        {
            animator.SetTrigger("IdleTrigger");
        }
    }

    private bool IsNonDeathState(AnimatorStateInfo stateInfo)
    {
        return stateInfo.IsName("Rise") || stateInfo.IsName("IdleLoop");
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
