﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusBossSystem : MonoBehaviour
{
    // Singleton instance
    private static VenusBossSystem _instance;

    public static VenusBossSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    [SerializeField]
    public VenusGate Gate;
    [SerializeField]
    public EnemySpawner VenusSpawner;
    [SerializeField]
    public List<SpikeyEgg> SpikeyEggs;

    private static EnemySpawner s_TentacleSpawnerOne;
    private static EnemySpawner s_TentacleSpawnerTwo;

    private static List<SpikeyEgg> s_Eggs;
    private static int s_EggsIndex;
    private static float m_LastEggSpawnTime;
    private static bool m_ShouldSpawnEggs;
    private const float EGG_SPAWN_TIME = 15f;

    private static VenusGate s_VenusGate;
    private static EnemyManager s_Venus;
    private static bool s_FirstTentacle;

    private void Start()
    {
        s_VenusGate = Gate;
        s_Venus = VenusSpawner.Monster.GetComponent<EnemyManager>();

        s_Eggs = SpikeyEggs;
        s_EggsIndex = 0;
    }

    private void Update()
    {
        if (m_ShouldSpawnEggs && Time.time - EGG_SPAWN_TIME >= m_LastEggSpawnTime)
        {
            s_Eggs[s_EggsIndex++].DropEgg();

            if (s_EggsIndex >= s_Eggs.Count) s_EggsIndex = 0;

            m_LastEggSpawnTime = Time.time;
        }
    }

    // Interface

    public static void SendTentacleDeathSignal(EnemySpawner tentacleSpawner)
    {
        if (!s_FirstTentacle)
            s_FirstTentacle = true;
        else
        {
            ((VenusAnimator)s_Venus._Animator).TriggerBossStagger();

            s_VenusGate.EnableGate(false);
        }

        if (!s_TentacleSpawnerOne)
            s_TentacleSpawnerOne = tentacleSpawner;
        else if (!s_TentacleSpawnerTwo)
            s_TentacleSpawnerTwo = tentacleSpawner;
    }

    public static void ResetTentacles()
    {
        s_VenusGate.EnableGate(true);

        s_TentacleSpawnerOne.SpawnEnemy();
        s_TentacleSpawnerTwo.SpawnEnemy();

        s_FirstTentacle = false;
    }

    public static void PushPlayerFromGate()
    {
        s_VenusGate.PushPlayer();
    }

    public static void DestroyGate()
    {
        s_VenusGate.DisableSelf();
    }

    public static void SpawnEggs(bool shouldSpawn)
    {
        m_ShouldSpawnEggs = shouldSpawn;
        m_LastEggSpawnTime = Time.time;
    }
}
