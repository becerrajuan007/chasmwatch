﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusTentacleBehavior : EnemyBehaviorAbs
{
    // ----------------------------------------------------------------------------------
    // Non-aggro State Machine ----------------------------------------------------------
    // ----------------------------------------------------------------------------------

    public override void UpdateBehavior()
    {
        CurrentState = AI_STATE.IDLE;
    }

    protected override IEnumerator Idle()
    {
        while (p_CurrentState == AI_STATE.IDLE)
        {
            p_Agent.isStopped = true;

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Patrol()
    {
        while (p_CurrentState == AI_STATE.PATROL)
        {
            // Transition to idle. Set the last idle time.
            CurrentState = AI_STATE.IDLE;

            yield break;
        }
    }

    protected override IEnumerator Chase()
    {
        while (p_CurrentState == AI_STATE.CHASE)
        {
            // Transition to idle. Set the last idle time.
            CurrentState = AI_STATE.IDLE;

            yield break;
        }
    }

    // ----------------------------------------------------------------------------------
    // Aggro State Machine --------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Attack()
    {
        while (p_CurrentState == AI_STATE.ATTACK)
        {
            // Transition to idle. Set the last idle time.
            CurrentState = AI_STATE.IDLE;

            yield break;
        }
    }

    // Util

    protected override void TriggerAttack()
    {
        // Cannot attack
    }

    // Interface 

    public override void ResetAttackTime()
    {
        // Do nothing
    }
}
