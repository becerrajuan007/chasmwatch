﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusTentaclePercepts : EnemyPerceptsAbs
{
    public override Vector3 GetRandomPatrolDestination(float walkDistance)
    {
        // Not needed 
        return Vector3.zero;
    }

    public override void UpdatePercepts()
    {
        // Do not need to perceive
    }
}
