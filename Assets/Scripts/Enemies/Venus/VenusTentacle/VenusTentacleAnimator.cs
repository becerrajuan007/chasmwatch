﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusTentacleAnimator : EnemyAnimatorAbs
{
    public override bool IsAttacking()
    {
        return false;
    }

    public override void UpdateAnimatorData()
    {
        // No need
    }
}
