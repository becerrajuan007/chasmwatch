﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusTentacleAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Tentacle_Gross;

    // Interface
    public void PlayGross() { p_AudioSource.PlayOneShot(Tentacle_Gross, 1.5f); }

}
