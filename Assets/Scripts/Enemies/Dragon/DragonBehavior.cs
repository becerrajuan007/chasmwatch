﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DragonBehavior : EnemyBehaviorAbs
{
    // Fields
    [SerializeField]
    public float IdleTime = 5.0f;
    [SerializeField]
    public float PatrolDistance = 5.0f;
    [SerializeField]
    public float AttackDistance = 1.0f;
    [SerializeField]
    public float AttackBufferTime = 2.0f;
    [SerializeField]
    public float AttackRotationSpeed = 5.0f;
    [SerializeField]
    public float AttackResetScale = 0.75f;

    // ----------------------------------------------------------------------------------
    // Non-aggro State Machine ----------------------------------------------------------
    // ----------------------------------------------------------------------------------

    public override void UpdateBehavior()
    {
        if (CurrentState == AI_STATE.IDLE || CurrentState == AI_STATE.PATROL)
        {
            if (p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.CHASE;
        }
        else if (CurrentState == AI_STATE.CHASE || CurrentState == AI_STATE.ATTACK)
        {
            if (!p_Manager._Percepts.HasBeenAggroed()) CurrentState = AI_STATE.IDLE;
        }
    }

    protected override IEnumerator Idle()
    {
        while (p_CurrentState == AI_STATE.IDLE)
        {
            p_Agent.isStopped = true;

            // Wait X seconds
            if (Time.time - p_LastIdleTime >= IdleTime)
            {
                // Transition to patrol. Get a destination.
                p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);
                CurrentState = AI_STATE.PATROL;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Patrol()
    {
        while (p_CurrentState == AI_STATE.PATROL)
        {
            p_Agent.isStopped = false;
            p_Agent.SetDestination(p_CurrentPatrolDestination);

            // Wait for A*
            while (p_Agent.pathPending)
            {
                // If path is invalid, then look for a new destination
                if (p_Agent.pathStatus == NavMeshPathStatus.PathInvalid ||
                    p_Agent.pathStatus == NavMeshPathStatus.PathPartial)
                    p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);

                yield return null;
            }

            // Get disitance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_CurrentPatrolDestination);

            // Check for invalid path
            if (float.IsInfinity(destinationDistance))
            {
                p_CurrentPatrolDestination = p_Manager._Percepts.GetRandomPatrolDestination(PatrolDistance);
                yield return null;
            }

            // Are we close to the destination?
            else if (destinationDistance <= (AttackDistance / 5))
            {
                // Transition to idle. Set the last idle time.
                CurrentState = AI_STATE.IDLE;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    protected override IEnumerator Chase()
    {
        while (p_CurrentState == AI_STATE.CHASE)
        {
            p_Agent.isStopped = false;

            Vector3 playerPosition = p_Manager._Percepts.GetPlayerTransform().position;
            Vector3 adjustedDestination = new Vector3(playerPosition.x, transform.position.y, playerPosition.z);
            p_Agent.SetDestination(adjustedDestination);

            // Get distance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // In attack range?
            if (destinationDistance <= AttackDistance)
            {
                // Transition to attack. Record last attack time (forwarded to attack faster).
                p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
                CurrentState = AI_STATE.ATTACK;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // ----------------------------------------------------------------------------------
    // Aggro State Machine --------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    protected override IEnumerator Attack()
    {
        while (p_CurrentState == AI_STATE.ATTACK)
        {
            p_Agent.isStopped = true;

            // Trigger an attack animation
            if (Time.time - p_LastAttackTime >= AttackBufferTime)
            {
                TriggerAttack();
                p_LastAttackTime = Time.time;
            }
            // Rotate towards player
            else if (!p_Manager._Animator.IsAttacking())
            {
                RotateTowards(p_Manager._Percepts.GetPlayerTransform());
            }

            // Get disitance to destination
            float destinationDistance = Vector3.Distance(transform.position, p_Manager._Percepts.GetPlayerTransform().position);

            // Out of attack range?
            if (destinationDistance > AttackDistance)
            {
                // Transition to attack.
                CurrentState = AI_STATE.CHASE;

                yield break;
            }

            // Wait for next frame
            yield return null;
        }
    }

    // Util

    protected override void TriggerAttack()
    {
        // Note - Attack 1 is a melee attack, and we likely won't need it.
        // For now, just use attack 2 (the animator will spawn a projectile)
        ((DragonAnimator)p_Manager._Animator).TriggerAttack2();
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;

        transform.rotation = Quaternion.LookRotation(direction);
    }

    // Interface 

    public override void ResetAttackTime()
    {
        p_LastAttackTime = Time.time - (AttackBufferTime * AttackResetScale);
    }

    public void SpawnFireball()
    {
        // Spawn a projectile
        EnemyPools.SpawnFireball(transform.position + new Vector3(0, 1, 0), transform.rotation);
    }
}
