﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAnimator : EnemyAnimatorAbs
{
    // Member constants
    private readonly int h_Attack1 = Animator.StringToHash("Attack1");
    private readonly int h_Attack2 = Animator.StringToHash("Attack2");

    public override void UpdateAnimatorData()
    {
        p_Animator.SetFloat(p_ForwardVelocity, p_Manager._Behavior.GetCurrentForwardVelocity());
        p_Animator.SetBool(p_IsInCombat, p_Manager._Percepts.HasBeenAggroed());
    }

    public void TriggerAttack1() { p_Animator.SetTrigger(h_Attack1); }
    public void TriggerAttack2() { p_Animator.SetTrigger(h_Attack2); }
    public override bool IsAttacking()
    {
        return p_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1") ||
               p_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2");
    }
}
