﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonFireballProjectile : MonoBehaviour
{
    // Consts
    private const float FIREBALL_LIFETIME = 5.0f;
    private const float FIREBALL_SPEED = 7.5f;
    private const float FIREBALL_DAMAGE = 12f;

    // Members
    private float m_LastEnableTime;

    private void Awake()
    {
        m_LastEnableTime = -1.0f;
    }

    private void OnEnable()
    {
        m_LastEnableTime = Time.time;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check if we hit the player
        if (other.tag.Equals("Player"))
        {
            CombatEventSystem.SendAttackSignal(other, new System.Random().Next(), FIREBALL_DAMAGE, FIREBALL_DAMAGE, transform);
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        // Slowly go forward
        transform.position += (transform.forward * FIREBALL_SPEED) * Time.deltaTime;

        // Check if we should disappear
        if (Time.time >= m_LastEnableTime + FIREBALL_LIFETIME)
            gameObject.SetActive(false);
    }
}
