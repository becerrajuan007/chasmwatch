﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Dragon_Flap;
    [SerializeField]
    public AudioClip Dragon_Alert;
    [SerializeField]
    public AudioClip Dragon_Fire;
    [SerializeField]
    public AudioClip Dragon_Death;

    // Interface

    public void PlayFlap() { p_AudioSource.PlayOneShot(Dragon_Flap, 1.0f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Dragon_Alert, 1.0f); }
    public void PlayFire() { p_AudioSource.PlayOneShot(Dragon_Fire, 1.0f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Dragon_Death, 1.0f); }
}
