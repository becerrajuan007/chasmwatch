﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Mage_Step;
    [SerializeField]
    public AudioClip Mage_Laugh;
    [SerializeField]
    public AudioClip Mage_Swing;
    [SerializeField]
    public AudioClip Mage_Fire;
    [SerializeField]
    public AudioClip Mage_Death;

    // Interface

    public void PlayStep() { p_AudioSource.PlayOneShot(Mage_Step, 0.8f); }
    public void PlayLaugh() { p_AudioSource.PlayOneShot(Mage_Laugh, 2.5f); }
    public void PlaySwing() { p_AudioSource.PlayOneShot(Mage_Swing, 0.9f); }
    public void PlayFire() { p_AudioSource.PlayOneShot(Mage_Fire, 1.0f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Mage_Death, 2.5f); }

}
