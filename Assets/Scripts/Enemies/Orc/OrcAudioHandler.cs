﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Orc_Footstep;
    [SerializeField]
    public AudioClip Orc_Alert;
    [SerializeField]
    public AudioClip Orc_Attack1;
    [SerializeField]
    public AudioClip Orc_Attack2;
    [SerializeField]
    public AudioClip Orc_Death;

    // Interface

    public void PlayFootstep() { p_AudioSource.PlayOneShot(Orc_Footstep, 0.5f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Orc_Alert, 1.0f); }
    public void PlayAttack1() { p_AudioSource.PlayOneShot(Orc_Attack1, 1.0f); }
    public void PlayAttack2() { p_AudioSource.PlayOneShot(Orc_Attack2, 2.0f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Orc_Death, 1.0f); }
}
