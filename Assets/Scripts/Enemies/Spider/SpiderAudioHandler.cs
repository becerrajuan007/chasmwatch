﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Spider_Move;
    [SerializeField]
    public AudioClip Spider_Alert;
    [SerializeField]
    public AudioClip Spider_Death;

    public void PlayMove() { p_AudioSource.PlayOneShot(Spider_Move, 0.5f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Spider_Alert, 0.5f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Spider_Death, 0.5f); }
}
