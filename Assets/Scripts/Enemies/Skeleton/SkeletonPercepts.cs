﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SkeletonPercepts : EnemyPerceptsAbs
{
    // Fields
    [SerializeField]
    public float EyeSightAngle = 60f;

    public override void UpdatePercepts()
    {
        if (p_PlayerTransform != null)
        {
            RaycastHit hit;
            if (Physics.Linecast(p_EyePoint.position, p_PlayerTransform.position, out hit, p_LayerMask) && hit.collider.tag.Equals("Player"))
            {
                Vector3 adjustedPlayerPosition = new Vector3(p_PlayerTransform.position.x, p_EyePoint.position.y, p_PlayerTransform.position.z);
                float angle = Vector3.Angle(adjustedPlayerPosition - p_EyePoint.position, p_EyePoint.forward);
                float distance = Vector3.Distance(p_EyePoint.position, p_PlayerTransform.position);

                if (distance <= 5f || angle <= EyeSightAngle / 2)
                {
                    // Player is in sight and enemy is aggroed
                    p_InSight = true;
                    p_Aggroed = true;
                }
                else
                {
                    // Player is not in sight (bad angle), but enemy may be aggroed
                    p_InSight = false;
                }
            }
            else
            {
                // Player is not in sight (view obstructed), but enemy may be aggroed
                p_InSight = false;
            }
        }
        else
        {
            // Player is not in sight and is out of range
            p_InSight = false;
            p_Aggroed = false;
        }
    }

    public override Vector3 GetRandomPatrolDestination(float walkDistance)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * ((CapsuleCollider)p_ChaseRange).radius;
        randomDirection += transform.parent.position;

        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, walkDistance, 1);

        return hit.position;
    }
}
