﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Skeleton_Step;
    [SerializeField]
    public AudioClip Skeleton_Alert;
    [SerializeField]
    public AudioClip Skeleton_Slash;
    [SerializeField]
    public AudioClip Skeleton_Death;

    // Interface

    public void PlayStep() { p_AudioSource.PlayOneShot(Skeleton_Step, 0.6f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Skeleton_Alert, 0.5f); }
    public void PlaySlash() { p_AudioSource.PlayOneShot(Skeleton_Slash, 0.8f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Skeleton_Death, 0.5f); }
}
