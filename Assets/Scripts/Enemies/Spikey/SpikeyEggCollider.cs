﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeyEggCollider : MonoBehaviour
{
    // Const 
    private readonly string GROUND_TAG = "Ground";

    // Members
    private SpikeyEgg m_Parent;

    public void Init(SpikeyEgg parent)
    {
        m_Parent = parent;
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals(GROUND_TAG))
        {
            m_Parent.SpawnSpikey();
        }
    }
}
