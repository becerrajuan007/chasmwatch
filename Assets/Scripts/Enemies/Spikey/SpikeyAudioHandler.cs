﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeyAudioHandler : EnemyAudioHandlerAbs
{
    // Critical fields
    [SerializeField]
    public AudioClip Spikey_Move;
    [SerializeField]
    public AudioClip Spikey_Alert;
    [SerializeField]
    public AudioClip Spikey_Attack;
    [SerializeField]
    public AudioClip Spikey_Death;

    // Interface

    public void PlayMove() { p_AudioSource.PlayOneShot(Spikey_Move, 0.6f); }
    public void PlayAlert() { p_AudioSource.PlayOneShot(Spikey_Alert, 1.0f); }
    public void PlayAttack() { p_AudioSource.PlayOneShot(Spikey_Attack, 1.0f); }
    public void PlayDeath() { p_AudioSource.PlayOneShot(Spikey_Death, 1.0f); }
}
