﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeyEgg : MonoBehaviour
{
    // Const
    private const float FALL_SPEED = 10f;
    private readonly string[] COLLIDER_TAGS = { "Ground" };

    // Members
    private LayerMask m_LayerMask;
    private SpikeyEggCollider m_Collider;
    private EnemySpawner m_SpikeySpawner;
    private bool m_EggIsFalling;

    private void Awake()
    {
        m_Collider = GetComponentInChildren<SpikeyEggCollider>();
        m_SpikeySpawner = GetComponentInChildren<EnemySpawner>();
        m_LayerMask = LayerMask.GetMask(COLLIDER_TAGS);

        SetSpikeySpawnerLocation();

        m_Collider.Init(this);
    }

    private void SetSpikeySpawnerLocation()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 100f, m_LayerMask))
        {
            m_SpikeySpawner.transform.position = hit.point;
        }
    }

    private void Update()
    {
        if (m_EggIsFalling)
        {
            m_Collider.transform.position -= new Vector3(0f, FALL_SPEED, 0f) * Time.deltaTime;
        }
    }

    // Interface

    public void DropEgg()
    {
        m_Collider.gameObject.SetActive(true);
        m_Collider.transform.position = new Vector3(m_Collider.transform.position.x, transform.position.y, m_Collider.transform.position.z);
        m_EggIsFalling = true;
    }

    public void SpawnSpikey()
    {
        m_EggIsFalling = false;

        m_SpikeySpawner.UniqueSpawnEnemy();

        m_Collider.gameObject.SetActive(false);
    }
}
