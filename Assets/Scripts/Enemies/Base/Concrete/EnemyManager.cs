﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // Sub-scripts

    // Concrete
    public EnemyAttributes _Attributes { get; private set; }
    public EnemyPerceptsAbs _Percepts { get; private set; }
    public EnemyBehaviorAbs _Behavior { get; private set; }
    public EnemyAnimatorAbs _Animator { get; private set; }
    public EnemyAudioHandlerAbs _Audio { get; private set; }
    public EnemyHitbox _Hitbox { get; private set; }
    public EnemyHurtbox _Hurtbox { get; set; }

    // Components
    private GameObject m_EnemyActor;

    // Public members
    public float InitTime { get; private set; }

    private void Awake()
    {
        InitTime = Time.time;

        _Attributes = GetComponent<EnemyAttributes>();

        _Percepts = GetComponentInChildren<EnemyPerceptsAbs>();
        _Behavior = GetComponent<EnemyBehaviorAbs>();
        _Animator = GetComponent<EnemyAnimatorAbs>();
        _Audio = GetComponent<EnemyAudioHandlerAbs>();
        _Hitbox = FindDeepChild(transform, "Hitbox").GetComponent<EnemyHitbox>();
        _Hurtbox = GetComponent<EnemyHurtbox>();

        m_EnemyActor = _Attributes.gameObject;
    }

    private void Update()
    {
        // Update attributes
        _Attributes.UpdateAttributes();

        // Update percepts
        _Percepts.UpdatePercepts();

        // Update behavior
        _Behavior.UpdateBehavior();

        // Update animator data
        _Animator.UpdateAnimatorData();
    }

    // Util

    private Transform FindDeepChild(Transform parent, string name)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(parent);

        while (queue.Count > 0)
        {
            Transform transform = queue.Dequeue();
            if (transform.name == name)
                return transform;
            foreach (Transform t in transform)
                queue.Enqueue(t);
        }

        Debug.LogError(name + " could not be found.");
        return null;
    }

    // Interface

    public void EnableEnemy(bool enable)
    {
        if (enable && _Attributes.CanRespawn() && _Attributes.EventIndex != 1000)
        {
            Spawn();
        }
        else
        {
            // If this enemy was in the player's collider set, remove it
            CombatEventSystem.RemovePlayerTarget(_Hurtbox.GetHurtbox());

            _Hurtbox.EnableHurtbox(false);
            m_EnemyActor.transform.localPosition = new Vector3();
            m_EnemyActor.transform.localRotation = new Quaternion();

            m_EnemyActor.SetActive(false);
        }
    }

    public void Spawn()
    {
        if (m_EnemyActor.activeSelf) return;

        m_EnemyActor.SetActive(true);

        InitTime = Time.time;

        _Attributes.ResetAttributes();
        _Behavior.ResetAI();
        _Percepts.ResetPercepts();
        _Hurtbox.EnableHurtbox(true);
    }
}
