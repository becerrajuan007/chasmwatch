﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitbox : MonoBehaviour
{
    // Super-script
    private EnemyManager _Manager;

    // Constants
    private readonly System.Random c_Random = new System.Random();

    // Components
    private Collider m_Hitbox;

    // Members
    private int m_CurrentAttackID;

    private void Awake()
    {
        _Manager = transform.root.GetComponentInChildren<EnemyManager>();

        m_Hitbox = GetComponent<Collider>();
        m_Hitbox.enabled = false;

        m_CurrentAttackID = 0;
    }

    // Triggers

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            CombatEventSystem.SendAttackSignal(other, m_CurrentAttackID, _Manager._Attributes.GetDamage(), _Manager._Attributes.GetPoiseDamage(), _Manager.transform);
        }
    }

    // Interface
    
    public void EnableHitbox(int enable)
    {
        m_CurrentAttackID = c_Random.Next();
        m_Hitbox.enabled = (enable == 1);
    }
}
