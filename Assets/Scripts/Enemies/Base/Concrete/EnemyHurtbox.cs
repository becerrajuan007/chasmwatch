﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHurtbox : MonoBehaviour
{
    // Fields
    [SerializeField]
    public int AttackIDSetSize = 3;

    // Super-script
    private EnemyManager _EnemyManager;

    // Components
    private Collider m_Collider;
    private Rigidbody m_RigidBody;

    // Members
    private Queue<int> m_AttackIDSet;

    private void Awake()
    {
        _EnemyManager = GetComponent<EnemyManager>();

        m_Collider = GetComponent<Collider>();
        m_RigidBody = GetComponent<Rigidbody>();

        m_AttackIDSet = new Queue<int>();
    }

    // Util
    private void PushAttackID(int attackID)
    {
        m_AttackIDSet.Enqueue(attackID);

        if (m_AttackIDSet.Count > AttackIDSetSize)
            m_AttackIDSet.Dequeue();
    }

    private bool CheckAttackID(int attackID)
    {
        return m_AttackIDSet.Contains(attackID);
    }

    // Interface

    public bool ReceiveDamageSignal(float damage, float poise, int attackID, Transform source)
    {
        if (!CheckAttackID(attackID))
        {
            PushAttackID(attackID);

            _EnemyManager._Percepts.TriggerAggro(source);
            _EnemyManager._Attributes.ChangeHealth(-damage);
            _EnemyManager._Attributes.ChangePoise(-poise);

            // Notify nearby enemies
            _EnemyManager._Percepts.NotifyNearbyEnemies();

            return true;
        }

        return false;
    }

    public bool ReceiveForceSignal(Vector3 forceVector)
    {
        m_RigidBody.isKinematic = false;

        m_RigidBody.AddForce(forceVector, ForceMode.Acceleration);

        m_RigidBody.isKinematic = true;

        return true;
    }

    public void EnableHurtbox(bool enable)
    {
        m_Collider.enabled = enable;
    }

    public Collider GetHurtbox() { return m_Collider; }

}
