﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttributes : MonoBehaviour
{
    // Fields
    [SerializeField]
    public string Name = "__enemy__";
    [SerializeField]
    public int Level = 1;
    [SerializeField]
    public EnemyType Type = EnemyType.Slime;
    [SerializeField]
    public Transform TargetTransform;
    [SerializeField]
    public int EventIndex;

    // Super-script
    private EnemyManager _EnemyManager;

    // Constants
    private UnityEngine.Object m_ArrowPrefab;
    private UnityEngine.Object m_HeartPrefab;
    private static LayerMask m_PickupMask;
    private const float DAMAGE_TAKEN_TIME = 2f;
    private static PlayerAttributes m_PlayerAttributes;

    // Members
    private float m_CurrentHealth, m_MaxHealth;
    private float m_CurrentPoise, m_MaxPoise;
    private float m_Damage, m_PoiseDamage;

    private float m_RecentDamageTaken;
    private float m_LastDamageTakenTime;

    private bool m_Permadeath;
    private bool m_HasDied = false;

    // Enums
    public enum EnemyType { Slime, Bat, Spider, Venus, Tentacle, Skeleton, Spikey, Dragon, Orc, Mage, Golem }

    private void Awake()
    {
        _EnemyManager = GetComponent<EnemyManager>();

        Level = (int)Mathf.Clamp(Level, 1f, 100f);
        Name = Type + " (" + Level + ")";

        switch (Type)
        {
            case EnemyType.Slime:
                m_MaxHealth = 10f + (5f * Level);
                m_MaxPoise = 0f;
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Bat:
                m_MaxHealth = 10 + Level;
                m_MaxPoise = 0f;
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Spider:
                m_MaxHealth = 10f + (2f * Level);
                m_MaxPoise = 0f;
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Venus:
                m_MaxHealth = 800f;
                m_MaxPoise = 100000f;
                m_Damage = 15f;
                m_PoiseDamage = 20f;
                m_Permadeath = true;
                break;
            case EnemyType.Tentacle:
                m_MaxHealth = 60f;
                m_MaxPoise = 0f;
                m_Damage = 0f;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Skeleton:
                m_MaxHealth = 100f + (5f * Level);
                m_MaxPoise = 500f + (2f * Level);
                m_Damage = 15f;
                m_PoiseDamage = 3f * Level;
                m_Permadeath = false;
                break;
            case EnemyType.Spikey:
                m_MaxHealth = 10f + (2f * Level);
                m_MaxPoise = 0f;
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Dragon:
                m_MaxHealth = 10f + Level;
                m_MaxPoise = 0f;
                m_Damage = 20f;
                m_PoiseDamage = 0f;
                m_Permadeath = false;
                break;
            case EnemyType.Orc:
                m_MaxHealth = 50f + (18f * Level);
                m_MaxPoise = 10f + (5f * Level);
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 1.0f * Level;
                m_Permadeath = true;
                break;
            case EnemyType.Mage:
                m_MaxHealth = 50f + (15f * Level);
                m_MaxPoise = 10f + (4f * Level);
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 1.0f * Level;
                m_Permadeath = true;
                break;
            case EnemyType.Golem:
                m_MaxHealth = 50f + (15f * Level);
                m_MaxPoise = 10f + (4f * Level);
                m_Damage = 1.0f * Level;
                m_PoiseDamage = 1.0f * Level;
                m_Permadeath = true;
                break;
            default:
                Debug.LogError(Type + " has not been defined.");
                break;
        }

        m_CurrentHealth = m_MaxHealth;
        m_CurrentPoise = m_MaxPoise;
        m_RecentDamageTaken = 0f;
        m_LastDamageTakenTime = -10000f;

        m_ArrowPrefab = (GameObject)Resources.Load("Drops/ArrowDrop");
        m_HeartPrefab = (GameObject)Resources.Load("Drops/HeartDrop");
        m_PickupMask = LayerMask.GetMask(new string[] { "Ground" });
        if (!m_PlayerAttributes) m_PlayerAttributes = FindObjectOfType<PlayerAttributes>();
    }

    // Update

    public void UpdateAttributes()
    {
        // Update recent damage taken
        if (m_RecentDamageTaken > 0f && Time.time - m_LastDamageTakenTime >= DAMAGE_TAKEN_TIME)
        {
            m_RecentDamageTaken = 0f;
        }
    }

    // Interface

    public void ResetAttributes()
    {
        m_CurrentHealth = m_MaxHealth;
        m_CurrentPoise = m_MaxPoise;
    }

    public int GetCurrentHealth() { return (int)m_CurrentHealth; }
    public int GetMaxHealth() { return (int)m_MaxHealth; }
    public void ChangeHealth(float delta)
    { 
        m_CurrentHealth = Mathf.Clamp(m_CurrentHealth + delta, 0f, m_MaxHealth);

        // Update recent damage time
        if (delta <= 0f)
        {
            m_LastDamageTakenTime = Time.time;
            m_RecentDamageTaken -= delta;
        }

        // Enemy is dead
        if (m_CurrentHealth == 0f)
        {
            m_LastDamageTakenTime = -10000f;
            CombatEventSystem.DispatchEnemyDeathSignal(gameObject.GetComponent<Collider>());
            _EnemyManager._Hurtbox.EnableHurtbox(false);
            _EnemyManager._Behavior.TriggerDeath();
            _EnemyManager._Animator.TriggerDeath();
        }
    }

    public int GetCurrentPoise() { return (int)m_CurrentPoise; }
    public int GetMaxPoise() { return (int)m_MaxPoise; }
    public void ChangePoise(float delta)
    { 
        m_CurrentPoise = Mathf.Clamp(m_CurrentPoise + delta, 0f, m_MaxPoise);

        // Enemy is staggered
        if (m_CurrentPoise == 0f && m_CurrentHealth != 0f)
        {
            _EnemyManager._Animator.TriggerStagger();
            _EnemyManager._Behavior.ResetAttackTime();

            m_CurrentPoise = m_MaxPoise;
        }
    }

    public int GetDamage() { return (int)m_Damage; }
    public int GetPoiseDamage() { return (int)m_PoiseDamage; }
    public int GetRecentDamageTaken() { return (int)m_RecentDamageTaken; }
    public bool CanRespawn() { return !(m_Permadeath && m_HasDied); }
    public Transform GetTargetTransform() { return TargetTransform; }

    // Animator interface

    public void HandleDeath()
    {
        // Drop some loot randomly
        float lootRoll = (float)new System.Random().NextDouble();

        if (lootRoll >= 0.5f)
        {
            GameObject drop;

            // Determine what to drop for the player
            float healthScore = (float)m_PlayerAttributes.GetCurrentHealth() / m_PlayerAttributes.GetMaxHealth();
            float arrowScore = (float)m_PlayerAttributes.GetCurrentArrows() / m_PlayerAttributes.GetMaxArrows();

            if (healthScore <= arrowScore)
                drop = Instantiate(m_HeartPrefab, transform.position, transform.rotation) as GameObject;
            else
                drop = Instantiate(m_ArrowPrefab, transform.position, transform.rotation) as GameObject;

            // Place the loot on the environment
            RaycastHit hit;
            if (Physics.Raycast(drop.transform.position, -Vector3.up, out hit, m_PickupMask))
                drop.transform.position = hit.point;

            // Add item to drop pool
            DungeonZoneSystem.AddToDropPool(drop.transform);
        }

        // Spawn a poof
        ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.FALL_DUST, transform.position, transform.rotation, new Vector3(2f, 2f, 2f));

        // Play a poof sound
        AudioSystem.SpawnSound(AudioSystem.SoundType.POOF, transform.position, 1.0f);

        // Send death signal
        DungeonZoneSystem.BroadcastEnemyDeathSignal();

        m_HasDied = true;

        // Do some fun orc stuff
        if (Type == EnemyType.Orc) FinalBossBlock.ActivateLight(EventIndex);
        // Do creepy tentacle stuff
        else if (Type == EnemyType.Tentacle) VenusBossSystem.SendTentacleDeathSignal(transform.parent.GetComponent<EnemySpawner>());
        // Do some progressive Venus stuff
        else if (Type == EnemyType.Venus)
        {
            VenusBossSystem.DestroyGate();
            VenusBossSystem.SpawnEggs(false);
            DungeonZoneSystem.GetCurrentZone().KillAllEnemies();
            CutsceneSystem.PlayOutroCutscene();
        }

        _EnemyManager.EnableEnemy(false);
    }

}
