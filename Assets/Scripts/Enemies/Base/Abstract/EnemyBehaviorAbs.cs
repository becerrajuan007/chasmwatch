﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyBehaviorAbs : MonoBehaviour
{
    // Super-script
    public EnemyManager p_Manager;

    // Components
    protected NavMeshAgent p_Agent;
    protected Rigidbody p_Rigidbody;

    // Members
    protected float p_LastIdleTime;
    protected float p_LastAttackTime;
    protected Vector3 p_CurrentPatrolDestination;

    public enum ATTACK_TYPE { ONE, TWO }
    private ATTACK_TYPE m_NextAttackType = ATTACK_TYPE.ONE;
    private const float ATTACK_ONE_CHANCE = 0.8f;

    public enum AI_STATE { IDLE, PATROL, CHASE, ATTACK, DEAD }
    protected AI_STATE p_CurrentState;
    protected AI_STATE CurrentState
    {
        get { return p_CurrentState; }

        set
        {
            // Update the current state
            p_CurrentState = value;

            // Pause everything
            StopAllCoroutines();

            switch (p_CurrentState)
            {
                case AI_STATE.IDLE:
                    p_LastIdleTime = Time.time;
                    StartCoroutine(Idle());
                    break;
                case AI_STATE.PATROL:
                    StartCoroutine(Patrol());
                    break;
                case AI_STATE.CHASE:
                    StartCoroutine(Chase());
                    break;
                case AI_STATE.ATTACK:
                    StartCoroutine(Attack());
                    break;
                case AI_STATE.DEAD:
                    StartCoroutine(Dead());
                    break;
                default:
                    Debug.LogError(p_CurrentState + " has not been configured!");
                    break;
            }
        }
    }

    public virtual void Awake()
    {
        p_Manager = GetComponent<EnemyManager>();

        p_Agent = GetComponent<NavMeshAgent>();
        p_Rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void Start()
    {
        ResetAI();
    }

    protected void ResetTime()
    {
        p_LastIdleTime = Time.time;
        p_LastAttackTime = Time.time;
    }

    protected void GenerateNextAttackType(float attackOne) { m_NextAttackType = attackOne < ATTACK_ONE_CHANCE ? ATTACK_TYPE.ONE : ATTACK_TYPE.TWO; }
    protected ATTACK_TYPE GetNextAttackType() { return m_NextAttackType; }

    public void ResetAI()
    {
        ResetTime();

        CurrentState = AI_STATE.IDLE;
    }

    public void EnableHitbox(int enable) { p_Manager._Hitbox.EnableHitbox(enable); }
    public float GetCurrentForwardVelocity() { return new Vector3(p_Agent.velocity.x, 0f, p_Agent.velocity.z).magnitude / p_Agent.speed; }
    public void TriggerDeath() { CurrentState = AI_STATE.DEAD; }
    public void ResetAgent()
    {
        p_Agent.isStopped = true;
        p_Agent.velocity = Vector3.zero;
    }

    // Abs
    abstract protected IEnumerator Idle();
    abstract protected IEnumerator Patrol();
    abstract protected IEnumerator Chase();
    abstract protected IEnumerator Attack();
    protected IEnumerator Dead()
    {
        while (p_CurrentState == AI_STATE.DEAD)
        {
            p_Agent.isStopped = true;
            yield return null;
        }
    }

    abstract protected void TriggerAttack();
    abstract public void UpdateBehavior();
    abstract public void ResetAttackTime();

}
