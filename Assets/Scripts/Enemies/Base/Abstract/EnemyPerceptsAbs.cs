﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyPerceptsAbs : MonoBehaviour
{
    // Components
    protected Transform p_EyePoint;
    protected Collider p_ChaseRange;

    // Members
    protected LayerMask p_LayerMask;
    private HashSet<Collider> m_NearbyEnemies;

    protected Transform p_PlayerTransform;
    protected bool p_InSight;
    protected bool p_Aggroed;

    // Constants
    private readonly string[] c_PossibleColliders = { "Default", "Player" };

    protected virtual void Start()
    {
        ResetPercepts();
    }

    protected void Awake()
    {
        p_EyePoint = transform;
        p_ChaseRange = GetComponent<Collider>();

        p_LayerMask = LayerMask.GetMask(c_PossibleColliders);
        m_NearbyEnemies = new HashSet<Collider>();
    }

    protected void OnTriggerEnter(Collider other)
    {
        switch(other.tag)
        {
            case "Player":
                p_PlayerTransform = other.transform;
                break;
            case "Enemy":
                m_NearbyEnemies.Add(other);
                break;
            default:
                break;
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        switch (other.tag)
        {
            case "Player":
                p_PlayerTransform = null;
                p_InSight = false;
                p_Aggroed = false;
                break;
            case "Enemy":
                if (m_NearbyEnemies.Contains(other))
                    m_NearbyEnemies.Remove(other);
                break;
            default:
                break;
        }
    }

    // Abs
    abstract public void UpdatePercepts();
    abstract public Vector3 GetRandomPatrolDestination(float walkDistance);

    // Inher
    public void ResetPercepts()
    {
        p_PlayerTransform = null;
        p_InSight = false;
        p_Aggroed = false;
    }
    public Transform GetPlayerTransform() { return p_PlayerTransform; }
    public bool PlayerInRange() { return p_PlayerTransform != null; }
    public bool PlayerInSight() { return p_InSight; }
    public bool HasBeenAggroed() { return p_Aggroed; }
    public void TriggerAggro(Transform playerTransform)
    {
        // Player is in sight and enemy is aggroed
        p_InSight = true;
        p_Aggroed = true;

        // Add player transform
        p_PlayerTransform = playerTransform;
    }
    public void NotifyNearbyEnemies()
    {
        foreach (Collider enemy in m_NearbyEnemies)
            CombatEventSystem.DispatchEnemyAlertSignal(enemy, p_PlayerTransform);
    }
}
