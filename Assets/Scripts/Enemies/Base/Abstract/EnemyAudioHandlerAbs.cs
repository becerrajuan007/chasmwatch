﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAudioHandlerAbs : MonoBehaviour
{
    // Super-script
    protected EnemyManager p_Manager;

    // Components 
    protected AudioSource p_AudioSource;

    protected void Awake()
    {
        p_Manager = GetComponent<EnemyManager>();

        p_AudioSource = GetComponent<AudioSource>();
    }


}
