﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAnimatorAbs : MonoBehaviour
{
    // Super-script
    protected EnemyManager p_Manager;

    // Components
    protected Animator p_Animator;

    // Member constants
    protected readonly int p_ForwardVelocity = Animator.StringToHash("ForwardVelocity");

    protected readonly int p_IsInCombat = Animator.StringToHash("IsInCombat");

    protected readonly int p_Stagger = Animator.StringToHash("Stagger");
    protected readonly int p_Death = Animator.StringToHash("Death");
    protected void Awake()
    {
        p_Manager = GetComponent<EnemyManager>();

        p_Animator = GetComponent<Animator>();
    }

    // Inher
    public void TriggerStagger()
    {
        p_Manager._Behavior.EnableHitbox(0);
        p_Animator.SetTrigger(p_Stagger);
    }
    public void TriggerDeath()
    {
        p_Manager._Behavior.EnableHitbox(0);
        p_Animator.SetTrigger(p_Death);
    }

    // Abs
    abstract public void UpdateAnimatorData();
    abstract public bool IsAttacking();

}
