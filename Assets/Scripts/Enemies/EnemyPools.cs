﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPools : MonoBehaviour
{
    // Singleton instance
    private static EnemyPools _instance;

    public static EnemyPools Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Start

    // Fields
    [SerializeField]
    public GameObject FireballPrefab;
    [SerializeField]
    public GameObject MageballPrefab;
    [SerializeField]
    public int FireballPoolSize = 5;

    // Members
    private static Transform m_FireballPool;

    private static GameObject m_FireballPrefab;
    private static int m_FireballPoolSize;
    private static GameObject[] m_FireballArray;
    private static Queue<Transform> m_FireballQueue;

    private static GameObject m_MageballPrefab;
    private static GameObject[] m_MageballArray;
    private static Queue<Transform> m_MageballQueue;

    private void Start()
    {
        InitStaticVariables();
        InitDragonFireballs();
    }

    // Util

    private void InitStaticVariables()
    {
        m_FireballPool = transform.Find("DragonFireballs");

        m_FireballPrefab = FireballPrefab;
        m_MageballPrefab = MageballPrefab;
        m_FireballPoolSize = FireballPoolSize;
    }

    private static void InitDragonFireballs()
    {
        m_FireballArray = new GameObject[m_FireballPoolSize];
        m_FireballQueue = new Queue<Transform>();

        m_MageballArray = new GameObject[m_FireballPoolSize];
        m_MageballQueue = new Queue<Transform>();

        for (int i = 0; i < m_FireballPoolSize; i++)
        {
            // Fireballs
            m_FireballArray[i] = Instantiate(m_FireballPrefab, Vector3.zero, Quaternion.identity);
            Transform fireballObjTransform = m_FireballArray[i].GetComponent<Transform>();
            fireballObjTransform.parent = m_FireballPool;

            m_FireballQueue.Enqueue(fireballObjTransform);
            m_FireballArray[i].SetActive(false);

            // Mageballs
            m_MageballArray[i] = Instantiate(m_MageballPrefab, Vector3.zero, Quaternion.identity);
            Transform mageballObjTransform = m_MageballArray[i].GetComponent<Transform>();
            mageballObjTransform.parent = m_FireballPool;

            m_MageballQueue.Enqueue(mageballObjTransform);
            m_MageballArray[i].SetActive(false);
        }
    }

    // Interface

    public static void DestroyAllProjectiles()
    {
        foreach(GameObject fireball in m_FireballArray)
        {
            fireball.SetActive(false);
        }
    }

    public static Transform SpawnFireball(Vector3 position, Quaternion rotation)
    {
        Transform spawnedFireball = m_FireballQueue.Dequeue();

        spawnedFireball.gameObject.SetActive(true);
        spawnedFireball.position = position;
        spawnedFireball.rotation = rotation;

        m_FireballQueue.Enqueue(spawnedFireball);

        return spawnedFireball;
    }

    public static Transform SpawnMageball(Vector3 position, Quaternion rotation)
    {
        Transform spawnedMageball = m_MageballQueue.Dequeue();

        spawnedMageball.gameObject.SetActive(true);
        spawnedMageball.position = position;
        spawnedMageball.rotation = rotation;

        m_MageballQueue.Enqueue(spawnedMageball);

        return spawnedMageball;
    }
}
