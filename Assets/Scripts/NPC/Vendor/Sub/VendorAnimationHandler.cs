﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorAnimationHandler : MonoBehaviour
{
    // Components
    private Animator m_Animator;

    // Member constants
    private readonly int h_MovementMagnitude = Animator.StringToHash("MovementMagnitude");

    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
    }

    public void UpdateAnimatorData()
    {
        m_Animator.SetFloat(h_MovementMagnitude, 0f);
    }
}
