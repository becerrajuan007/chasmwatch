﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorInteractionHandler : MonoBehaviour
{
    // Super-script
    private VendorManager _VendorManager;

    // Members
    private Transform m_LastReturnedPlayerTransform;
    private Queue<string> m_CurrentConversation = null;

    private void Awake()
    {
        _VendorManager = transform.parent.GetComponent<VendorManager>();

        m_LastReturnedPlayerTransform = null;
    }

    // Interface
    public bool ReceiveConversationRequest(Transform playerTransform)
    {
        m_CurrentConversation = DialogueRetrieverService.GetVendorConversation(DialogueRetrieverService.DialogueType.VENDOR_GENERAL);

        if (m_CurrentConversation.Count > 0)
        {
            m_LastReturnedPlayerTransform = playerTransform;

            InteractionEventSystem.SendDialogueToPlayer(_VendorManager._VendorAttributes.GetName(), m_CurrentConversation.Dequeue());
            return true;
        }
        else
        {
            return false;
        }
    }

    public string GetNextConversationString()
    {
        if (m_CurrentConversation.Count > 0)
        {
            return m_CurrentConversation.Dequeue();
        }
        else
        {
            return null;
        }
    }

    public Transform GetLastReturnedPlayerTransform() { return m_LastReturnedPlayerTransform; }
}
