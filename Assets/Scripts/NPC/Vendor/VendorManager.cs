﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorManager : MonoBehaviour
{
    // Sub-scripts
    public VendorAnimationHandler _VendorAnimationHandler { get; private set; }
    public VendorInteractionHandler _VendorInteractionHandler { get; private set; }
    public NPCAttributes _VendorAttributes { get; private set; }

    // Public members
    public float InitTime { get; private set; }

    private void Awake()
    {
        _VendorAnimationHandler = GetComponentInChildren<VendorAnimationHandler>();
        _VendorAttributes = GetComponentInChildren<NPCAttributes>();
    }

    private void Update()
    {
        // Update animator data
        _VendorAnimationHandler.UpdateAnimatorData();

        InitTime = Time.time;
    }
}
