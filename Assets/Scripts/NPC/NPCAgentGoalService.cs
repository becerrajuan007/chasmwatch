﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAgentGoalService : MonoBehaviour
{
    // Singleton instance
    private static NPCAgentGoalService _instance;
    public static NPCAgentGoalService Instance { get { return _instance; } }
    private static NPCAgentGoalService s_NPCAgentGoalService;

    // Fields
    [SerializeField]
    public Transform[] VillagerGoals;

    // Members
    private static Transform[] s_VillagerGoals;

    // Constants
    private static System.Random cs_Random = new System.Random();

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

        s_VillagerGoals = VillagerGoals;
    }

    private void Start()
    {
        s_NPCAgentGoalService = FindObjectOfType<NPCAgentGoalService>();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Interface

    public static Transform GetRandomVillagerGoal()
    {
        if (s_VillagerGoals.Length == 0) return null;

        return s_VillagerGoals[cs_Random.Next(0, s_VillagerGoals.Length)];
    }

}
