﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : MonoBehaviour
{
    // Sub-scripts
    public NPCAnimationHandler _NPCAnimationHandler { get; protected set; }
    public NPCMover _NPCMover { get; protected set; }
    public NPCInteractionHandler _NPCInteractionHandler { get; protected set; }
    public NPCAttributes _NPCAttributes { get; protected set; }
    public NPCAIHandler _NPCAIHandler { get; protected set; }

    // States
    public enum InteractionState { NORMAL, CONVERSATION }

    // Member states
    public InteractionState _InteractionState { get; set; }

    // Public members
    public float InitTime { get; protected set; }

    protected void Awake()
    {
        _NPCAnimationHandler = GetComponentInChildren<NPCAnimationHandler>();
        _NPCMover = GetComponentInChildren<NPCMover>();
        _NPCInteractionHandler = GetComponentInChildren<NPCInteractionHandler>();
        _NPCAttributes = GetComponentInChildren<NPCAttributes>();
        _NPCAIHandler = GetComponentInChildren<NPCAIHandler>();

        InitTime = Time.time;
    }

    protected void Update()
    {
        // Move the NPC
        _NPCMover.MoveNPC();

        // Update AI's percepts
        _NPCAIHandler.UpdatePercepts();

        // Update animator data
        _NPCAnimationHandler.UpdateAnimatorData();
    }

}
