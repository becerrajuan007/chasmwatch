﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMover : MonoBehaviour
{
    // Critical fields
    [SerializeField]
    public Transform GroundCasterTransform;

    // Fields
    [SerializeField]
    public float MaxXZSpeed = 5.0f;
    [SerializeField]
    public float MaxYSpeed = 9.8f;
    [Range(0.01f, 1.0f)]
    public float AccelerationFactor = 0.2f;
    [SerializeField]
    public float TurnSmoothTime = 0.15f;
    [Range(0.01f, 2.0f)]
    public float GroundCastDistance = 0.5f;

    // Super-script
    protected NPCManager _NPCManager;

    // Components
    protected CharacterController m_CharacterController;

    // Members
    protected Vector3 m_DesiredVelocity;
    protected Vector3 m_Velocity;
    protected float m_TurnSmoothVelocity;
    protected LayerMask m_LayerMask;

    protected void Awake()
    {
        if (GroundCasterTransform == null) Debug.LogError("Ground caster is null! Grounds checking will not work correctly.");

        _NPCManager = transform.parent.GetComponent<NPCManager>();

        m_CharacterController = GetComponent<CharacterController>();

        m_Velocity.y = -MaxYSpeed;
        m_LayerMask = LayerMask.GetMask("Ground");
    }

    // Update

    public void MoveNPC()
    {
        if (_NPCManager._InteractionState != NPCManager.InteractionState.NORMAL)
        {
            OrientCharacter();
            ApplyGravity();

            // Finally, move the player based on modified velocity (modified from above methods)
            m_Velocity = Vector3.Lerp(m_Velocity, m_DesiredVelocity, AccelerationFactor);
            m_CharacterController.Move(m_Velocity * Time.deltaTime);
        }

        // Otherwise, let the navmesh agent take care of movement
    }

    // Util

    protected void OrientCharacter()
    {
        // TODO: AI stuff

        if (_NPCManager._InteractionState == NPCManager.InteractionState.CONVERSATION)
        {
            Transform target = _NPCManager._NPCInteractionHandler.GetLastReturnedPlayerTransform();
            Vector3 lookAt = new Vector3(target.position.x, transform.position.y, target.position.z);  // Fix Y axis to avoid weird stuff

            Vector3 direction = lookAt - transform.position;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), Time.time);
        }

        // Otherwise, let the navmesh agent take care of orientation
    }

    protected void ApplyGravity()
    {
        // TODO: This is fake gravity

        m_DesiredVelocity.y = -MaxYSpeed;
        m_Velocity.y = -MaxYSpeed;
    }

    protected bool CastToGround() { return Physics.Raycast(GroundCasterTransform.position, -Vector3.up, GroundCastDistance, m_LayerMask); }
    protected bool GetIsGrounded() { return m_CharacterController.isGrounded || CastToGround(); }

    // Public interface

    public float GetVelocityXZMagnitude() { return new Vector3(m_Velocity.x, 0f, m_Velocity.z).magnitude / MaxXZSpeed; }
    public float GetVelocityYMagnitude() { return GetIsGrounded() ? 0f : -m_Velocity.y / MaxYSpeed; }

}
