﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimationHandler : MonoBehaviour
{
    // Super-script
    protected NPCManager _NPCManager;

    // Components
    protected Animator m_Animator;

    // Member constants
    protected readonly int h_MovementMagnitude = Animator.StringToHash("MovementMagnitude");

    protected void Awake()
    {
        _NPCManager = transform.parent.GetComponent<NPCManager>();

        m_Animator = GetComponent<Animator>();
    }

    public void UpdateAnimatorData()
    {
        // Update velocity
        switch (_NPCManager._InteractionState)
        {
            case NPCManager.InteractionState.NORMAL:
                m_Animator.SetFloat(h_MovementMagnitude, _NPCManager._NPCAIHandler.GetCurrentAgentSpeed());
                break;
            case NPCManager.InteractionState.CONVERSATION:
                m_Animator.SetFloat(h_MovementMagnitude, _NPCManager._NPCMover.GetVelocityXZMagnitude());
                break;
            default:
                Debug.LogError(_NPCManager._InteractionState + " has not been configured!");
                break;
        }
    }
}
