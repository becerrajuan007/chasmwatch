﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;

public class NPCAIHandler : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float DestinationSuccessDistance = 5f;

    // Super-script
    protected NPCManager _NPCManager;

    // Components
    protected NavMeshAgent m_Agent;
    protected Transform m_CurrentPatrolDesination;

    protected void Awake()
    {
        _NPCManager = transform.parent.GetComponent<NPCManager>();

        m_Agent = GetComponent<NavMeshAgent>();
        m_CurrentPatrolDesination = transform;
    }

    protected void Start()
    {
        // Make sure that our agent has the same attributes as our mover
        m_Agent.speed = _NPCManager._NPCMover.MaxXZSpeed / 2;   // Walking speed

        // Set the initial AI state
        CurrentState = AI_STATE.PATROL;
    }

    // ----------------------------------------------------------------------------------
    // AI State Machine -----------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    public enum AI_STATE { IDLE, PATROL }

    protected AI_STATE m_CurrentState;
    protected AI_STATE CurrentState
    {
        get { return m_CurrentState; }

        set
        {
            // Update the current state
            m_CurrentState = value;

            // Stop all coroutines
            StopAllCoroutines();

            switch(m_CurrentState)
            {
                case AI_STATE.IDLE:
                    StartCoroutine(AIIDle());
                    break;
                case AI_STATE.PATROL:
                    StartCoroutine(AIPatrol());
                    break;
                default:
                    Debug.LogError(m_CurrentState + " has not been configured!");
                    break;
            }
        }
    }

    // Patrol coroutine

    protected IEnumerator AIIDle()
    {
        while(m_CurrentState == AI_STATE.IDLE)
        {
            // You don't need to do much lmao
            m_Agent.isStopped = true;

            // If we exit the conversation, go back on patrol
            if (_NPCManager._InteractionState == NPCManager.InteractionState.NORMAL)
            {
                CurrentState = AI_STATE.PATROL;
                yield break;
            }

            // Wait for the next frame
            yield return null;
        }
    }

    protected IEnumerator AIPatrol()
    {
        while(m_CurrentState == AI_STATE.PATROL)
        {
            // Go to the patrol position
            m_Agent.isStopped = false;
            m_Agent.SetDestination(m_CurrentPatrolDesination.position);

            // Wait for A* to do its magic
            while (m_Agent.pathPending)
                yield return null;

            // If we reached close to our desination, pick a new destination
            if (Vector3.Distance(transform.position, m_CurrentPatrolDesination.position) <= DestinationSuccessDistance)
                m_CurrentPatrolDesination = NPCAgentGoalService.GetRandomVillagerGoal();

            // If we are in a conversation, switch to idle
            if (_NPCManager._InteractionState == NPCManager.InteractionState.CONVERSATION)
            {
                CurrentState = AI_STATE.IDLE;
                yield break;
            }

            // Wait until the next frame
            yield return null;
        }
    }

    // Update

    public void UpdatePercepts()
    {
        // TODO: implement
    }

    // Interface
    public void SetPatrolDesination(Transform desination) { m_CurrentPatrolDesination = desination; }
    public float GetCurrentAgentSpeed() { return new Vector3(m_Agent.velocity.x, 0f, m_Agent.velocity.z).magnitude / _NPCManager._NPCMover.MaxXZSpeed; }

}
