﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAttributes : MonoBehaviour
{
    // Fields
    [SerializeField]
    public string Name = "NPC";

    // Super-script
    protected NPCManager _NPCManager;

    protected void Awake()
    {
        _NPCManager = transform.parent.GetComponent<NPCManager>();
    }

    // Interface

    public string GetName() { return Name; }
}
