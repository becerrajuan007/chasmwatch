﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInteractionHandler : MonoBehaviour
{
    // Super-script
    protected NPCManager _NPCManager;

    // Members
    protected Transform m_LastReturnedPlayerTransform;
    protected Queue<string> m_CurrentConversation = null;

    protected void Awake()
    {
        _NPCManager = transform.parent.GetComponent<NPCManager>();

        m_LastReturnedPlayerTransform = null;
    }

    // Interface
    public bool ReceiveConversationRequest(Transform playerTransform)
    {
        m_CurrentConversation = DialogueRetrieverService.GetRandomVillagerConversation();

        if (m_CurrentConversation.Count > 0)
        {
            _NPCManager._InteractionState = NPCManager.InteractionState.CONVERSATION;
            m_LastReturnedPlayerTransform = playerTransform;

            InteractionEventSystem.SendDialogueToPlayer(_NPCManager._NPCAttributes.GetName(), m_CurrentConversation.Dequeue());
            return true;
        }
        else
        {
            return false;
        }
    }

    public string GetNextConversationString()
    {
        if (m_CurrentConversation.Count > 0)
        {
            return m_CurrentConversation.Dequeue();
        }
        else
        {
            _NPCManager._InteractionState = NPCManager.InteractionState.NORMAL;

            return null;
        }
    }

    public Transform GetLastReturnedPlayerTransform() { return m_LastReturnedPlayerTransform; }
}
