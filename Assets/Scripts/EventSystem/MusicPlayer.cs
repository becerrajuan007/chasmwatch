﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    // Singleton instance
    private static MusicPlayer _instance;

    public static MusicPlayer Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

        m_MusicPlayer = transform.GetComponent<AudioSource>();
        InitAudioSources();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Fields
    [SerializeField]
    public AudioClip DungeonMusic;
    [SerializeField]
    public AudioClip IntroMusic;

    // Members
    private static AudioSource m_MusicPlayer;

    private static AudioClip m_DungeonMusic;
    private static AudioClip m_IntroMusic;

    // Enums
    public enum Track { DUNGEON, INTRO }

    // Util
    private void InitAudioSources()
    {
        m_DungeonMusic = DungeonMusic;
        m_IntroMusic = IntroMusic;
    }

    // Interface
    public static void PlayTrack(Track track)
    {
        // Determine clip to use
        AudioClip clip;
        switch(track)
        {
            case Track.DUNGEON:
                clip = m_DungeonMusic;
                break;
            case Track.INTRO:
                clip = m_IntroMusic;
                break;
            default:
                Debug.LogError("Audio track " + track + " has not been set up!");
                clip = null;
                break;
        }

        m_MusicPlayer.clip = clip;
        m_MusicPlayer.Play();
    }
}
