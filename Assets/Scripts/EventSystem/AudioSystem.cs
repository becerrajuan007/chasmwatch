﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSystem : MonoBehaviour
{
    // Singleton instance
    private static AudioSystem _instance;

    public static AudioSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    [SerializeField]
    public int PoolSize = 10;

    private UnityEngine.GameObject m_AudioSourcePrefab;
    private static AudioSource[] s_AudioSourceArr;
    private static Queue<Transform> s_AudioSourceQueue;

    // Poof clip
    [SerializeField]
    public AudioClip PoofSound;
    private static AudioClip s_PoofSound;

    // Pickup clip 
    [SerializeField]
    public AudioClip PickupSound;
    private static AudioClip s_PickupSound;

    // Switch clip
    [SerializeField]
    public AudioClip SwitchSound;
    private static AudioClip s_SwitchSound;

    // Gate clip
    [SerializeField]
    public AudioClip GateSound;
    private static AudioClip s_GateSound;

    // Door clip
    [SerializeField]
    public AudioClip DoorSound;
    private static AudioClip s_DoorSound;

    // Arrow tink
    [SerializeField]
    public AudioClip ArrowSound;
    private static AudioClip s_ArrowSound;

    [SerializeField]
    public AudioClip ShrineSound;
    private static AudioClip s_ShrineSound;


    public enum SoundType { POOF, PICKUP, SWITCH, GATE, DOOR, ARROW, SHINE }

    private void Start()
    {
        InitPool();

        InitClip(PoofSound, ref s_PoofSound);
        InitClip(PickupSound, ref s_PickupSound);
        InitClip(SwitchSound, ref s_SwitchSound);
        InitClip(GateSound, ref s_GateSound);
        InitClip(DoorSound, ref s_DoorSound);
        InitClip(ArrowSound, ref s_ArrowSound);
        InitClip(ShrineSound, ref s_ShrineSound);
    }

    private void InitPool()
    {
        m_AudioSourcePrefab = (GameObject)Resources.Load("EventSystem/AudioSystemSource");

        s_AudioSourceArr = new AudioSource[PoolSize];
        s_AudioSourceQueue = new Queue<Transform>();

        for (int i = 0; i < PoolSize; i++)
        {
            AudioSource source = Instantiate(m_AudioSourcePrefab).GetComponent<AudioSource>(); ;

            s_AudioSourceArr[i] = source;
            Transform objTransform = s_AudioSourceArr[i].GetComponent<Transform>();
            objTransform.parent = transform;

            s_AudioSourceQueue.Enqueue(objTransform);
            s_AudioSourceArr[i].gameObject.SetActive(false);
        }
    }

    private void InitClip(AudioClip publicClip, ref AudioClip staticClip) { staticClip = publicClip; }

    private static AudioClip GetAudioClip(SoundType type)
    {
        switch (type)
        {
            case SoundType.POOF:    return s_PoofSound;
            case SoundType.PICKUP:  return s_PickupSound;
            case SoundType.SWITCH:  return s_SwitchSound;
            case SoundType.GATE:    return s_GateSound;
            case SoundType.DOOR:    return s_DoorSound;
            case SoundType.ARROW:   return s_ArrowSound;
            case SoundType.SHINE:   return s_ShrineSound;

            default:
                Debug.LogError("Sound clip type " + type + " has not been set up!");
                return null;
        }
    }

    // Interface

    public static Transform SpawnSound(SoundType type, Vector3 position, float volume)
    {
        // Determine clip to play
        AudioClip clip = GetAudioClip(type);

        // Get source in queue
        Transform spawnedSource = s_AudioSourceQueue.Dequeue();

        // Spawn source
        spawnedSource.gameObject.SetActive(true);
        spawnedSource.position = position;

        // Play the audio clip at the source
        AudioSource source = spawnedSource.GetComponent<AudioSource>();
        source.Stop();
        source.PlayOneShot(clip, volume);

        // Put back into queue
        s_AudioSourceQueue.Enqueue(spawnedSource);

        // Return reference to transform
        return spawnedSource;
    }

}
