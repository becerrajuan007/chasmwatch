﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonZone : MonoBehaviour
{
    [SerializeField]
    public string Name = "__zone__";
    [SerializeField]
    public DungeonZone[] AdjacentDungeonZones;
    [SerializeField]
    public DungeonDoor[] AdjacentDungeonDoors;
    [SerializeField]
    public bool IsOutdoors = false;
    [SerializeField]
    public Transform KeyDropLocation;

    // Const
    public const float ENEMY_KEY_RATIO = 0.7f;
    private static Object KEY_PREFAB;
    private static LayerMask m_PickupMask;

    // Members
    private EnemySpawner[] m_EnemySpawners;
    private DungeonDecor m_DungeonDecor;
    private int m_EnemyKillCount;
    private bool m_KeySpawned;

    private void Awake()
    {
        if (AdjacentDungeonZones.Length == 0) Debug.LogError("Dungeon zone " + Name + " has no adjacent zones!");
        if (AdjacentDungeonDoors.Length != AdjacentDungeonZones.Length)
            Debug.LogError("Dungeon doors do not match dungeon zones!");

        m_EnemySpawners = GetComponentsInChildren<EnemySpawner>();
        m_DungeonDecor = GetComponentInChildren<DungeonDecor>();

        KEY_PREFAB = (GameObject)Resources.Load("Drops/KeyDrop");
        m_PickupMask = LayerMask.GetMask(new string[] { "Ground" });
    }

    public void InitZone()
    {
        for (int i = 0; i < AdjacentDungeonDoors.Length; i++)
        {
            AdjacentDungeonDoors[i].BindZones(this, AdjacentDungeonZones[i]);
        }

        if (m_DungeonDecor) m_DungeonDecor.SpawnDecor(false);

        m_EnemyKillCount = 0;
        m_KeySpawned = false;
    }

    public void LoadZone()
    {
        SpawnEmemies(true);
        if (m_DungeonDecor) m_DungeonDecor.SpawnDecor(true);
        WeatherSystem.EnableRain(IsOutdoors);
        m_EnemyKillCount = 0;

        if (KeyDropLocation && !m_KeySpawned)
        {
            var player = FindObjectOfType<PlayerManager>();
            player._PlayerUIHandler.PushMessage("An enemy is holding a key in this room...", false);
        }
    }

    public void UnloadZone()
    {
        SpawnEmemies(false);
        if (m_DungeonDecor) m_DungeonDecor.SpawnDecor(false);
    }

    public void SpawnEmemies(bool spawn)
    {
        if (spawn)
        {
            foreach (EnemySpawner es in m_EnemySpawners)
                es.SpawnEnemy();
        }
        else
        {
            foreach (EnemySpawner es in m_EnemySpawners)
                es.DespawnEnemy();
        }
    }

    public void KillAllEnemies()
    {
        foreach (EnemySpawner es in m_EnemySpawners)
            es.KillEnemy();
    }

    public void GetEnemyDeathSignal()
    {
        if (KeyDropLocation && !m_KeySpawned && (float)++m_EnemyKillCount / (float)m_EnemySpawners.Length >= ENEMY_KEY_RATIO)
        {
            var drop = Instantiate(KEY_PREFAB, KeyDropLocation.position, KeyDropLocation.rotation) as GameObject;
            DungeonZoneSystem.AddToPermanentDropPool(drop.transform);

            // Try to place this object on the ground
            RaycastHit hit;

            if (Physics.Raycast(drop.transform.position, -Vector3.up, out hit, m_PickupMask))
            {
                drop.transform.position = hit.point;
            }

            DungeonZoneSystem.AddToPermanentDropPool(drop.transform);

            var player = FindObjectOfType<PlayerManager>();
            player._PlayerUIHandler.PushMessage("A key has appeared in this room!", false);

            m_KeySpawned = true;
        }
    }

}
