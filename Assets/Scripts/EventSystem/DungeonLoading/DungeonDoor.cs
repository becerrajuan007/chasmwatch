﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDoor : MonoBehaviour
{
    // Fields
    [SerializeField]
    public bool IsLocked;

    // Enum
    private enum DoorState { CLOSED, ANIMATING}

    // Const
    public static float OPENING_TIME = 1.5f;

    // Components
    private BoxCollider m_Collider;
    private GameObject m_Chains;

    // Members
    private DoorState m_State;
    private float m_LastOpenTime;
    private Quaternion m_StartRotation;

    private bool m_Binded;
    private DungeonZone m_OwnedZone;
    private DungeonZone m_NextZone;

    private void Awake()
    {
        m_Collider = GetComponentInChildren<BoxCollider>();
        m_Chains = transform.Find("Chains").gameObject;
        m_Chains.SetActive(IsLocked);

        m_State = DoorState.CLOSED;
        m_LastOpenTime = 0.0f;
        m_StartRotation = transform.rotation;

        m_Binded = false;
    }

    private void Update()
    {
        if (m_State == DoorState.ANIMATING)
        {
            float progress = (Time.time - m_LastOpenTime) / OPENING_TIME;

            if (progress <= 0.5f)
            {
                // Opening
                transform.rotation = Quaternion.Euler(m_StartRotation.eulerAngles.x,
                    Mathf.Lerp(m_StartRotation.eulerAngles.y, m_StartRotation.eulerAngles.y + 90f, progress * 2),
                    m_StartRotation.eulerAngles.z);
            }

            if (progress >= 1.0f)
            {
                m_Collider.enabled = true;
                transform.rotation = m_StartRotation;
                m_State = DoorState.CLOSED;
            }
        }
    }

    // Util
    private void TransitionZone()
    {
        if (DungeonZoneSystem.GetCurrentZone() == m_OwnedZone)
            DungeonZoneSystem.TransitionZone(m_NextZone);
        else
            DungeonZoneSystem.TransitionZone(m_OwnedZone);
    }

    // Interface

    public void BindZones(DungeonZone ownedZone, DungeonZone nextZone)
    {
        if (m_Binded) return;

        m_OwnedZone = ownedZone;
        m_NextZone = nextZone;

        m_Binded = true;
    }

    public bool OpenDoor(PlayerAttributes attributes)
    {
        if (m_State == DoorState.ANIMATING) return false;

        // Check if door can be opened via key
        if (IsLocked)
        {
            if (attributes.GetCurrentKeys() > 0)
            {
                attributes.ChangeKeys(-1);
                IsLocked = false;
                m_Chains.SetActive(false);
                attributes._PlayerManager._PlayerUIHandler.PushMessage("Used key to open door", false);
            }
            else
            {
                attributes._PlayerManager._PlayerUIHandler.PushMessage("This door is locked!", true);
                return false;
            }
        }

        m_Collider.enabled = false;
        m_State = DoorState.ANIMATING;
        m_LastOpenTime = Time.time;

        TransitionZone();

        // Play sound
        AudioSystem.SpawnSound(AudioSystem.SoundType.DOOR, transform.position, 1.5f);

        CombatEventSystem.RemoveAllPlayerTargets();

        return true;
    }

    public bool InOwnedZone() { return DungeonZoneSystem.GetCurrentZone() == m_OwnedZone; }

}
