﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonDecor : MonoBehaviour
{
    // Members
    private GameObject[] m_Decor;

    private void Awake()
    {
        m_Decor = new GameObject[transform.childCount];

        int index = 0;
        foreach (Transform child in transform)
        {
            m_Decor[index++] = child.gameObject;
        }
    }

    // Interface

    public void SpawnDecor(bool spawn)
    {
        foreach (GameObject decoration in m_Decor)
        {
            decoration.SetActive(spawn);
        }
    }
}
