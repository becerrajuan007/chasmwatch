﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossBlock : MonoBehaviour
{
    // Singleton instance
    private static FinalBossBlock _instance;

    public static FinalBossBlock Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Members
    private static Light m_LeftLight;
    private static Light m_BasementLight;
    private static Light m_RoofLight;
    private static Light m_RightLight;

    private static DungeonGate m_LeftGate;
    private static DungeonGate m_BasementGate;
    private static DungeonGate m_RoofGate;
    private static DungeonGate m_RightGate;

    private static GameObject m_Block;
    private static Light m_RevealLight;

    private void Start()
    {
        Transform lights = transform.Find("Lights");
        m_LeftLight = lights.Find("LeftLight").GetComponent<Light>();
        m_BasementLight = lights.Find("BasementLight").GetComponent<Light>();
        m_RoofLight = lights.Find("RoofLight").GetComponent<Light>();
        m_RightLight = lights.Find("RightLight").GetComponent<Light>();

        Transform gates = transform.Find("Gates");
        m_LeftGate = gates.Find("LeftGate").GetComponent<DungeonGate>();
        m_BasementGate = gates.Find("BasementGate").GetComponent<DungeonGate>();
        m_RoofGate = gates.Find("RoofGate").GetComponent<DungeonGate>();
        m_RightGate = gates.Find("RightGate").GetComponent<DungeonGate>();

        m_Block = transform.Find("Block").gameObject;
        m_RevealLight = transform.Find("RevealLight").GetComponent<Light>();

        // Deactivate lights
        m_LeftLight.enabled = false;
        m_BasementLight.enabled = false;
        m_RoofLight.enabled = false;
        m_RightLight.enabled = false;

        m_RevealLight.enabled = false;
    }

    public static void ActivateLight(int index)
    {
        switch(index)
        {
            case 0:
                m_LeftLight.enabled = true;
                m_LeftGate.RaiseGate(true);
                break;
            case 1:
                m_BasementLight.enabled = true;
                m_BasementGate.RaiseGate(true);
                break;
            case 2:
                m_RoofLight.enabled = true;
                m_RoofGate.RaiseGate(true);
                break;
            case 3:
                m_RightLight.enabled = true;
                m_RightGate.RaiseGate(true);
                break;
            default:
                Debug.LogError("Tried to activate unused light!");
                break;
        }

        CheckActivateBossDoor();
    }

    public static bool ActivateAllLights()
    {
        m_LeftLight.enabled = true;
        m_BasementLight.enabled = true;
        m_RoofLight.enabled = true;
        m_RightLight.enabled = true;

        CheckActivateBossDoor();

        PauseMenu.SetSubMenuMessageText("Boss Door Opened");

        return true;
    }

    private static void CheckActivateBossDoor()
    {
        if (m_LeftLight.enabled && m_BasementLight.enabled &&
            m_RoofLight.enabled && m_RightLight.enabled)
        {
            m_RevealLight.enabled = true;
            m_Block.SetActive(false);
        }

        // Display message
        int numActivated = (m_LeftLight.enabled ? 1 : 0) + (m_BasementLight.enabled ? 1 : 0) + (m_RoofLight.enabled ? 1 : 0) + (m_RightLight.enabled ? 1 : 0);
        FindObjectOfType<PlayerManager>()._PlayerUIHandler.PushMessage(numActivated + " / 4 Orcs Defeated", false);
    }
}
