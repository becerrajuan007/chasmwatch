﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class DungeonShrine : MonoBehaviour
{
    // Fields
    [SerializeField]
    public string ShrineName = "__shrine__";
    [SerializeField]
    public DungeonZone Zone;

    // Const
    private float CIRCLE_TIME = 5f;

    // Components
    private CapsuleCollider m_ActivationArea;
    private Transform m_SpawnPoint;
    private Light m_Light;
    private ParticleSystem m_Circle;

    // Members
    private bool m_Activated;
    private float m_LastActivationTime = -1f;

    private void Awake()
    {
        m_ActivationArea = GetComponent<CapsuleCollider>();
        m_SpawnPoint = transform.Find("SpawnPoint");
        m_Light = GetComponentInChildren<Light>();
        m_Circle = GetComponentInChildren<ParticleSystem>();

        m_Activated = false;
        m_Circle.Stop();
    }

    private void Update()
    {
        if (m_Circle.isPlaying && Time.time - m_LastActivationTime >= CIRCLE_TIME)
        {
            m_Circle.Stop();
        }
    }

    // Interface

    public bool ActivateShrine()
    {
        bool firstActivation = !m_Activated;

        if (firstActivation)
        {
            m_Activated = true;
            m_Light.color = new Color(0, 1, 0);
        }

        // Play particle effect
        m_Circle.Play();

        // Play sound effect
        AudioSystem.SpawnSound(AudioSystem.SoundType.SHINE, transform.position, 1.0f);

        m_LastActivationTime = Time.time;

        return firstActivation;
    }

    public Transform GetSpawnPoint()
    {
        return m_SpawnPoint;
    }
}
