﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGateMulti : DungeonGate
{
    // Fields
    [SerializeField]
    public List<DungeonSwitch> TiedSwitches;

    // Members
    private int m_ActivatedSwitches;

    private void Start()
    {
        if (TiedSwitches.Count < 2) Debug.LogError("Multiple switches are not tied to multi-gate!");

        m_ActivatedSwitches = 0;
    }

    // Interface

    public override void RaiseGate(bool raise)
    {
        m_ActivatedSwitches += raise ? 1 : -1;

        // Record before position
        Vector3 previousPosition = transform.position;

        transform.position = p_InitialPosition + (m_ActivatedSwitches == TiedSwitches.Count ? new Vector3(0f, GATE_DIST, 0f) : Vector3.zero);

        // Play sound if gate position has changed
        if (previousPosition != transform.position)
            AudioSystem.SpawnSound(AudioSystem.SoundType.GATE, transform.position, 1.0f);
    }

}
