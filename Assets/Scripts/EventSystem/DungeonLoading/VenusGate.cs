﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusGate : MonoBehaviour
{
    // Members
    private GameObject m_Gate;

    private Transform m_Pusher;
    private float m_LastPushTime = -2f;
    private float m_InitPusherPosition;
    private float m_FinalPusherPosition;

    private void Awake()
    {
        m_Gate = transform.Find("Gate").gameObject;

        m_Pusher = transform.Find("Pusher");
        m_InitPusherPosition = m_Pusher.position.z;
        m_FinalPusherPosition = m_InitPusherPosition - 2.5f;
    }

    private void Update()
    {
        if (Time.time - m_LastPushTime <= 1f)
        {
            m_Pusher.position = Vector3.Lerp(new Vector3(m_Pusher.position.x, m_Pusher.position.y, m_InitPusherPosition),
                                             new Vector3(m_Pusher.position.x, m_Pusher.position.y, m_FinalPusherPosition),
                                             Time.time - m_LastPushTime);
        }
        else
            ResetPusher();
    }

    private void ResetPusher()
    {
        m_Pusher.position = new Vector3(m_Pusher.position.x, m_Pusher.position.y, m_InitPusherPosition);
    }

    // Interface

    public void PushPlayer()
    {
        m_LastPushTime = Time.time;
    }

    public void EnableGate(bool enable)
    {
        m_Gate.SetActive(enable);

        if (enable) ResetPusher();
    }

    public void DisableSelf()
    {
        gameObject.SetActive(false);
    }
}
