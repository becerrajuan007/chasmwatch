﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonSwitch : MonoBehaviour
{
    // Fields
    [SerializeField]
    public DungeonGate Gate;

    // Constants
    private static readonly Color DEACTIVATED_COLOR = new Color(1, 0, 0);
    private static readonly Color ACTIVATED_COLOR = new Color(0, 1, 0);
    private const int ATTACK_ID_SIZE = 3;

    // Components
    private Light m_Light;

    // Members
    private bool m_Activated;
    private Queue<int> m_AttackIDSet;

    private void Awake()
    {
        if (!Gate) Debug.LogError("Dungeon switch has no corresponding gate!");

        m_Light = GetComponentInChildren<Light>();
        m_AttackIDSet = new Queue<int>();

        m_Activated = false;
    }

    // Util
    private void PushAttackID(int attackID)
    {
        m_AttackIDSet.Enqueue(attackID);

        if (m_AttackIDSet.Count > ATTACK_ID_SIZE)
            m_AttackIDSet.Dequeue();
    }

    private bool CheckAttackID(int attackID)
    {
        return m_AttackIDSet.Contains(attackID);
    }

    // Interface

    public bool ToggleSwitch(int attackID)
    {
        if (!CheckAttackID(attackID))
        {
            PushAttackID(attackID);

            m_Activated = !m_Activated;
            Gate.RaiseGate(m_Activated);

            // Play a sound effect
            AudioSystem.SpawnSound(AudioSystem.SoundType.SWITCH, transform.position, 1.0f);

            if (m_Activated)
                m_Light.color = ACTIVATED_COLOR;
            else
                m_Light.color = DEACTIVATED_COLOR;

            // Display message
            FindObjectOfType<PlayerManager>()._PlayerUIHandler.PushMessage("Switch " + (m_Activated ? "Activated" : "Deactivated"), false);

            return true;
        }

        return false;
    }

    public void ForceActivate()
    {
        m_Activated = true;
        Gate.RaiseGate(m_Activated);
        m_Light.color = ACTIVATED_COLOR;
    }

}
