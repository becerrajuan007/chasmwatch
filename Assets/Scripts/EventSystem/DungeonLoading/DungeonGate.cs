﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGate : MonoBehaviour
{
    // Constants
    protected const float GATE_DIST = -6.0f;

    // Members
    protected Vector3 p_InitialPosition;

    private void Awake()
    {
        p_InitialPosition = transform.position;
    }

    // Interface

    public virtual void RaiseGate(bool raise)
    {
        transform.position = p_InitialPosition + (raise ? new Vector3(0f, GATE_DIST, 0f) : Vector3.zero);

        // Play a sound
        AudioSystem.SpawnSound(AudioSystem.SoundType.GATE, transform.position, 1.0f);
    }
}
