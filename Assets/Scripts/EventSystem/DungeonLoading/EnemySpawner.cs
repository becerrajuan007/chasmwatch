﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // Fields
    [SerializeField]
    public GameObject Monster = null;

    // Members

    private EnemyManager m_EnemyManager;

    private void Awake()
    {
        if (Monster == null) Debug.LogError("Monster field is null! Monster spawner will not work correctly.");
        Monster = Instantiate(Monster, transform.position, transform.rotation, transform);

        m_EnemyManager = Monster.GetComponent<EnemyManager>();

        m_EnemyManager.EnableEnemy(false);
    }

    // Interface

    public void SpawnEnemy()
    {
        m_EnemyManager.EnableEnemy(true);
    }
    public void UniqueSpawnEnemy()
    {
        m_EnemyManager.Spawn();
    }
    public void DespawnEnemy() { m_EnemyManager.EnableEnemy(false); }
    public void KillEnemy() { if (m_EnemyManager.isActiveAndEnabled) m_EnemyManager._Hurtbox.ReceiveDamageSignal(float.MaxValue, float.MaxValue, new System.Random().Next(), transform); }
    
}
