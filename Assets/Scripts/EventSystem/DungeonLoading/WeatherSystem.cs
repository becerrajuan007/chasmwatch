﻿using DigitalRuby.RainMaker;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherSystem : MonoBehaviour
{
    // Singleton instance
    private static WeatherSystem _instance;

    public static WeatherSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    private void Start()
    {
        m_RainScript = GetComponentInChildren<RainScript>();

        m_RainIntensityStart = m_RainScript.RainIntensity;
    }

    // Members

    private static BaseRainScript m_RainScript;

    private static float m_RainIntensityStart;

    // Interface

    public static void EnableRain(bool enable)
    {
        m_RainScript.RainIntensity = enable ? m_RainIntensityStart : 0.015f;
    }
}
