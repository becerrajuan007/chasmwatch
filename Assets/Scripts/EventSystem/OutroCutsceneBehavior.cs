﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OutroCutsceneBehavior : StateMachineBehaviour
{
    // Const
    private const string MAIN_MENU_PATH = "Scenes/MainMenu";

    // Members
    private int m_State;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_State = 0;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float progress = stateInfo.normalizedTime % 1;

        if (progress <= 0.2f)
        {
            if (m_State != 1)
            {
                m_State = 1;
                CutsceneSystem.SetCutsceneText("Venus has been defeated");
                CutsceneSystem.FadeScreen(false, 2f);
            }
        }
        else if (progress <= 0.3f)
        {
            if (m_State != 2)
            {
                m_State = 2;
                CutsceneSystem.SetCutsceneText("");
            }
        }
        else if (progress <= 0.5f)
        {
            if (m_State != 3)
            {
                m_State = 3;
                CutsceneSystem.SetCutsceneText("The Chasm has been purged");
            }
        }
        else if (progress <= 0.6f)
        {
            if (m_State != 4)
            {
                m_State = 4;
                CutsceneSystem.SetCutsceneText("");
                CutsceneSystem.FadeScreen(true, 8f);
            }
        }
        else if (progress <= 0.8f)
        {
            if (m_State != 5)
            {
                m_State = 5;
                CutsceneSystem.SetCutsceneText("Thank you for playing my game! - Juan");
            }
        }
        else if (progress >= 0.95f)
        {
            if (m_State != 6)
            {
                m_State = 6;
                SceneManager.LoadScene(MAIN_MENU_PATH);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
