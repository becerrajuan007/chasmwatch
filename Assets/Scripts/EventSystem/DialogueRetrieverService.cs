﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DialogueRetrieverService : MonoBehaviour
{
    // Singleton instance
    private static DialogueRetrieverService _instance;

    public static DialogueRetrieverService Instance { get { return _instance; } }
    private static DialogueRetrieverService s_DialogueRetrieverService;

    // Constants
    private const string VILLAGER_DIALOGUE_PATH = "Assets/JSON/Dialogue/Villagers.json";
    private const string VENDOR_GENERAL_DIALOGUE_PATH = "Assets/JSON/Dialogue/VendorGeneral.json";
    private static readonly System.Random cs_Random = new System.Random();
    public enum DialogueType { VILLAGER, VENDOR_GENERAL }

    // Dialogue type

    [Serializable]
    public struct Dialogue
    {
        [Serializable]
        public struct Conversation
        {
            [Serializable]
            public struct Sentence
            {
                public string sentence;
            }

            public Sentence[] Sentences;
        }

        public Conversation[] Conversations;
    }

    // Cachable types
    private static bool s_InitVillagerDialogue;
    private static Dialogue s_VillagerDialogue;

    private static bool s_InitVendorGeneralDialogue;
    private static Dialogue s_VendorGeneralDialogue;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void Start()
    {
        s_DialogueRetrieverService = FindObjectOfType<DialogueRetrieverService>();

        s_InitVillagerDialogue = false;
        s_InitVendorGeneralDialogue = false;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Util
    private static void RequestDialogueLoad(DialogueType type)
    {
        switch (type)
        {
            case DialogueType.VILLAGER:
                s_VillagerDialogue = JsonUtility.FromJson<Dialogue>(System.IO.File.ReadAllText(VILLAGER_DIALOGUE_PATH));
                s_InitVillagerDialogue = true;
                break;
            case DialogueType.VENDOR_GENERAL:
                s_VendorGeneralDialogue = JsonUtility.FromJson<Dialogue>(System.IO.File.ReadAllText(VENDOR_GENERAL_DIALOGUE_PATH));
                s_InitVendorGeneralDialogue = true;
                break;
            default:
                Debug.LogError("The dialogue type " + type + " has not been configured!");
                break;
        }
    }

    private static Queue<string> GetAsQueue(Dialogue.Conversation conversation)
    {
        Queue<string> retVal = new Queue<string>();
        for (int i = 0; i < conversation.Sentences.Length; i++)
        {
            retVal.Enqueue(conversation.Sentences[i].sentence);
        }
        return retVal;
    }

    // Interface

    public static Queue<string> GetRandomVillagerConversation()
    {
        if (!s_InitVillagerDialogue) RequestDialogueLoad(DialogueType.VILLAGER);

        Dialogue.Conversation conversation = s_VillagerDialogue.Conversations[cs_Random.Next(0, s_VillagerDialogue.Conversations.Length)];
        return GetAsQueue(conversation);
    }

    public static Queue<string> GetVendorConversation(DialogueType type)
    {
        switch(type)
        {
            case DialogueType.VENDOR_GENERAL:

                if (!s_InitVendorGeneralDialogue) RequestDialogueLoad(type);

                Dialogue.Conversation conversation = s_VendorGeneralDialogue.Conversations[cs_Random.Next(0, s_VendorGeneralDialogue.Conversations.Length)];
                return GetAsQueue(conversation);
            default:
                Debug.LogError("The dialogue type " + type + " has not been configured!");
                return null;
        }
    }

}