﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCutsceneBehavior : StateMachineBehaviour
{
    // Members
    private int m_State = 0;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_State = 0;
        DungeonZoneSystem.GetCurrentZone().SpawnEmemies(false);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float progress = stateInfo.normalizedTime % 1;       
        
        if (progress <= 0.2f)
        {
            if (m_State != 1)
            {
                m_State = 1;
                CutsceneSystem.SetCutsceneText("Juan Becerra presents...");
                CutsceneSystem.FadeScreen(false, 15f);
            }
        }
        else if (progress <= 0.3f)
        {
            if (m_State != 2)
            {
                m_State = 2;
                CutsceneSystem.SetCutsceneText("");
            }
        }
        else if (progress <= 0.5f)
        {
            if (m_State != 3)
            {
                m_State = 3;
                CutsceneSystem.SetCutsceneText("A neat lil' Unity project:");
            }
        }
        else if (progress <= 0.6f)
        {
            if (m_State != 4)
            {
                m_State = 4;
                CutsceneSystem.SetCutsceneText("");
                CutsceneSystem.FadeScreen(true, 10f);
            }
        }
        else if (progress <= 0.8f)
        {
            if (m_State != 5)
            {
                m_State = 5;
                CutsceneSystem.SetCutsceneText("Chasm");
            }
        }
        else if (progress >= 0.95f)
        {
            if (m_State != 6)
            {
                m_State = 6;
                CutsceneSystem.StartCutscene(false);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
