﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SettingsSystem : MonoBehaviour
{
    // Singleton instance
    private static SettingsSystem _instance;

    public static SettingsSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

        try
        {
            LoadSettings();
        }
        catch (FileNotFoundException e)
        {
            CreateFirstSaveSettings();
        }

        Startup();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Const
    private const string SETTINGS_PATH = "/settings.save";

    // Save file structure
    [System.Serializable]
    private class SettingsSave
    {
        // Input
        public float CameraSensitivity;
        public static readonly Vector2 CAMERA_SENSITIVITY_RANGE = new Vector2(0.1f, 10f);
        public bool Rumble;

        // Resolution
        public Vector2 Resolution;
        public static readonly Vector2[] RESOLUTIONS =
        {
            // For now, just handle 16:9 resolutions
            new Vector2(1280, 720), new Vector2(1366, 768),
            new Vector2(1600, 900), new Vector2(1920, 1080),
            new Vector2(2560, 1440), new Vector2(3840, 2160)
        };
        public bool Fullscreen;

        // Audio
        public float MasterVolume;
        public float AmbientVolume;
        public float EffectsVolume;
        public static readonly Vector2 VOLUME_RANGE = new Vector2(0f, 1f);

        // Default constructor
        public SettingsSave()
        {
            CameraSensitivity = 2.5f;
            Rumble = true;
            Resolution = RESOLUTIONS[3];
            Fullscreen = true;

            MasterVolume = 0.7f;
            AmbientVolume = 0.7f;
            EffectsVolume = 0.7f;
        }
    }

    // Save file instance
    private static SettingsSave m_Save;

    // Save flags
    private static bool m_ScreenChanged;

    // Util

    private void Startup()
    {
        // Set the resolution
        Vector2 settingsResolution = SettingsSystem.GetResolution();
        bool isFullscreen = SettingsSystem.IsFullscreen();
        Screen.SetResolution((int)settingsResolution.x, (int)settingsResolution.y, isFullscreen);
        AudioListener.volume = m_Save.MasterVolume;
    }

    private void CreateFirstSaveSettings()
    {
        Debug.Log("File not found. Creating new savefile.");

        m_Save = new SettingsSave();
        SaveSettings();
    }

    // Save interface

    public static void LoadSettings()
    {
        string jsonSave = File.ReadAllText(Application.persistentDataPath + SETTINGS_PATH);
        m_Save = JsonUtility.FromJson<SettingsSave>(jsonSave);
    }

    public static void SaveSettings()
    {
        string jsonSave = JsonUtility.ToJson(m_Save);
        File.WriteAllText(Application.persistentDataPath + SETTINGS_PATH, jsonSave);

        if (m_ScreenChanged)
        {
            m_ScreenChanged = false;

            Vector2 settingsResolution = GetResolution();
            bool isFullscreen = IsFullscreen();
            Screen.SetResolution((int)settingsResolution.x, (int)settingsResolution.y, isFullscreen);
        }
    }

    // Options interface

    // Camera
    public static float GetCameraSensitivity() => m_Save.CameraSensitivity;
    public static void SetCameraSensitivity(float sensitivity) => m_Save.CameraSensitivity = sensitivity;
    public static Vector2 GetCameraSensitivityRange() => SettingsSave.CAMERA_SENSITIVITY_RANGE;

    // Rumble
    public static bool IsRumble() => m_Save.Rumble;
    public static void SetEnableRumble(bool rumble) => m_Save.Rumble = rumble;

    // Resolution
    public static Vector2 GetResolution() => m_Save.Resolution;
    public static void SetResolution(int index) { m_Save.Resolution = SettingsSave.RESOLUTIONS[index]; m_ScreenChanged = true; }
    public static string GetResolutionAsString(Vector2 resolution) => ((int)resolution.x).ToString() + "x" + ((int)resolution.y).ToString();
    public static Vector2[] GetAllResolutions() => SettingsSave.RESOLUTIONS;

    // Fullscreen
    public static bool IsFullscreen() => m_Save.Fullscreen;
    public static void SetFullscreen(bool fullscreen) { m_Save.Fullscreen = fullscreen; m_ScreenChanged = true; }

    // Audio
    public static Vector2 GetVolumeRange() => SettingsSave.VOLUME_RANGE;
    public static float GetMasterVolume() => m_Save.MasterVolume;
    public static void SetMasterVolume(float volume) { m_Save.MasterVolume = volume; AudioListener.volume = m_Save.MasterVolume; }
    public static float GetAmbientVolume() => m_Save.AmbientVolume;
    public static void SetAmbientVolume(float volume) => m_Save.AmbientVolume = volume;
    public static float GetEffectsVolume() => m_Save.EffectsVolume;
    public static void SetEffectsVolume(float volume) => m_Save.EffectsVolume = volume;

}
