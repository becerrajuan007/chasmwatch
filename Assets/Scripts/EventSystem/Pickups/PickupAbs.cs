﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAbs : MonoBehaviour
{
    // Critical field
    [SerializeField]
    public PickupType Type = PickupType.ARROWS;

    // Enum
    public enum PickupType { ARROWS, HEART, KEY }

    // Members
    private Transform m_PickupTransform;

    private void Awake()
    {
        m_PickupTransform = transform;
    }

    private void Update()
    {
        // Slowly rotate the object
        m_PickupTransform.rotation = Quaternion.Euler(m_PickupTransform.rotation.eulerAngles +
            new Vector3(0f, 90f, 0f) * Time.deltaTime);

        // Bob up and down
        m_PickupTransform.localPosition = m_PickupTransform.localPosition +
            (new Vector3(0f, Mathf.Sin(Time.time) * 0.2f, 0f) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check if this is the player
        if (other.tag.Equals("Player"))
        {
            InteractionEventSystem.SendPickupSignal(Type);
            ParticlesSystem.SpawnParticle(ParticlesSystem.ParticleType.PICKUP_GRAB, transform.position, transform.rotation, new Vector3(1f, 1f, 1f));
            AudioSystem.SpawnSound(AudioSystem.SoundType.PICKUP, transform.position, 0.8f);
            Destroy(gameObject);
        }
    }
}
