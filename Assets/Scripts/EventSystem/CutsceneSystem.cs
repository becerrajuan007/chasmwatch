﻿using DigitalRuby.RainMaker;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneSystem : MonoBehaviour
{
    // Singleton instance
    private static CutsceneSystem _instance;

    public static CutsceneSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

        Init();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Fields
    [SerializeField]
    public bool StartIntroCutscene = false;

    // Const 
    private static readonly Color BLACK_COLOR = new Color(0f, 0f, 0f, 1f);
    private static readonly Color CLEAR_COLOR = new Color(0f, 0f, 0f, 0f);

    private static readonly int h_StartIntro = Animator.StringToHash("StartIntro");
    private static readonly int h_StartOutro = Animator.StringToHash("StartOutro");

    // Members
    private static Camera m_CutsceneCamera;
    private static Canvas m_Canvas;
    private static Text m_Text;
    private static Image m_Fade;

    private static PlayerManager m_PlayerManager;
    private static BaseRainScript m_RainScript;
    private static Animator m_Animator;

    private static bool m_ShouldFade;
    private static float m_LastFadeTime = 0f;
    private static float m_CurrentFadeTime = 0f;

    private void Init()
    {
        m_CutsceneCamera = transform.GetComponentInChildren<Camera>();
        m_Canvas = transform.GetComponentInChildren<Canvas>();
        m_Text = m_Canvas.transform.GetComponentInChildren<Text>();
        m_Fade = m_Canvas.transform.GetComponentInChildren<Image>();

        m_PlayerManager = FindObjectOfType<PlayerManager>();
        m_RainScript = FindObjectOfType<BaseRainScript>();
        m_Animator = m_CutsceneCamera.gameObject.GetComponent<Animator>();

        m_Text.text = "";
        m_Fade.enabled = false;
    }

    private void Start()
    {
        if (StartIntroCutscene)
            PlayIntroCutscene();
        else
            StartCutscene(false);
    }

    // Update
    private void Update()
    {
        // Should be animating
        float delta = (Time.time - m_LastFadeTime) / m_CurrentFadeTime;
        if (float.IsNaN(delta)) return;

        if (delta <= m_CurrentFadeTime)
        {
            m_Fade.color = Color.Lerp(m_ShouldFade ? CLEAR_COLOR : BLACK_COLOR, m_ShouldFade ? BLACK_COLOR : CLEAR_COLOR, delta);
        }
    }

    // Util

    public static void StartCutscene(bool start)
    {
        m_PlayerManager.gameObject.SetActive(!start);
        m_CutsceneCamera.gameObject.SetActive(start);

        if (start)
        {
            m_RainScript.Camera = m_CutsceneCamera;
        }
        else
        {
            m_RainScript.Camera = m_PlayerManager._PlayerCameraMover.GetPlayerCamera();

            if (DungeonZoneSystem.GetCurrentZone())
                DungeonZoneSystem.GetCurrentZone().SpawnEmemies(true);

            MusicPlayer.PlayTrack(MusicPlayer.Track.DUNGEON);
            m_Text.text = "";
            m_Fade.enabled = false;

            FindObjectOfType<PlayerTargettingSystem>().InitTargetingMesh();
        }
    }

    // Interface

    public static void PlayIntroCutscene()
    {
        StartCutscene(true);

        m_Animator.SetTrigger(h_StartIntro);
        MusicPlayer.PlayTrack(MusicPlayer.Track.INTRO);
    }

    public static void PlayOutroCutscene()
    {
        StartCutscene(true);

        m_Animator.SetTrigger(h_StartOutro);
        MusicPlayer.PlayTrack(MusicPlayer.Track.INTRO);
    }

    public static void SetCutsceneText(string text) => m_Text.text = text;

    public static void FadeScreen(bool fadeInto, float time)
    {
        m_ShouldFade = fadeInto;
        m_LastFadeTime = Time.time;
        m_CurrentFadeTime = time;

        m_Fade.enabled = true;
        m_Fade.color = fadeInto ? CLEAR_COLOR : BLACK_COLOR;
    }
}
