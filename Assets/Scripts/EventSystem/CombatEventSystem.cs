﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatEventSystem : MonoBehaviour
{
    // Singleton instance
    private static CombatEventSystem _instance;

    public static CombatEventSystem Instance {  get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    private void Start()
    {
        s_TargettingSystem = FindObjectOfType<PlayerTargettingSystem>();
    }

    // Members
    private static PlayerTargettingSystem s_TargettingSystem;

    // Util
    private static PlayerCollisionHandler GetPlayerCollisionHandler(Collider collider) { return collider.gameObject.GetComponent<PlayerCollisionHandler>(); }
    private static EnemyHurtbox GetEnemyCollisionHandler(Collider collider) { return collider.gameObject.GetComponent<EnemyHurtbox>(); }
    private static EnemyPerceptsAbs GetEnemyPercepts(Collider collider) { return collider.gameObject.GetComponent<EnemyManager>()._Percepts; }
    private static DungeonSwitch GetDungeonSwitch(Collider collider) { return collider.gameObject.GetComponent<DungeonSwitch>(); }

    // Interface

    public static bool SendAttackSignal(Collider target, int attackID, float damage, float poise, Transform source)
    {
        switch(target.tag)
        {
            case "Player":
                return GetPlayerCollisionHandler(target).ReceiveDamageSignal(damage, poise, attackID, source);
            case "Enemy":
                return GetEnemyCollisionHandler(target).ReceiveDamageSignal(damage, poise, attackID, source);
            case "DungeonSwitch":
                return GetDungeonSwitch(target).ToggleSwitch(attackID);
            default:
                // Hit some other collider, ignore signal
                return false;
        }
    }

    public static bool SendForceSignal(Collider target, float force, Transform source)
    {
        switch(target.tag)
        {
            case "Enemy":
                DispatchEnemyAlertSignal(target, source);
                return GetEnemyCollisionHandler(target).ReceiveForceSignal((source.position - target.transform.position).normalized * force);
            default:
                return false;
        }
    }

    public static void DispatchEnemyDeathSignal(Collider deadTarget)
    {
        if (!s_TargettingSystem) s_TargettingSystem = FindObjectOfType<PlayerTargettingSystem>();

        // Remove this enemy from targetting system, attempt to target new enemy
        s_TargettingSystem.RemoveCollider(deadTarget);

        if (s_TargettingSystem._PlayerManager._FocusState == PlayerManager.FocusState.LOCKEDON)
            s_TargettingSystem._PlayerManager._PlayerCameraMover.TargetClosestEnemy();
    }

    public static void DispatchEnemyAlertSignal(Collider target, Transform playerTransform)
    {
        switch(target.tag)
        {
            case "Enemy":
                GetEnemyPercepts(target).TriggerAggro(playerTransform);
                break;
            default:
                // Tried to alert some other collider, ignore signal
                break;
        }
    }

    public static void RemovePlayerTarget(Collider enemy)
    {
        if (s_TargettingSystem)
            s_TargettingSystem.RemoveCollider(enemy);
    }

    public static void RemoveAllPlayerTargets()
    {
        if (s_TargettingSystem)
            s_TargettingSystem.RemoveAllColliders();
    }

}
