﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesSystem : MonoBehaviour
{
    // Singleton instance
    private static ParticlesSystem _instance;

    public static ParticlesSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Fields

    [SerializeField]
    public int LargePoolSize = 10;
    [SerializeField]
    public int SmallPoolSize = 4;

    // Hit spark
    [SerializeField]
    public GameObject HitSpark;
    private static GameObject s_HitSparkPool;
    private static GameObject[] s_HitSparkArr;
    private static Queue<Transform> s_HitSparkQueue;

    // StepDust
    [SerializeField]
    public GameObject StepDust;
    private static GameObject s_StepDustPool;
    private static GameObject[] s_StepDustArr;
    private static Queue<Transform> s_StepDustQueue;

    // DodgeDust
    [SerializeField]
    public GameObject DodgeDust;
    private static GameObject s_DodgeDustPool;
    private static GameObject[] s_DodgeDustArr;
    private static Queue<Transform> s_DodgeDustQueue;

    // FallDust
    [SerializeField]
    public GameObject FallDust;
    private static GameObject s_FallDustPool;
    private static GameObject[] s_FallDustArr;
    private static Queue<Transform> s_FallDustQueue;

    // PickupGrab
    [SerializeField]
    public GameObject PickupGrab;
    private static GameObject s_PickupGrabPool;
    private static GameObject[] s_PickupGrabArr;
    private static Queue<Transform> s_PickupGrabQueue;

    // ArrowFire
    [SerializeField]
    public GameObject ArrowFire;
    private static GameObject s_ArrowFirePool;
    private static GameObject[] s_ArrowFireArr;
    private static Queue<Transform> s_ArrowFireQueue;

    // Enumerated particles type
    public enum ParticleType { HIT_SPARK, STEP_DUST, DODGE_DUST, FALL_DUST, PICKUP_GRAB, ARROW_FIRE }

    private void Start()
    {
        InitPool(ref HitSpark, ref s_HitSparkPool, ref s_HitSparkArr, ref s_HitSparkQueue, LargePoolSize);
        InitPool(ref StepDust, ref s_StepDustPool, ref s_StepDustArr, ref s_StepDustQueue, SmallPoolSize);
        InitPool(ref DodgeDust, ref s_DodgeDustPool, ref s_DodgeDustArr, ref s_DodgeDustQueue, SmallPoolSize);
        InitPool(ref FallDust, ref s_FallDustPool, ref s_FallDustArr, ref s_FallDustQueue, SmallPoolSize);
        InitPool(ref PickupGrab, ref s_PickupGrabPool, ref s_PickupGrabArr, ref s_PickupGrabQueue, SmallPoolSize);
        InitPool(ref ArrowFire, ref s_ArrowFirePool, ref s_ArrowFireArr, ref s_ArrowFireQueue, SmallPoolSize);
    }

    private void InitPool(ref GameObject prefab, ref GameObject poolTransform, ref GameObject[] objectArray, ref Queue<Transform> objectQueue, int poolSize)
    {
        poolTransform = new GameObject();
        poolTransform.transform.parent = transform;

        objectArray = new GameObject[poolSize];
        objectQueue = new Queue<Transform>();

        for (int i = 0; i < poolSize; i++)
        {
            objectArray[i] = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            Transform objTransform = objectArray[i].GetComponent<Transform>();
            objTransform.parent = poolTransform.transform;

            objectQueue.Enqueue(objTransform);
            objectArray[i].SetActive(false);
        }
    }

    // Interface

    public static Transform SpawnParticle(ParticleType type, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        Queue<Transform> poolQueue;

        switch (type)
        {
            case ParticleType.HIT_SPARK:
                poolQueue = s_HitSparkQueue;
                break;
            case ParticleType.STEP_DUST:
                poolQueue = s_StepDustQueue;
                break;
            case ParticleType.DODGE_DUST:
                poolQueue = s_DodgeDustQueue;
                break;
            case ParticleType.FALL_DUST:
                poolQueue = s_FallDustQueue;
                break;
            case ParticleType.PICKUP_GRAB:
                poolQueue = s_PickupGrabQueue;
                break;
            case ParticleType.ARROW_FIRE:
                poolQueue = s_ArrowFireQueue;
                break;
            default:
                Debug.LogError("Particle type " + type.ToString() + " has not been set up!");
                poolQueue = new Queue<Transform>();
                break;
        }

        Transform spawnedParticle = poolQueue.Dequeue();

        spawnedParticle.gameObject.SetActive(true);

        spawnedParticle.position = position;
        spawnedParticle.rotation = rotation;
        spawnedParticle.localScale = scale;

        spawnedParticle.GetComponent<ParticleSystem>().Play();

        poolQueue.Enqueue(spawnedParticle);

        return spawnedParticle;
    }
}
