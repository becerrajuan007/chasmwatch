﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XInputDotNetPure;

public class XInputSystem : MonoBehaviour
{
    // Singleton instance
    private static XInputSystem _instance;

    public static XInputSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    private class VibeJob : System.IComparable
    {
        public float EndTime { get; private set; }
        public float LeftIntensity { get; private set; }
        public float RightIntensity { get; private set; }

        public VibeJob(float duration, float leftIntensity, float rightIntensity)
        {
            EndTime = Time.time + duration;
            LeftIntensity = leftIntensity;
            RightIntensity = rightIntensity;
        }

        public bool IsFinished() => Time.time - EndTime > 0f;

        public int CompareTo(object obj)
        {
            VibeJob otherJob = (VibeJob)obj;

            if (EndTime < otherJob.EndTime)
                return 1;
            else if (EndTime > otherJob.EndTime)
                return -1;
            else
                return 0;
        }
    }

    // Members
    private static SortedSet<VibeJob> m_VibeJobs;

    private void Start()
    {
        m_VibeJobs = new SortedSet<VibeJob>();
    }

    private void FixedUpdate()
    {
        if (!SettingsSystem.IsRumble())
        {
            m_VibeJobs.Clear();
            return;
        }

        float leftIntensity = 0f;
        float rightIntensity = 0f;

        for (int i = 0; i < m_VibeJobs.Count; i++)
        {
            // NOTE: This can get costly in large sets.
            VibeJob job = m_VibeJobs.ElementAt(i);

            // Is this job finished?
            if (job.IsFinished())
            {
                m_VibeJobs.Remove(job);
                i--;
            }
            else
            {
                // Check if we found a new greatest intensity
                if (job.LeftIntensity > leftIntensity) leftIntensity = job.LeftIntensity;
                if (job.RightIntensity > rightIntensity) rightIntensity = job.RightIntensity;
            }
        }

        // Send intensity to current controller
        // NOTE: May need to manually look up controller
        GamePad.SetVibration(0, leftIntensity, rightIntensity);
    }

    // Interface

    public static void AddVibration(float duration, float leftIntensity, float rightIntensity)
    {
        m_VibeJobs.Add(new VibeJob(duration, leftIntensity, rightIntensity));
    }

}
