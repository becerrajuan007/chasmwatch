﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonZoneSystem : MonoBehaviour
{
    // Singleton instance
    private static DungeonZoneSystem _instance;

    public static DungeonZoneSystem Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;

        Init();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Start

    // Fields
    [SerializeField]
    public DungeonZone StartZone;

    // Members
    private static DungeonZone m_CurrentZone;
    private static Transform m_DropPool;
    private static Transform m_PermanentDropPool;

    private void Init()
    {
        m_DropPool = transform.Find("DropPool");
        m_PermanentDropPool = transform.Find("PermanentDropPool");
    }

    private void Start()
    {
        if (StartZone == null)
            Debug.LogError("Starting dungeon zone is null! Dungeon Zone System will not work correctly.");
        else
        {
            DungeonZoneBFSInit(StartZone);

            m_CurrentZone = StartZone;
            m_CurrentZone.LoadZone();
        }
    }

    // Util
    
    private void DungeonZoneBFSInit(DungeonZone startZone)
    {
        HashSet<DungeonZone> visitedZones = new HashSet<DungeonZone>();
        Queue<DungeonZone> zoneInitQueue = new Queue<DungeonZone>();
        zoneInitQueue.Enqueue(startZone);

        while(zoneInitQueue.Count > 0)
        {
            DungeonZone zone = zoneInitQueue.Dequeue();
            

            foreach (DungeonZone z in zone.AdjacentDungeonZones)
            {
                if (!visitedZones.Contains(z))
                    zoneInitQueue.Enqueue(z);
            }

            visitedZones.Add(zone);

            zone.InitZone();
        }
    }

    private static void ClearDropPool()
    {
        foreach (Transform drop in m_DropPool)
        {
            Destroy(drop.gameObject);
        }
    }

    // Interface

    public static DungeonZone GetCurrentZone() { return m_CurrentZone; }

    public static void TransitionZone(DungeonZone newZone)
    {
        if (m_CurrentZone)
            m_CurrentZone.UnloadZone();

        m_CurrentZone = newZone;
        m_CurrentZone.LoadZone();

        ClearDropPool();
        EnemyPools.DestroyAllProjectiles();
    }

    public static void AddToDropPool(Transform drop)
    {
        drop.parent = m_DropPool;
    }

    public static void AddToPermanentDropPool(Transform drop)
    {
        drop.parent = m_PermanentDropPool;
    }

    public static void BroadcastEnemyDeathSignal() => m_CurrentZone.GetEnemyDeathSignal();

    public static bool ClearRoomEnemies()
    {
        m_CurrentZone.KillAllEnemies();

        PauseMenu.SetSubMenuMessageText("Cleared Room of Enemies");

        return true;
    }

    public static bool OpenAllDungeonGates()
    {
        // Perform BFS on all zones
        HashSet<DungeonZone> visitedZones = new HashSet<DungeonZone>();
        Queue<DungeonZone> zoneInitQueue = new Queue<DungeonZone>();
        zoneInitQueue.Enqueue(m_CurrentZone);

        while (zoneInitQueue.Count > 0)
        {
            // Enqueue adjacent zones
            DungeonZone zone = zoneInitQueue.Dequeue();
            foreach (DungeonZone z in zone.AdjacentDungeonZones)
            {
                if (!visitedZones.Contains(z))
                    zoneInitQueue.Enqueue(z);
            }

            // Search for dungeon switch in zone, then active them
            foreach (DungeonSwitch dungeonSwitch in zone.gameObject.GetComponentsInChildren<DungeonSwitch>())
                dungeonSwitch.ForceActivate();

            // Seek out and open any remaining doors
            foreach (DungeonGate dungeonGate in zone.gameObject.GetComponentsInChildren<DungeonGate>())
                dungeonGate.RaiseGate(true);

            // Seek out and open any remaining multis
            foreach (DungeonGateMulti dungeonGateMulti in zone.gameObject.GetComponentsInChildren<DungeonGateMulti>())
                dungeonGateMulti.RaiseGate(true);

            // Mark as visited
            visitedZones.Add(zone);
        }

        PauseMenu.SetSubMenuMessageText("Opened All Dungeon Gates");

        return true;
    }

    public static bool UnlockAllShrines()
    {
        // Perform BFS on all zones
        HashSet<DungeonZone> visitedZones = new HashSet<DungeonZone>();
        Queue<DungeonZone> zoneInitQueue = new Queue<DungeonZone>();
        zoneInitQueue.Enqueue(m_CurrentZone);

        while (zoneInitQueue.Count > 0)
        {
            // Enqueue adjacent zones
            DungeonZone zone = zoneInitQueue.Dequeue();
            foreach (DungeonZone z in zone.AdjacentDungeonZones)
            {
                if (!visitedZones.Contains(z))
                    zoneInitQueue.Enqueue(z);
            }

            // Get reference to last visited shrine
            PlayerAttributes attributes = FindObjectsOfType<PlayerManager>()[0]._PlayerAttributes;
            DungeonShrine lastVisitedShrine = attributes.GetLastVisitedShrine(); ;

            // Look for a shrine, then add it to player's visited shrines
            foreach (DungeonShrine shrine in zone.gameObject.GetComponentsInChildren<DungeonShrine>())
            {
                shrine.ActivateShrine();
                attributes.AddToVisitedDungeonShrines(shrine);
            }

            // Set original shrine as current
            attributes.SetAsCurrentDungeonShrine(lastVisitedShrine);

            // Mark as visited
            visitedZones.Add(zone);
        }

        PauseMenu.SetSubMenuMessageText("Unlocked All Shrines");

        return true;
    }

}
