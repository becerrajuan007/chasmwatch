﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionEventSystem : MonoBehaviour
{
    // Singleton instance
    private static InteractionEventSystem _instance;

    public static InteractionEventSystem Instance { get { return _instance; } }

    // Members
    private static PlayerInteractionSystem m_PlayerInteractionSystem;

    private void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
            _instance = this;
    }

    private void Start()
    {
        m_PlayerInteractionSystem = FindObjectOfType<PlayerInteractionSystem>();
    }

    private void OnDestroy()
    {
        if (this == _instance)
            _instance = null;
    }

    // Util

    private static NPCInteractionHandler GetNPCInteractionHandler(Collider collider) { return collider.gameObject.GetComponent<NPCInteractionHandler>(); }
    private static VendorInteractionHandler GetVendorInteractionHandler(Collider collider) { return collider.gameObject.GetComponent<VendorInteractionHandler>(); }
    private static DungeonDoor GetDungeonDoorHandler(Collider doorCollider) { return doorCollider.transform.parent.GetComponent<DungeonDoor>(); }
    private static DungeonShrine GetDungeonShrineHandler(Collider shrineCollider) { return shrineCollider.gameObject.GetComponent<DungeonShrine>(); }

    // CONVERSATIONS
    public static bool SendConversationSignal(Collider target, Transform playerTransform)
    {
        switch(target.tag)
        {
            case "NPC":
                var NPC = GetNPCInteractionHandler(target);
                return NPC.ReceiveConversationRequest(playerTransform);
            case "Vendor":
                var Vendor = GetVendorInteractionHandler(target);
                return Vendor.ReceiveConversationRequest(playerTransform);
            default:
                // Tried to strike a friendly conversation with a brick wall
                // The wild gamer did not listen
                return false;
        }
    }

    public static string RequestNextDialogueMessage(Collider target)
    {
        switch (target.tag)
        {
            case "NPC":
                var NPC = GetNPCInteractionHandler(target);
                return NPC.GetNextConversationString();
            case "Vendor":
                var Vendor = GetVendorInteractionHandler(target);
                return Vendor.GetNextConversationString();
        }

        return null;
    }
    public static void SendDialogueToPlayer(string npcName, string message) { m_PlayerInteractionSystem.ReceiveDialogue(npcName, message); }

    // DOOR OPENING

    // Members
    private static Transform s_LastDoorTransform;
    private static bool s_LastDoorFromOwnedArea;

    public static bool SendOpenDoorSignal(Collider target, PlayerAttributes attributes)
    {
        if (target.tag.Equals("Door"))
        {
            var door = GetDungeonDoorHandler(target);
            s_LastDoorTransform = door.transform;
            s_LastDoorFromOwnedArea = door.InOwnedZone();
            return door.OpenDoor(attributes);
        }

        return false;
    }

    public static bool SendActivateShrineSignal(Collider target)
    {
        if (target.tag.Equals("Shrine"))
        {
            var shrine = GetDungeonShrineHandler(target);
            return shrine.ActivateShrine();
        }

        return false;
    }

    public static void SendPickupSignal(PickupAbs.PickupType type)
    {
        if (!m_PlayerInteractionSystem) m_PlayerInteractionSystem = FindObjectOfType<PlayerInteractionSystem>();

        m_PlayerInteractionSystem.ReceivePickupSignal(type);
    }

    public static Transform GetLastDoorTransform() { return s_LastDoorTransform; }
    public static bool GetLastDoorFromOwnedArea() { return s_LastDoorFromOwnedArea; }
}
