﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCamera : MonoBehaviour
{
    private Transform m_CameraTransform;

    private void Awake()
    {
        m_CameraTransform = transform;
    }

    private void Update()
    {
        // Slowly rotate and bob up and down
        m_CameraTransform.rotation = Quaternion.Euler(m_CameraTransform.eulerAngles + 
            (new Vector3(Mathf.Sin(Time.time) * 5f, 10f, 0f) * Time.deltaTime));
    }
}
