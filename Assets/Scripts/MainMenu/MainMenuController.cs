﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    // Constants
    private const string DEMO_SCENE_PATH = "Scenes/ForestTempleDemo";

    private const string LeftStickHorz = "LeftStickHorz";
    private const string LeftStickVert = "LeftStickVert";

    private const string ControlPadHorz = "ControlPadHorz";
    private const string ControlPadVert = "ControlPadVert";

    private const string InteractButton = "InteractButton";
    private const string BackButton = "DodgeButton";

    private const float SELECTOR_OFFSET = -12f;

    // Members
    private List<Vector2> m_MenuButtons;
    private int m_MenuIndex;

    private RectTransform m_SelectorTransform;
    private GameObject m_LoadingScreen;

    private SubMenuAbs m_OptionsSubMenu;
    private SubMenuAbs m_ActiveSubMenu = null;

    private void Awake()
    {
        // Init selector object
        m_SelectorTransform = transform.Find("Selector").GetComponent<RectTransform>();
        m_LoadingScreen = transform.Find("LoadingScreen").gameObject;

        m_LoadingScreen.SetActive(false);

        // Init list of objects
        m_MenuButtons = new List<Vector2>();

        var menuObjects = transform.Find("MenuButtons");
        foreach (RectTransform rt in menuObjects.GetComponentsInChildren<RectTransform>())
        {
            if (rt != menuObjects.GetComponent<RectTransform>())
                m_MenuButtons.Add(rt.anchoredPosition);
        }

        InitOptionsMenu();
    }

    private void InitOptionsMenu()
    {
        GameObject subMenuPrefab = (GameObject)Resources.Load("Menus/SubMenu");
        m_OptionsSubMenu = AllocateSubMenu(subMenuPrefab);
        m_OptionsSubMenu.InitSubMenu("Options");
        m_OptionsSubMenu.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);

        m_OptionsSubMenu.AddSliderOption("Resolution", SettingsSystem.SetResolution, SettingsSystem.GetResolution,
            SettingsSystem.GetResolutionAsString, GetResolutionStrings(SettingsSystem.GetAllResolutions()));
        m_OptionsSubMenu.AddCheckboxOption("Fullscreen", SettingsSystem.SetFullscreen, SettingsSystem.IsFullscreen);
        // Input
        m_OptionsSubMenu.AddSliderOption("Camera Sensitivity", SettingsSystem.SetCameraSensitivity, SettingsSystem.GetCameraSensitivity,
            SettingsSystem.GetCameraSensitivityRange());
        m_OptionsSubMenu.AddCheckboxOption("Rumble", SettingsSystem.SetEnableRumble, SettingsSystem.IsRumble);
        // Audio
        m_OptionsSubMenu.AddSliderOption("Master Volume", SettingsSystem.SetMasterVolume, SettingsSystem.GetMasterVolume,
            SettingsSystem.GetVolumeRange());
    }

    private SubMenuAbs AllocateSubMenu(GameObject prefab)
    {
        var debugInstance = Instantiate(prefab) as GameObject;
        debugInstance.transform.SetParent(transform);
        return debugInstance.GetComponent<SubMenuAbs>();
    }

    private string[] GetResolutionStrings(Vector2[] resolutions)
    {
        string[] strings = new string[resolutions.Length];

        for (int i = 0; i < resolutions.Length; i++)
        {
            strings[i] = SettingsSystem.GetResolutionAsString(resolutions[i]);
        }

        return strings;
    }

    private void Start()
    {
        // Set selector position to first item in list
        m_MenuIndex = 0;
        m_SelectorTransform.anchoredPosition = m_MenuButtons[m_MenuIndex] + new Vector2(0f, SELECTOR_OFFSET);
    }

    private void Update()
    {
        // Listen for inputs
        Vector2 stickInput = new Vector2(Input.GetAxis(LeftStickHorz), -Input.GetAxis(LeftStickVert));
        Vector2 controlInput = new Vector2(Input.GetAxis(ControlPadHorz), Input.GetAxis(ControlPadVert));

        if (stickInput != Vector2.zero)
            ToggleMenuItem(stickInput);
        else
            ToggleMenuItem(controlInput);

        // Listen for interact button
        SelectButton(Input.GetAxis(InteractButton) > 0);

        // Listen for backtrack button
        BacktrackButton(Input.GetAxis(BackButton) > 0);
    }

    private float m_LastToggleTime = -1f;
    private bool m_ToggleHeld = false;

    private void ToggleMenuItem(Vector2 input)
    {
        if (Mathf.Abs(input.y) > 0.5f)
        {
            if (!m_ToggleHeld || Time.time - m_LastToggleTime > 0.4f)
            {
                bool down = input.y < 0;

                if (m_ActiveSubMenu)
                {
                    m_ActiveSubMenu.ToggleMenuSelection(down);
                }
                else
                {
                    if (down)
                    {
                        m_MenuIndex++;
                        if (m_MenuIndex > m_MenuButtons.Count - 1) m_MenuIndex = 0;
                    }
                    else
                    {
                        m_MenuIndex--;
                        if (m_MenuIndex < 0) m_MenuIndex = m_MenuButtons.Count - 1;
                    }
                }

                // Update selector position
                m_SelectorTransform.anchoredPosition = m_MenuButtons[m_MenuIndex] + new Vector2(0f, SELECTOR_OFFSET);

                m_LastToggleTime = Time.time;
            }
        }

        if (Mathf.Abs(input.x) > 0.5f)
        {
            if (!m_ToggleHeld || Time.time - m_LastToggleTime > 0.4f)
            {
                bool left = input.x < 0;

                if (m_ActiveSubMenu)
                {
                    m_ActiveSubMenu.ToggleMenuSelectionSide(left);
                }

                m_LastToggleTime = Time.time;
            }
        }

        m_ToggleHeld = input != Vector2.zero;
    }

    private bool selectPressed = false;
    private void SelectButton(bool isHeld)
    {
        if (isHeld && !selectPressed)
        {
            selectPressed = true;

            if (m_ActiveSubMenu)
            {
                m_ActiveSubMenu.SelectMenuItem();
            }
            else
            {
                switch (m_MenuIndex)
                {
                    case 0:
                        // Start Dungeon Demo
                        m_LoadingScreen.SetActive(true);
                        SceneManager.LoadScene(DEMO_SCENE_PATH);
                        break;
                    case 1:
                        // Options
                        m_ActiveSubMenu = m_OptionsSubMenu;
                        m_OptionsSubMenu.ShowSubMenu(true);
                        break;
                    case 2:
                        // Quit
                        Application.Quit();
                        break;
                }
            }
        }
        else if (!isHeld)
        {
            selectPressed = false;
        }
    }

    private bool backtrackPressed = false;
    private void BacktrackButton(bool isHeld)
    {
        if (isHeld && !backtrackPressed)
        {
            backtrackPressed = true;

            if (m_ActiveSubMenu)
            {
                if (m_ActiveSubMenu == m_OptionsSubMenu)
                    SettingsSystem.SaveSettings();

                m_ActiveSubMenu.ShowSubMenu(false);
                m_ActiveSubMenu = null;
            }
        }
        else if (!isHeld)
        {
            backtrackPressed = false;
        }
    }

}
