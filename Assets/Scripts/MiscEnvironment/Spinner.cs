﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    // Fields
    [SerializeField]
    public float SpinRate = 90f;

    // Members
    private float m_SpinAmount;

    private void Awake()
    {
        m_SpinAmount = 0f;
    }

    private void Update()
    {
        m_SpinAmount -= SpinRate * Time.deltaTime;
        m_SpinAmount %= 360f;

        transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, m_SpinAmount);
    }
}
