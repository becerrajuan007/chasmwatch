﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuSliderDiscrete : SubMenuOption
{
    // Components
    private Slider m_Slider;
    private Text m_SliderSelection;

    // Members
    private string[] m_OptionValues; 

    // Function delegates
    private Delegate m_Setter;
    private Delegate m_Getter;

    public void InitSubMenuSliderDiscrete<T>(Action<int> optionSetter, Func<T> optionGetter, Func<T, string> selectionGetter, string[] optionValues)
    {
        m_Slider = GetComponentInChildren<Slider>();
        m_SliderSelection = GetComponentsInChildren<Text>()[1];

        m_OptionValues = optionValues;

        m_Setter = optionSetter;
        m_Getter = optionGetter;

        // Set up the slider
        m_Slider.maxValue = optionValues.Length - 1;

        // Determine what to select on start
        int index = 0;
        string targetOption = selectionGetter.Invoke((T)m_Getter.DynamicInvoke());
        foreach (string option in m_OptionValues)
        {
            if (option.Equals(targetOption))
                break;
            else
                index++;
        }

        // Set to the current value
        SetSlider(targetOption, index);
    }

    // Util
    private void SetSlider(string optionName, int pointerPosition)
    {
        m_SliderSelection.text = optionName;

        pointerPosition = (int)Mathf.Clamp(pointerPosition, m_Slider.minValue, m_Slider.maxValue);
        m_Slider.value = pointerPosition;

        m_Setter.DynamicInvoke(pointerPosition);
    }

    // Interface

    public void ToggleSelection(bool left)
    {
        int newPointerPosition = (int)Mathf.Clamp((int)m_Slider.value + (left ? -1 : 1), m_Slider.minValue, m_Slider.maxValue);

        m_SliderSelection.text = m_OptionValues[newPointerPosition];
        m_Slider.value = newPointerPosition;

        m_Setter.DynamicInvoke(newPointerPosition);
    }
}
