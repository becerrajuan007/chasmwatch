﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuAbs : MonoBehaviour
{
    // Parent script
    private PlayerUIHandler _PlayerUIHandler;

    // Constants
    private const float c_OutViewPosition = 1000f;
    private const float c_MenuOptionStart = 150f;
    private const float c_MenuOptionOffset = 40f;

    // Prefabs
    private static bool s_PrefabInit;
    private static GameObject s_SubMenuOptionPrefab;
    private static GameObject s_SubMenuCheckboxPrefab;
    private static GameObject s_SubMenuSliderDiscretePrefab;
    private static GameObject s_SubMenuSliderContinuousPrefab;

    // Members
    private RectTransform m_RectTransform;
    private RectTransform m_PointerTransform;

    private Text m_MenuTitle;
    private Text m_Message;
    private Transform m_MenuButtonsContainer;
    private List<SubMenuOption> m_SubMenuOptions;
    private int m_MenuIndex;

    private void Awake()
    {
        _PlayerUIHandler = FindObjectOfType<PlayerUIHandler>();

        if (!s_PrefabInit)
        {
            s_PrefabInit = true;

            s_SubMenuOptionPrefab = Resources.Load("Menus/SubMenuOption") as GameObject;
            s_SubMenuCheckboxPrefab = Resources.Load("Menus/SubMenuCheckbox") as GameObject;
            s_SubMenuSliderDiscretePrefab = Resources.Load("Menus/SubMenuSliderDiscrete") as GameObject;
            s_SubMenuSliderContinuousPrefab = Resources.Load("Menus/SubMenuSliderContinuous") as GameObject;
        }

        m_RectTransform = GetComponent<RectTransform>();
        m_PointerTransform = transform.Find("MenuPointer").GetComponent<RectTransform>();

        m_MenuTitle = transform.Find("Title").GetComponent<Text>();
        m_Message = transform.Find("Message").GetComponent<Text>();
        m_MenuButtonsContainer = transform.Find("MenuItems");
        m_SubMenuOptions = new List<SubMenuOption>();
        m_MenuIndex = 0;
    }

    private void UpdateMenuPointerPosition()
    {
        m_PointerTransform.anchoredPosition = new Vector2(m_PointerTransform.anchoredPosition.x, m_SubMenuOptions[m_MenuIndex].GetPosition());
    }

    // Setup Interface

    public void InitSubMenu(string title)
    {
        m_MenuTitle.text = title;

        ShowSubMenu(false);
    }

    public void AddEventOption(string optionName, Func<bool> optionEvent)
    {
        // Instantiate object
        GameObject newOption = Instantiate(s_SubMenuOptionPrefab);
        newOption.transform.SetParent(m_MenuButtonsContainer);

        // Get the script reference to initialize name, event, and position
        SubMenuOption optionScript = newOption.GetComponent<SubMenuOption>();
        m_SubMenuOptions.Add(optionScript);
        optionScript.Init(optionName, c_MenuOptionStart - ((m_SubMenuOptions.Count - 1) * c_MenuOptionOffset));
        optionScript.InitSubMenuOption(optionEvent);
    }

    public void AddCheckboxOption(string optionName, Action<bool> optionSetter, Func<bool> optionGetter)
    {
        // Instantiate object
        GameObject newOption = Instantiate(s_SubMenuCheckboxPrefab);
        newOption.transform.SetParent(m_MenuButtonsContainer);

        // Get script to init variables
        SubMenuCheckbox optionScript = newOption.GetComponent<SubMenuCheckbox>();
        m_SubMenuOptions.Add(optionScript);
        optionScript.Init(optionName, c_MenuOptionStart - ((m_SubMenuOptions.Count - 1) * c_MenuOptionOffset));
        optionScript.InitSubMenuCheckbox(optionSetter, optionGetter);
    }

    public void AddSliderOption<T>(string optionName, Action<int> optionSetter, Func<T> optionGetter, 
        Func<T, string> selectionGetter, string[] optionValues)
    {
        // Instantiate object
        GameObject newOption = Instantiate(s_SubMenuSliderDiscretePrefab);
        newOption.transform.SetParent(m_MenuButtonsContainer);

        // Get script to init variables
        SubMenuSliderDiscrete optionScript = newOption.GetComponent<SubMenuSliderDiscrete>();
        m_SubMenuOptions.Add(optionScript);
        optionScript.Init(optionName, c_MenuOptionStart - ((m_SubMenuOptions.Count - 1) * c_MenuOptionOffset));
        optionScript.InitSubMenuSliderDiscrete(optionSetter, optionGetter, selectionGetter, optionValues);
    }

    public void AddSliderOption(string optionName, Action<float> optionSetter, Func<float> optionGetter, Vector2 optionRange)
    {
        // Instantiate object
        GameObject newOption = Instantiate(s_SubMenuSliderContinuousPrefab);
        newOption.transform.SetParent(m_MenuButtonsContainer);

        SubMenuSliderContinuous optionScript = newOption.GetComponent<SubMenuSliderContinuous>();
        m_SubMenuOptions.Add(optionScript);
        optionScript.Init(optionName, c_MenuOptionStart - ((m_SubMenuOptions.Count - 1) * c_MenuOptionOffset));
        optionScript.InitSubMenuSliderContinuous(optionSetter, optionGetter, optionRange);
    }

    // Runtime Interface

    public void ShowSubMenu(bool show)
    {
        if (show)
        {
            // Show box
            m_RectTransform.anchoredPosition = Vector2.zero;

            // Reset pointer
            m_MenuIndex = 0;
            UpdateMenuPointerPosition();
        }
        else
        {
            // Hide box
            m_RectTransform.anchoredPosition = new Vector2(0f, c_OutViewPosition);
        }

        SetMessageText("");
    }

    public void ToggleMenuSelection(bool down)
    {
        if (down)
        {
            m_MenuIndex++;
            if (m_MenuIndex > m_SubMenuOptions.Count - 1) m_MenuIndex = 0;
        }
        else
        {
            m_MenuIndex--;
            if (m_MenuIndex < 0) m_MenuIndex = m_SubMenuOptions.Count - 1;
        }

        // Player audio
        if (_PlayerUIHandler)
            _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuToggle();

        UpdateMenuPointerPosition();
    }

    public void ToggleMenuSelectionSide(bool left)
    {
        var option = m_SubMenuOptions[m_MenuIndex];

        switch (option.GetType().ToString())
        {
            case "SubMenuSliderDiscrete":
                ((SubMenuSliderDiscrete)option).ToggleSelection(left);
                if (_PlayerUIHandler)
                    _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuToggle();
                break;
            case "SubMenuSliderContinuous":
                ((SubMenuSliderContinuous)option).SlideSelection(left);
                if (_PlayerUIHandler)
                    _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuToggle();
                break;
            default:
                // Ignore this input
                break;
        }
    }

    public void SelectMenuItem()
    {
        var option = m_SubMenuOptions[m_MenuIndex];

        switch(option.GetType().ToString())
        {
            case "SubMenuCheckbox":
                ((SubMenuCheckbox)option).ToggleOption();
                if (_PlayerUIHandler)
                    _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuSelect();
                break;
            case "SubMenuSliderDiscrete":
                // Ignore this input
                break;
            case "SubMenuSliderContinuous":
                // Ignore this input
                break;
            default:
                // Assume it is the base class SubMenuOption
                if (!m_SubMenuOptions[m_MenuIndex].ActivateOptionEvent())
                    Debug.LogError("Menu option " + m_MenuIndex + " failed to invoke method!");
                else if (_PlayerUIHandler)
                    _PlayerUIHandler._PlayerManager._PlayerAudioHandler.PlayMenuSelect();
                break;
        }
    }

    public void SetMessageText(string text) => m_Message.text = text;

}
