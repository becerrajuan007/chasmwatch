﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuSliderContinuous : SubMenuOption
{
    // Consts
    private const float SLIDE_INCREMENT = 0.1f;

    // Components
    private Slider m_Slider;
    private Text m_SliderSelection;

    // Function delegates
    private Action<float> m_Setter;
    private Func<float> m_Getter;

    public void InitSubMenuSliderContinuous(Action<float> optionSetter, Func<float> optionGetter, Vector2 optionRange)
    {
        m_Slider = GetComponentInChildren<Slider>();
        m_SliderSelection = GetComponentsInChildren<Text>()[1];

        m_Setter = optionSetter;
        m_Getter = optionGetter;

        // Set up the slider
        m_Slider.minValue = optionRange.x;
        m_Slider.maxValue = optionRange.y;

        // Determine what to select on start
        SetSlider(m_Getter.Invoke());
    }

    // Util

    private void SetSlider(float value)
    {
        value = Mathf.Clamp(value, m_Slider.minValue, m_Slider.maxValue);
        decimal decimalValue = Math.Round((decimal)value, 1);

        m_Slider.value = value;
        m_SliderSelection.text = decimalValue.ToString();

        m_Setter.Invoke(m_Slider.value);
    }

    // Interface

    public void SlideSelection(bool left)
    {
        SetSlider(m_Slider.value + SLIDE_INCREMENT * (left ? -1 : 1));
    }

}
