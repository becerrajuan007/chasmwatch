﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuCheckbox : SubMenuOption
{
    // Members
    private Toggle m_Toggle;

    // Function delegates
    private Action<bool> m_Setter;
    private Func<bool> m_Getter;

    public void InitSubMenuCheckbox(Action<bool> optionSetter, Func<bool> optionGetter)
    {
        m_Toggle = GetComponentInChildren<Toggle>();

        m_Setter = optionSetter;
        m_Getter = optionGetter;

        m_Toggle.isOn = m_Getter.Invoke();
    }

    public void ToggleOption()
    {
        bool enabledBefore = m_Getter.Invoke();
        m_Setter.Invoke(!enabledBefore);
        m_Toggle.isOn = !enabledBefore;
    }

}
