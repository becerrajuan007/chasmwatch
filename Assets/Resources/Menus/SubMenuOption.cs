﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuOption : MonoBehaviour
{
    // Members
    private RectTransform m_RectTransform;
    private Text m_OptionText;
    
    // Function delegates
    private Func<bool> m_OptionEvent;

    private void Awake()
    {
        m_RectTransform = GetComponent<RectTransform>();
        m_OptionText = GetComponent<Text>();
    }

    // Interface
    public void Init(string optionName, float position)
    {
        m_OptionText.text = optionName;
        m_RectTransform.anchoredPosition = new Vector2(0f, position);
        m_RectTransform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void InitSubMenuOption(Func<bool> optionEvent)
    {
        m_OptionEvent = optionEvent;
    }

    public float GetPosition()
    {
        return m_RectTransform.anchoredPosition.y;
    }

    public bool ActivateOptionEvent()
    {
        return m_OptionEvent.Invoke();
    }

}
