# Chasm

Chasm is an action-adventure role-playing game being developed in Unity using art assets from the Fantastic Fantasy Mega Bundle. Despite utilizing a good amount of art assets, nearly all code is written by me. Currently, the game has a small Zelda-like dungeon demo with Dark Souls-inspired combat, which can be found in separate build branches (check the “Usage” section). The game is still in development, but as of now, it has many features, including a timing-based combat system, a sprawling dungeon layout, and multiple enemy types to engage with. 

# Video
[![Chasmwatch Build 0.3](http://img.youtube.com/vi/9cWp1fp4-I0/0.jpg)](https://www.youtube.com/watch?v=9cWp1fp4-I0 "Chasmwatch Build 0.3")

# Screenshots
![1](RMImages/1.PNG)
![2](RMImages/2.PNG)
![3](RMImages/3.PNG)
![4](RMImages/4.PNG)
![5](RMImages/5.PNG)

More screenshots can be found under "/RMImages"

# Usage

Chasm can be played by selecting a branch other than "master", navigating to the "Build" directory, and downloading/launching the executable. The latest build can be found here: https://gitlab.com/becerrajuan007/Chasmwatch/-/tree/build_0.1/Build

# Log

### 10/31/2020 - Polishing and the Final Push (Build v0.5)
The Spookiest Part of Development

A LOT has been accomplished in this update! This includes a final boss encounter, debug options, improved rendering effects, dynamic audio and particle systems, and more! This was included in Build v0.4, but I wasn’t quite happy with pushing such an incremental change. Instead, I’m choosing to fully finished and decently polished demo by *The End of 2020*. Judging by the work I’ve been able to accomplish, I think that this is well within my grasp. Stay tuned! For now, here's a short GIF:

![11](RMImages/11.gif)

### 10/08/2020 - New Content, 'Improved' Visuals, and Bugfixes (Build v0.3)
Oops, I spilled my post-processing

In this update, I focused on filling out and creating an actual ‘game’ out of the forest dungeon demo, adding new visual flourishes to the demo, and generally handling my accumulated tech-debt.

The demo now features four orcs that must be defeated in (almost) any order, which requires the player to traverse the entire dungeon, fighting enemies and solving basic puzzles. The player is now able to die upon losing all health, which will return the player back to the last rested shrine. I hope to add new types of puzzles and enemies in the future to vary up the gameplay.

As for new visual flourishes, all dungeon zones now have dynamically spawning prefabs, light modules, and other granular details. I’m not much of an environment artist, but overall, I’m pretty happy with what I’ve been able to produce. I’ve also added new post-processing effects and rain particle effects to make the entire environment feel gloomier.

Build 0.3 is now fully playable. It allows you to get up to the point where you can enter the final boss zone, but no boss exists yet. For future updates, I’ll be cleaning up the game feel, adding new gameplay mechanics, and adding a final boss to the forest dungeon demo.

### 09/11/2020 - Dungeon Zone System, Improved Combat, and Build v0.1
When in doubt, pull out your data structures notebook 

In this update, I focused on adding new enemy types, adding new combat features, and implementing a tree-based dungeon zone system.
In addition to the Slime, I extended the abstract enemy interface, which allowed me to add two new enemy types: the bat and the orc. Bats act similarly to Slimes,  as they share most of their behavioral code. I hope to eventually add a system for them to glide above the terrain to give them a more unique combat behavior. In contrast, the Orc serves as ends to dungeon paths. They will eventually be guarding key items used to progress to the Forest Dungeon’s final boss.

In addition to enemies, I tightened up a few of the aspects to combat, including animation transitions and combo chaining. Most importantly, I added the ability to use a bow, which has the player enter a close third-person view, akin to a third-person shooter. A line then renders where the arrow will land upon pulling the bow.

Finally, I implemented a dungeon event system, which keeps track of enemy spawners. Upon reentering a dungeon area, weak mobs will respawn, just like in a Zelda game. However, large enemies like the Orc will despawn forever upon death, like in Dark Souls. In addition, I added the ability to activate shrines, which recover the player’s health and can be used to teleport to other spawn locations.

In the next update, I’ll be focusing on larger dungeon events, new enemy types, and generally finishing the Forest Dungeon Demo. Keep any eye out for v0.2!

### 08/11/2020 - New/Improved Mechanics, Enemy AI, and the Forest Dungeon
I hit you. Please fall over.

In this updates, I implemented and improved many of the player’s mechanics. For instance, the player can now combo light and heavy attacks, block the majority of incoming damage, dodge for invincibility frames, and aim with a bow (no arrows yet). Overall, it’s starting to feel like a proper action role playing game. However, just having a couple of player mechanics alone isn’t too much fun. To help out, I added an enemy class.

For now, I have a single “Slime” enemy, which will walk around its immediate area. One it can see the player, it will chase and enter an attack routine. These attacks have proper collision and damage values, and can be blocked or dodged by the player. In addition, subsequent attacks will break the enemy’s poise and trigger a stagger. Overall, I’m pretty happy with this abstract enemy class.

To pull everything together, I started work on a dungeon environment (more pictures under "/RMImages"). This scene is modeled after Ocarina of Time’s Forest Temple, and has the player reach four separate areas in the dungeon to unlock the final boss chamber. Currently, it’s just blocked out with basic low-poly prefabs, but I hope to continue work on it. In the end, I hope to use this area as a “Portfolio Demo”.

In the next update, I’ll be focusing on improving player mechanics, adding more enemy types, and programming the dungeon’s event system.

### 07/30/2020 - "I don't know, I never thought I'd get this far"
Here's some documentation, friendo

I normally make a log in all of my personal project READMEs, but I honestly thought that this would become another throw-away project. However, I actually kind of like this one! I’ve always struggled in architecting my projects as my ideas quickly turn into incoherent messes of coupled code. I’ve been reading “Game Programming Patterns” by Robert Nystrom, and I’ve managed to implement a lot of helpful design patterns and feel much more confident in this project. I now hope to turn this into a portfolio project where I can show off my gameplay programming implementation, as well as some fancy rendering tricks using Unity’s ShaderLab.
I’ve already implemented several features, and I hope to keep adding more over the next several months. Some features include: improved NPC behavior, improved combat game-feel, enemy AI behavior trees, and explorable dungeons.
